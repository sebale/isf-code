<?php 

function sb_badges_get_user_badges() {
    global $DB, $USER;

    $params = array(
        'userid1' => $USER->id,
    );

    $sql = 'SELECT
                b.*,
                u.email,
                bi.uniquehash,
                bi.dateissued,
                bi.dateexpire,
                bi.id as issuedid,
                bi.visible
            FROM
                {badge} b
                LEFT JOIN {badge_issued} bi ON bi.badgeid = b.id AND bi.userid = :userid1
                LEFT JOIN {user} u ON u.id = bi.userid
                LEFT JOIN {badge_criteria} bc ON bc.badgeid = b.id 
                LEFT JOIN {badge_criteria_param} bcp ON bcp.critid = bc.id 
            WHERE bi.id > 0';

    $sql .= ' GROUP BY b.id ORDER BY bi.dateissued DESC, b.timecreated DESC';
    $badges = $DB->get_records_sql($sql, $params);

    return $badges;
}
function sb_badges_get_user_certificates() {
    global $DB, $USER;

    $params = array(
        'userid' => $USER->id,
    );
    $sql = 'SELECT
              cer.id,
              c.fullname AS course_name,
              c.id AS course_id,
              cfo.value AS icon,
              cer.name,
              cm.id as cmid
            FROM mdl_certificate_issues ci
              LEFT JOIN mdl_certificate cer ON cer.id=ci.certificateid
              LEFT JOIN mdl_course c ON c.id=cer.course
              LEFT JOIN mdl_course_format_options cfo ON cfo.courseid=c.id AND cfo.name=\'icon\' AND cfo.format=\'isf\'
              JOIN mdl_modules m ON m.name=\'certificate\'
              LEFT JOIN mdl_course_modules cm ON cm.module=m.id AND cm.course=c.id AND cm.instance=cer.id
            WHERE ci.userid =:userid';

    return $DB->get_records_sql($sql, $params);
}
function sb_badges_get_user_stat() {
    global $DB, $USER;

    $params = array(
        'userid1' => $USER->id,
        'userid2' => $USER->id,
        'userid3' => $USER->id,
    );
    $sql = 'SELECT
              (SELECT xp FROM mdl_block_xp WHERE userid=:userid1 AND courseid=1) AS points,
              (SELECT COUNT(id) FROM mdl_badge_issued WHERE userid=:userid2) AS badges,
              (SELECT COUNT(id) FROM mdl_certificate_issues WHERE userid=:userid3) AS certificates';

    return $DB->get_record_sql($sql, $params);
}

