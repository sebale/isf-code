<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles displaying the calendar block.
 *
 * @package    block_sb_coursenav
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once($CFG->dirroot.'/blocks/moodleblock.class.php');

class block_sb_coursenav extends block_base {

    /**
     * Initialise the block.
     */
    public function init() {
        $this->title = get_string('pluginname', 'block_sb_coursenav');
    }
    function hide_header() {
        return true;
    }
    function html_attributes() {
        $attributes = parent::html_attributes();
        $attributes['class'] .= ' no_border_block block_' . $this->name();
        return $attributes;
    }
    /**
     * Return preferred_width.
     *
     * @return int
     */
    public function preferred_width() {
        return 210;
    }

    /**
     * Return the content of this block.
     *
     * @return stdClass the content
     */
    public function get_content() {
        global $CFG, $DB, $USER, $OUTPUT, $PAGE;

        if ($this->content !== null) {
            return $this->content;
        }

        $this->content = new stdClass;
        $this->content->text = '';
        $this->content->footer = '';

        $course = $PAGE->course;
        $cm     = ($PAGE->cm) ? $PAGE->cm : 0;
        $issite = ($course->id == SITEID);

        if ($issite){
            return '';
        }

        $renderer = $PAGE->get_renderer('block_sb_coursenav');
        $this->content->text = $renderer->sb_coursenav($course, $issite, $cm);

        return $this->content;
    }
}


