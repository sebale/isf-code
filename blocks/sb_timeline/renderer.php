<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Time Line
 *
 * @package    block_sb_timeline
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

class block_sb_timeline_renderer extends plugin_renderer_base {

    public $timeline_limit = 10;

    public function sb_timeline($data = array(), $issite = true, $course) {
        global $CFG, $PAGE, $OUTPUT;

        $output = '';

        list($data_modules, $smodules) = $data;

        $display_modules = array(); $i=0;
        foreach($smodules as $cmid=>$date) {
            $display_modules = $data_modules[$cmid];
            if ($i == $this->timeline_limit) break;
        }

        $output .= html_writer::start_tag('div', array('class'=>'graph'.((!count($display_modules)) ? ' empty-graph' : '')));
        $output .= html_writer::start_tag('div', array('class'=>'timeline-header clearfix'));
            $output .= html_writer::tag('h2', get_string('pluginname', 'block_sb_timeline'));
            $output .= html_writer::tag('i', '', array('class'=>'fa fa-bars', 'title'=>get_string('list', 'block_sb_timeline'), 'data-value'=>'list'));
            $output .= html_writer::tag('i', '', array('class'=>'fa fa-ellipsis-v active', 'title'=>get_string('timeline', 'block_sb_timeline'), 'data-value'=>'time'));
        $output .= html_writer::end_tag('div');
            $output .= html_writer::start_tag('div', array('class'=>'timeline'));
            $k=1; $i=0; $year = 0;
            foreach($smodules as $cmid=>$date) {
                $module = $data_modules[$cmid];
                if ($i == $this->timeline_limit) break;
                if($module->date != 'no-date'){
                    if ($year != date('Y', $module->date)){
                        $year = date('Y', $module->date);
                        $output .= html_writer::tag('div', $year, array('class'=>'timesecion'));
                    }
                } elseif($i==0 and $year == 0) {
                    $output .= html_writer::tag('div', date('Y'), array('class'=>'timesecion'));
                }
                $output .= html_writer::start_tag('div', array('class'=>'timeslot clearfix'.(($k%2==0) ? ' alt' : '')));
                    $output .= html_writer::start_tag('div', array('class'=>'task'));
                        $output .= (($issite) ? html_writer::tag('span', $module->fullname) : '');

                        $url = new moodle_url('/mod/'.$module->name.'/view.php', array('id' => $module->id));
                        $output .= html_writer::link($url, $module->title);

                        $output .= html_writer::start_tag('p');
                            $output .= html_writer::tag('strong', get_string('type', 'block_sb_timeline')).': ';
                            $output .= get_string('pluginname', 'mod_'.$module->name);
                            $output .= html_writer::tag('div', '', array('class'=>'arrow'));
                        $output .= html_writer::end_tag('p');
                    $output .= html_writer::end_tag('div');

                    $output .= html_writer::tag('div', $OUTPUT->pix_icon('icon', $module->title, $module->name), array('class'=>'activity-icon'));

                    $output .= html_writer::tag('div', (($module->date == 'no-date') ? get_string('noduedateavailable', 'block_sb_timeline') : html_writer::tag('strong', get_string('due', 'block_sb_timeline').': ').date('m/d/Y', strtotime(userdate($module->date))).'<br />'.date('h:i A', strtotime(userdate($module->date)))), array('class'=>'time'));

                $output .= html_writer::end_tag('div');
                $k++;$i++;
            }
            if (count($data_modules)){
                $output .= html_writer::start_tag('div', array('class'=>'timesecion ts-bottom'));
                    if (count($data_modules) > $this->timeline_limit) {
                        $output .= html_writer::tag('span', get_string('more', 'block_sb_timeline'), array('class'=>'load-more', 'id'=>'load_more', 'onclick' => 'timeline_load_more('.$course->id.', '.count($data_modules).')', 'st'=>$this->timeline_limit, 'pos'=>$k));
                    } else {
                        $output .= html_writer::tag('div', (($course->startdate) ? date('Y', $course->startdate) : date('Y')), array('class'=>'end'));
                    }
                $output .= html_writer::end_tag('div');
            } else {
                $output .= html_writer::tag('div', get_string('nothingtodisplay', 'block_sb_timeline'), array('class'=>'alert alert-success'));
            }
            $output .= html_writer::end_tag('div');
        $output .= html_writer::end_tag('div');

        $preferences = get_user_preferences('show_timeline_'.$course->id, 1);

        $PAGE->requires->js_call_amd('block_sb_timeline/timeline', 'init', array('courseid'=>$course->id, 'timeline_limit'=>$this->timeline_limit, 'modules_count'=>count($data_modules), 'show_timeline'=>$preferences));

        return $output;
    }

}
