<?php

require_once('../../config.php');

$id            = optional_param('id', 0, PARAM_INT);
$action        = optional_param('action', '', PARAM_RAW);

if($action == 'load_timeline') {

    $timeline_limit = 10;
    $start          = optional_param('start', 0, PARAM_INT);
    $k              = optional_param('k', 0, PARAM_INT);
    $result = '';

    $course = $DB->get_record('course', array('id'=>$id));
    $context = context_course::instance($course->id, MUST_EXIST);
    $PAGE->set_context($context);

    if ($id > 1){

        $modules = $DB->get_records_sql("
                SELECT cm.id, m.name, c.fullname, cm.instance, cm.added, cm.completionexpected, cmc.id as cmcid
                  FROM {course_modules} cm
             LEFT JOIN {modules} m ON m.id = cm.module
             LEFT JOIN {course_sections} cs ON cs.id = cm.section AND cs.course = cm.course
             LEFT JOIN {course} c ON c.id = cm.course
             LEFT JOIN {course_modules_completion} cmc ON cmc.coursemoduleid = cm.id AND cmc.userid = :userid AND cmc.completionstate = 1 AND cm.section > 0
                 WHERE c.id = :courseid AND cmc.id IS NULL AND cs.section > 0 AND cm.visible > 0
              ORDER BY cm.added ASC", array('userid'=>$USER->id, 'courseid'=>$id)
        );

        $smodules = array();
        $data_modules = array();
        foreach ($modules as $module){
            $mod = $DB->get_record_sql("SELECT * FROM {".$module->name."} WHERE id = ".$module->instance);
            $module->title = $mod->name;
            if($module->completionexpected){
                $module->date = $module->completionexpected;
            }elseif(isset($mod->timeclose) and $mod->timeclose != '0'){
                $module->date = $mod->timeclose;
            }elseif(isset($mod->duedate) and $mod->duedate != '0'){
                $module->date = $mod->duedate;
            }else{
                $module->date = 'no-date';
            }
            if ($module->date != 'no-date' and $module->date <= time()) continue;

            $data_modules[$module->id] = $module;
            $smodules[$module->id] = $module->date;
        }
        asort($smodules);
    } else {
        $modules = $DB->get_records_sql("
                SELECT cm.id, m.name, c.fullname, cm.instance, cm.added, cm.completionexpected, cmc.id as cmcid
                  FROM {course_modules} cm
             LEFT JOIN {modules} m ON m.id = cm.module
             LEFT JOIN {course_sections} cs ON cs.id = cm.section AND cs.course = cm.course
             LEFT JOIN {course} c ON c.id = cm.course
             LEFT JOIN {enrol} e ON e.courseid = c.id
             LEFT JOIN {user_enrolments} ue ON ue.enrolid = e.id
             LEFT JOIN {course_modules_completion} cmc ON cmc.coursemoduleid = cm.id AND cmc.userid = :userid1 AND cmc.completionstate = 1
                 WHERE ue.userid = :userid2 AND cmc.id IS NULL AND cs.section > 0 AND cm.visible > 0
              ORDER BY cm.added ASC", array('userid1'=>$USER->id, 'userid2'=>$USER->id));

        $smodules = array();
        $data_modules = array();
        foreach ($modules as $module){
            $mod = $DB->get_record_sql("SELECT * FROM {".$module->name."} WHERE id = ".$module->instance);
            $module->title = $mod->name;
            if(isset($mod->timeclose) and $mod->timeclose != '0'){
                $module->date = $mod->timeclose;
            }elseif(isset($mod->duedate) and $mod->duedate != '0'){
                $module->date = $mod->duedate;
            }elseif($module->completionexpected){
                $module->date = $module->completionexpected;
            }else{
                $module->date = 'no-date';
            }
            if ($module->date != 'no-date' and $module->date <= time()) continue;

            $data_modules[$module->id] = $module;
            $smodules[$module->id] = $module->date;
        }
        asort($smodules);
    }

    $output = '';

    $i=0; $year = 0;
    foreach($smodules as $cmid=>$date) {
        $module = $data_modules[$cmid];
        $i++;
        if ($i <= $start) continue;
        if ($i > $start+$timeline_limit) break;

        $output .= html_writer::start_tag('div', array('class'=>'timeslot clearfix'.(($k%2==0) ? ' alt' : '')));
            $output .= html_writer::start_tag('div', array('class'=>'task'));
                $output .= (($id > 1) ? '' : html_writer::tag('span', $module->fullname));

                $url = new moodle_url('/mod/'.$module->name.'/view.php', array('id' => $module->id));
                $output .= html_writer::link($url, $module->title);

                $output .= html_writer::start_tag('p');
                    $output .= html_writer::tag('strong', get_string('type', 'block_sb_timeline')).': ';
                    $output .= get_string('pluginname', 'mod_'.$module->name);
                    $output .= html_writer::tag('div', '', array('class'=>'arrow'));
                $output .= html_writer::end_tag('p');
            $output .= html_writer::end_tag('div');

            $output .= html_writer::tag('div', $OUTPUT->pix_icon('icon', $module->title, $module->name), array('class'=>'activity-icon'));

            $output .= html_writer::tag('div', (($module->date == 'no-date') ? get_string('noduedateavailable', 'block_sb_timeline') : html_writer::tag('strong', get_string('due', 'block_sb_timeline').': ').date('m/d/Y', strtotime(userdate($module->date))).'<br />'.date('h:i A', strtotime(userdate($module->date)))), array('class'=>'time'));

        $output .= html_writer::end_tag('div');
        $k++;
    }

    if (count($data_modules)){
        $output .= html_writer::start_tag('div', array('class'=>'timesecion ts-bottom'));
        if ($timeline_limit + $start < count($data_modules)) {
            $output .= html_writer::tag('span', get_string('more', 'block_sb_timeline'), array('class'=>'load-more', 'id'=>'load_more', 'onclick'=>'timeline_load_more('.$course->id.', '.count($data_modules).')', 'st'=>($timeline_limit+$start), 'pos'=>$k));
        } else {
            $output .= html_writer::tag('div', (($course->startdate) ? date('Y', $course->startdate) : date('Y')), array('class'=>'end'));
        }
        $output .= html_writer::end_tag('div');
    } else {
        $output .= html_writer::tag('div', get_string('nothingtodisplay', 'block_sb_timeline'), array('class'=>'alert alert-success'));
    }

    echo $output;
}
exit;
