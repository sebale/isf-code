<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Time Line
 *
 * @package    block_sb_timeline
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

function block_sb_timeline_render_navbar_output(\renderer_base $renderer) {
    global $USER, $CFG, $PAGE;
    $output = '';

    if (!isloggedin()){
        return $output;
    }

    /*$context = [];
    $output .= $renderer->render_from_template('block_sb_timeline/timeline_nav_toggler', $context);*/

    return $output;
}
