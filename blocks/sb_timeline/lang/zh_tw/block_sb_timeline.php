<?php 

$string['sb_timeline:addinstance'] = '添加新的時間軸';
$string['sb_timeline:myaddinstance'] = '添加新的時間軸到我的主頁';
$string['pluginname'] = '時間軸';
$string['type'] = '輸入';
$string['noduedateavailable'] = '沒有到期日';
$string['due'] = '到期';
$string['nothingtodisplay'] = '沒有內容顯示';
$string['more'] = '更多';
$string['list'] = '列表';
$string['timeline'] = '時間軸';
$string['toggletimeline'] = '切換時間軸';