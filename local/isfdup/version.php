<?php
/**
 * ISF Activity Duplication
 * View the associated README.md file for more information.
 *
 * Plugin version information
 *
 * @package    local_isfdup
 * @author     Shane Elliott (@link shane@pukunui.com)
 * @copyright  2017 Pukunui (@link pukunui.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2017022006;
$plugin->requires  = 2016110100;
$plugin->component = 'local_isfdup';
$plugin->maturity  = MATURITY_STABLE;
$plugin->cron      = 0;

