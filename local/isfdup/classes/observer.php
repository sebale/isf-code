<?php
/**
 * ISF Activity Duplication
 * View the associated README.md file for more information.
 *
 * Event handlers
 *
 * @package    local_isfdup
 * @author     Shane Elliott (@link shane@pukunui.com)
 * @copyright  2017 Pukunui (@link pukunui.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Handler definitions for events
 *
 * @author     Shane Elliott (@link shane@pukunui.com)
 * @copyright  2017 Pukunui (@link pukunui.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class local_isfdup_observer {
    
    /**
     * Event observer for activity module updates
     *
     * @param object $event
     */
    public static function module_updated($event) {
        // TODO
    }
}
