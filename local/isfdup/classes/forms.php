<?php
/**
 * ISF Activity Duplication
 * View the associated README.md file for more information.
 *
 * Form definitions
 *
 * @package    local_isfdup
 * @author     Shane Elliott (@link shane@pukunui.com)
 * @copyright  2017 Pukunui (@link pukunui.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');
require_once($CFG->dirroot.'/local/isfdup/locallib.php');

/**
 * Define form for white-listing module settings to be propagated.
 */
class local_isfdup_modulesettingsform extends moodleform {
    
    /**
     * Form definition
     */
    public function definition() {
        $mform =& $this->_form;
        $strrequired = get_string('required');

        $mform->addElement('text', 'name', get_string('settingname', 'local_isfdup'), get_string('settingnamedescription', 'local_isfdup'));
        $mform->setType('name', PARAM_ALPHAEXT);
        $mform->addRule('name', null, 'required', null, 'client');
        
        $this->add_action_buttons(false);
    }

    /**
     * Form validation
     *
     * @param array $data
     * @param array $files
     * @return array
     */
    public function validation($data, $files) {
        global $DB;

        $errors = parent::validation($data, $files);

        $modulename = local_isfdup_get_modulename($data['name']);
        if (empty($modulename)) {
            $parts = explode('_', $data['name']);
            $errors['name'] = get_string('error_unknownmodule', 'local_isfdup', $parts[0]);
        }

        return $errors;
    }
}


/**
 * Define form for course search.
 */
class local_isfdup_coursesearchform extends moodleform {

    /**
     * Form definition
     */
    public function definition() {
        
        $mform =& $this->_form;

        $courses = $this->_customdata['courses'];
        $section = $this->_customdata['section'];

        $strrequired = get_string('required');

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);

        $mform->addElement('hidden', 'cm');
        $mform->setType('cm', PARAM_INT);

        $mform->addElement('hidden', 'ts');
        $mform->setType('ts', PARAM_INT);

        // Add the topic section data iff sid is non-zero
        if (!empty($section) and ($cms=local_isfdup_get_section_modules($section->id))) {
            
            $mform->addElement('header', 'headerchoosemodules', get_string('choosemodules', 'local_isfdup'));
            $mform->addElement('checkbox', 'wholesection', get_string('duplicatewholesection', 'local_isfdup'), get_string('duplicatewholesectiondescription', 'local_isfdup'));
            $mform->setDefault('wholesection', 1);

            $cmgroup = array();
            foreach ($cms as $cmid=>$cm) {
                $cmgroup[] =& $mform->createElement('checkbox', 'cmid_'.$cmid, $cm);
            }
            $mform->addGroup($cmgroup, 'cmgroup', '', '<br />', false);
            $mform->disabledIf('cmgroup', 'wholesection', 'checked');

            $mform->addElement('checkbox', 'ignoreexisting', get_string('ignoreexistingduplicates', 'local_isfdup'), get_string('ignoreexistingduplicatesdescription', 'local_isfdup'));
            $mform->setDefault('ignoreexisting', 0);

            $mform->addElement('static', 'breaker2', '', '<hr>');
            $mform->addElement('header', 'headerchoosecourses', get_string('choosecourses', 'local_isfdup'));

        }

        // Cycle through course data.
        foreach ($courses as $cid=>$fullname) {
            $ccid = 'cid_'.$cid;
            $mform->addElement('checkbox', $ccid, $fullname);
            $mform->setType($ccid, PARAM_INT);
            $mform->setDefault($ccid, 0);
        }

        $this->add_action_buttons(false);
    }

    /**
     * Validate data
     *
     * @param array $data  submitted form data
     * @param array $files  submitted files
     * @return array
     */
    public function validation($data, $files) {
        $errors = parent::validation($data, $files);

        return $errors;
    }

}
