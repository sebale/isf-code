<?php
namespace local_isfdup;

/**
 * ISF Activity Duplication
 * View the associated README.md file for more information.
 *
 * Hook functions for changes in core code.
 *
 * @package    local_isfdup
 * @author     Shane Elliott (@link shane@pukunui.com)
 * @copyright  2017 Pukunui (@link pukunui.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use html_writer;
use moodle_url;
use lang_string;
use pix_icon;
use action_menu_link_secondary;

require_once($CFG->dirroot.'/local/isfdup/locallib.php');
require_once($CFG->dirroot.'/local/isfdup/classes/forms.php');

/**
 * Hook functions.
 *
 * @package    local_isfdup
 * @author     Shane Elliott(@link shane@pukunui.com)
 * @copyright  2017 Pukunui (@link pukunui.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class hooks {

    /**
     * Output label with lock icon in modedit form.
     *
     * @param string $label  the raw label
     * @param object $cm  course module
     * @return string
     */
    public static function locked_cm_label($label, $cm) {
        global $CFG, $OUTPUT;

        $strdupmod = get_string('duplicatedmodule', 'local_isfdup');
        $strconfirmlinkremoval = get_string('confirmlinkremoval', 'local_isfdup');
        $pixicon = $OUTPUT->pix_icon('t/locked', $strdupmod, 'moodle', array('class'=>'iconsmall','title'=>$strdupmod));

        //TODO Find a moodle jquery way to do the confirm popup.
        //TODO Implement an non-javascript fallback.
        $jsurl = $CFG->wwwroot."/local/isfdup/pt.php?a=rmcmlnem&cm={$cm->id}&confirm=1&sesskey=".sesskey();
        $pixlink = \html_writer::link('#', $pixicon,
                                      array('onclick' => 'if(confirm("'.$strconfirmlinkremoval.'")) window.location.href="'.$jsurl.'";')
                                     );
        return "$label $pixlink";
    }

    /**
     * Update linked course module names.
     *
     * @param integer $mastercmid  master course module id
     * @param string $name  new title
     * @return integer  number of course modules changed
     */
    public static function update_coursemodule_name($mastercmid, $name) {
        global $DB;

        $count = 0;
        if ($cmids = $DB->get_records('local_isfdup', array('sourcecmid'=>$mastercmid), 'id,destinationcmid')) {
            foreach ($cmids as $cm) {
                set_coursemodule_name($cm->destinationcmid, $name);
                $count++;
            }
        }
        return $count;
    }

    /**
     * Choose which form elements to freeze when editing a course module
     *
     * @param object $cm  course module
     * @return array
     */
    public static function get_cm_freeze_settings($cm) {
        global $DB;
        if (!empty($cm->id) and $DB->record_exists('local_isfdup', array('destinationcmid'=>$cm->id))) {
            return local_isfdup_get_simplified_settings($cm->module, true);
        }
        return null;
    }

    /**
     * Given submitted data from course/modedit.php also update linked
     * duplicate modules.
     *
     * @param object  $mastercm
     * @param object  $data  submitted form data
     * @param object  $mastercourse
     * @param object  $mform
     * @return boolean
     */
    public static function update_duplicated_moduleinfo($mastercm, $fromform, $mastercourse, $mform) {
        global $DB;

        // An array of settings that will be retained in the duplicated activity.
        // $newdatafield => $existingdatafield
        $dupsettings = array('instance'=>'instance',
                             'course'=>'course',
                             'cmidnumber'=>'idnumber',
                             'coursemodule'=>'id',
                             'visible'=>'visible',
                            );

        // Retrieve the settings to be propagated.
        if (!($settings = local_isfdup_get_simplified_settings($mastercm->module, true))) {
            // No settings to propagate.
            return;
        }
        // Some settings are required
        $settings[] = 'modulename';
        $settings[] = 'module';
        $settings[] = 'introeditor';
        $settings[] = 'printheading';
        $settings[] = 'printintro';
        $settings[] = 'revision';
        $settings[] = 'display';

        $originaldata = $mform->get_data();
        $data = new \stdClass;

        foreach ($settings as $setting) {
            if (isset($originaldata->$setting)) {
                $data->$setting = $originaldata->$setting;
            }
        }

        if ($cms = $DB->get_records('local_isfdup', array('sourcecmid'=>$mastercm->id), 'id,destinationcmid,destinationcourseid')) {
            foreach ($cms as $cminfo) {
                $cm = get_coursemodule_from_id('', $cminfo->destinationcmid, 0, true, MUST_EXIST);
                $course = $DB->get_record('course', array('id'=>$cminfo->destinationcourseid));
                // We need to mangle some information in the submitted form data.
                foreach ($dupsettings as $nds=>$ods) {
                    if (isset($cm->$ods)) {
                        $data->$nds = $cm->$ods;
                    }
                }
                $data2 = clone($data);
                update_moduleinfo($cm, $data2, $course, $mform);
            }
        }
    }

    /**
     * Delete the link between a master and destination course module.
     * As a destination course module can only have one master this should
     * be unique and thus all we require.
     *
     * @param integer $cmid  destination course module id
     * @return boolean
     */
    public static function remove_course_module_link($cmid) {
        global $DB;

        return $DB->delete_records('local_isfdup', array('destinationcmid'=>$cmid));
    }

    /**
     * Delete the link between a master and destination course modules for
     * a given master course module id
     *
     * @param integer $cmid  master course module id
     * @return boolean
     */
    public static function remove_master_course_module_link($cmid) {
        global $DB;

        return $DB->delete_records('local_isfdup', array('sourcecmid'=>$cmid));
    }

    /**
     * Delete all the course module links in a given section.
     *
     * @param integer $courseid  id from the course table
     * @param integer $sectionid  course section id
     * @return void
     */
    public static function remove_course_section($courseid, $sectionid) {
        if ($cmids = local_isfdup_get_section_modules($sectionid)) {
            foreach ($cmids as $cmid=>$name) {
                self::remove_course_module_link($cmid);
                self::remove_master_course_module_link($cmid);
            }
        }
    }

    /**
     * Display icon if given course module is linked.
     *
     * @param integer $cmid  course module id
     * @return string  HTML
     */
    public static function link_icon($cmid) {
        global $DB, $OUTPUT, $CFG;

        //TODO cache $pixicon so that we're not making this call for every module in a course.
        $strdupmod = get_string('duplicatedmodule', 'local_isfdup');
        $strconfirmlinkremoval = get_string('confirmlinkremoval', 'local_isfdup');
        $pixicon = $OUTPUT->pix_icon('t/locked', $strdupmod, 'moodle', array('class'=>'iconsmall','title'=>$strdupmod));

        //TODO Find a moodle jquery way to do the confirm popup.
        //TODO Implement an non-javascript fallback.
        $jsurl = $CFG->wwwroot."/local/isfdup/pt.php?a=rmcmln&cm=$cmid&confirm=1&sesskey=".sesskey();
        $pixlink = \html_writer::link('#', $pixicon,
                                      array('onclick' => 'if(confirm("'.$strconfirmlinkremoval.'")) window.location.href="'.$jsurl.'";')
                                     );
        return ($DB->record_exists('local_isfdup', array('destinationcmid'=>$cmid))) ? $pixlink : '';
    }

    /**
     * Set the "duplicate to"code snippet for course module editing menu
     *
     * @param cm_info $mod  course_module index
     * @return action_menu_link_secondary
     */
    public static function course_module_menu_link($mod) {
        $strduplicateto = new lang_string('duplicateto', 'local_isfdup');
/*
        return new action_menu_link_secondary(
            new moodle_url('/local/isfdup/dupto.php', array('cm'=>$mod->id, 'id'=>$mod->course)),
            new pix_icon('t/edit', $strduplicateto, 'moodle', array('class'=>'iconsmall', 'title'=>'')),
            $strduplicateto,
            array('class'=>'duplicateto', 'data-action'=>'duplicateto')
            );
*/
        return new action_menu_link_secondary(
            new moodle_url('#'),
            new pix_icon('t/duplicateto', $strduplicateto, 'moodle', array('class'=>'iconsmall', 'title'=>'')),
            $strduplicateto,
            array('class'=>'duplicateto',
                  'data-toggle'=>'modal',
                  'data-target'=>'#dupcmmodal'.$mod->id)
            );
    }

    /**
     * Check if a course module is editable, that is, it's not linked to a master
     *
     * @param integer $cmid  course module id
     * @return boolean
     */
    public static function can_edit_cm($cmid) {
        global $DB;

        return (!($DB->record_exists('local_isfdup', array('destinationcmid'=>$cmid))));
    }

    /**
     * Check if sections and modules can be duplicated
     * ie is the current course marked as a master course?
     *
     * @param integer $courseid  id from the course table
     * @return boolean
     */
    public static function can_duplicate($courseid) {
        global $DB;

        return ($DB->record_exists('local_isfdup_courses', array('courseid'=>$courseid, 'master'=>1)));
    }

    /**
     * Set the "duplicate to" code snippet for topic section editing menu
     *
     * @param stdClass $section
     * @return array
     */
    public static function section_menu_link($section) {
        $strduplicateto = new lang_string('duplicateto', 'local_isfdup');
        return array(
            'url'     => new moodle_url('#'),
            'icon'    => 't/duplicateto',
            'name'    => $strduplicateto,
            'pixattr' => array('class'=>'', 'alt'=>$strduplicateto),
            'attr'    => array('class'=>'icon edit',
                               'title'=>$strduplicateto,
                               ' data-toggle'=>'modal',
                               ' data-target'=>'#dupsectionmodal'.$section->id
                              )
        );
    }

    /**
     * Print the modal html for the section and all associated modules
     *
     * @param stdClass $section
     * @return string
     */
    public static function section_modals($section) {
        $out = self::section_modal_html($section);
        if (!empty($section->sequence)) {
            $cmids = explode(',', $section->sequence);
            foreach ($cmids as $cmid) {
                $cm = get_coursemodule_from_id('', $cmid, 0, true, MUST_EXIST);
                $out .= self::cm_modal_html($cm);
            }
        }
        return $out;
    }

    /**
     * Print bootstrap modal HTML code for a given section.
     *
     * @param stdClass $section
     * @return string
     */
    public static function section_modal_html($section) {
        $dupname = (empty($section->name)) ? 'Section' : $section->name;
        $strtitle = new lang_string('duplicatemodulesto', 'local_isfdup', $dupname);
        $url = new moodle_url('/local/isfdup/dupto.php');
        $formcustomdata = array('courses' => local_isfdup_get_external_courses(),
                                'section' => $section);
        $coursesearchform = new \local_isfdup_coursesearchform($url->out(), $formcustomdata);
        $data = array('cm' => 0,
                      'ts' => $section->id,
                      'id' => $section->course);
        $coursesearchform->set_data($data);

        $out = html_writer::start_tag('div', array('class'=>'modal fade',
                                                   'id'=>'dupsectionmodal'.$section->id,
                                                   'tabindex'=>'-1',
                                                   'role'=>'dialog'
                                                  ));
        $out.= html_writer::start_tag('div', array('class'=>'modal-dialog',
                                                   'role'=>'document'
                                                  ));
        $out.= html_writer::start_tag('div', array('class'=>'modal-content'));
        $out.= html_writer::start_tag('div', array('class'=>'modal-header'));
        $out.= html_writer::start_tag('button', array('type'=>'button',
                                                      'class'=>'close',
                                                      'data-dismiss'=>'modal',
                                                      'aria-label'=>'close'
                                                     ));
        $out.= html_writer::tag('span', '&times;', array('aria-hidden'=>'true'));
        $out.= html_writer::end_tag('button');
        $out.= html_writer::tag('h4', $strtitle->out(), array('class'=>'modal-title'));
        $out.= html_writer::end_tag('div');
        $out.= html_writer::start_tag('div', array('class'=>'modal-body'));
        $out.= $coursesearchform->render();
        $out.= html_writer::end_tag('div');
        $out.= html_writer::tag('div', '&nbsp', array('class'=>'modal-footer'));
        $out.= html_writer::end_tag('div');
        $out.= html_writer::end_tag('div');
        $out.= html_writer::end_tag('div');

        return $out;
    }

    /**
     * Print bootstrap modal HTML code for a given course section
     *
     * @param stdClass $cm
     * @return string
     */
    public static function cm_modal_html($cm) {
        $dupname = $cm->modname;
        $strtitle = new lang_string('duplicatemodulesto', 'local_isfdup', $dupname);
        $url = new moodle_url('/local/isfdup/dupto.php');
        $formcustomdata = array('courses' => local_isfdup_get_external_courses(),
                                'section' => null);
        $coursesearchform = new \local_isfdup_coursesearchform($url->out(), $formcustomdata);
        $data = array('cm' => $cm->id,
                      'ts' => 0,
                      'id' => $cm->course);
        $coursesearchform->set_data($data);

        $out = html_writer::start_tag('div', array('class'=>'modal fade',
                                                   'id'=>'dupcmmodal'.$cm->id,
                                                   'tabindex'=>'-1',
                                                   'role'=>'dialog'
                                                  ));
        $out.= html_writer::start_tag('div', array('class'=>'modal-dialog',
                                                   'role'=>'document'
                                                  ));
        $out.= html_writer::start_tag('div', array('class'=>'modal-content'));
        $out.= html_writer::start_tag('div', array('class'=>'modal-header'));
        $out.= html_writer::start_tag('button', array('type'=>'button',
                                                      'class'=>'close',
                                                      'data-dismiss'=>'modal',
                                                      'aria-label'=>'close'
                                                     ));
        $out.= html_writer::tag('span', '&times;', array('aria-hidden'=>'true'));
        $out.= html_writer::end_tag('button');
        $out.= html_writer::tag('h4', $strtitle->out(), array('class'=>'modal-title'));
        $out.= html_writer::end_tag('div');
        $out.= html_writer::start_tag('div', array('class'=>'modal-body'));
        $out.= $coursesearchform->render();
        $out.= html_writer::end_tag('div');
        $out.= html_writer::tag('div', '&nbsp', array('class'=>'modal-footer'));
        $out.= html_writer::end_tag('div');
        $out.= html_writer::end_tag('div');
        $out.= html_writer::end_tag('div');

        return $out;
    }

}
