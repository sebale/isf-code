<?php
/**
 * ISF Activity Duplication
 * View the associated README.md file for more information.
 *
 * Output renderers
 *
 * @package    local_isfdup
 * @author     Shane Elliott (@link shane@pukunui.com)
 * @copyright  2017 Pukunui (@link pukunui.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * General renderers for plugin
 *
 * @package    local_isfdup
 * @author     Shane Elliott (@link shane@pukunui.com)
 * @copyright  2017 Pukunui (@link pukunui.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class local_isfdup_renderer extends plugin_renderer_base {
    
    /**
     * Print a list of module settings.
     *
     * @param moodle_form $form  add new setting form.
     * @return string  HTML to be output.
     */
    public function print_module_settings($form) {
        global $OUTPUT;

        $strdelete = get_string('delete', 'local_isfdup');

        $settings = local_isfdup_get_whitelist_settings();
        $table = new html_table();
        $table->attributes = array('class'=>'generaltable');
        $table->head = array(get_string('settingname', 'local_isfdup'),
                             get_string('modulename', 'local_isfdup'),
                             get_string('actions', 'local_isfdup'),
                            );
        
        foreach ($settings as $setting) {
            $row = new html_table_row();

            $cell = new html_table_cell();
            $cell->text = $setting->setting;
            $row->cells[] = $cell;

            $cell = new html_table_cell();
            $cell->text = html_writer::link(new moodle_url('/course/search.php',
                                                           array('modulelist'=>$setting->module,
                                                                 'sesskey'=>sesskey())),
                                            $setting->module);
            $row->cells[] = $cell;

            $cell = new html_table_cell();
            $cell->text = html_writer::link(new moodle_url('',
                                                           array('d'=>$setting->id,
                                                                 'sesskey'=>sesskey())),
                                            $OUTPUT->pix_icon('t/delete',
                                                              $strdelete,
                                                              'moodle',
                                                              array('class'=>'iconsmall',
                                                                    'title'=>$strdelete)));
            $row->cells[] = $cell;
            $table->data[] = $row;
        }

        // Put all the output together.
        $html  = $this->header();
        $html .= html_writer::tag('div', get_string('managesettingsdescription', 'local_isfdup'), array('class'=>'bg-info'));
        $html .= html_writer::table($table);
        $html .= $this->heading(get_string('addnewsetting', 'local_isfdup'));

        ob_start();
        $form->display();
        $html .= ob_get_contents();
        ob_end_clean();

        $html .= $this->footer();

        return $html;
    }

    /**
     * Print the master course options
     *
     * @return string  HTML to be output.
     */
    public function print_master_course_selection() {
        $courses = local_isfdup_get_courses();

        $strunmakemaster = get_string('unmakemaster', 'local_isfdup');
        $strmakemaster = get_string('makemaster', 'local_isfdup');
        $iconunmakemaster = $this->pix_icon('t/locked', $strunmakemaster, 'moodle', array('class'=>'iconsmall', 'title'=>$strunmakemaster));
        $iconmakemaster = $this->pix_icon('t/unlocked', $strmakemaster, 'moodle', array('class'=>'iconsmall', 'title'=>$strmakemaster));
        $sesskey = sesskey();

        $table = new html_table();
        $table->attributes = array('class' => 'generaltable');
        $table->head = array(get_string('courses'),
                get_string('actions', 'local_isfdup'),
                );
        foreach ($courses as $cid=>$course) {
            $row = new html_table_row();

            $cell = new html_table_cell();
            $cell->text = html_writer::link(new moodle_url('/course/view.php', array('id'=>$cid)), $course->fullname);
            $row->cells[] = $cell;

            $cell = new html_table_cell();
            if ($course->master) {
                $cell->text = html_writer::link(new moodle_url('', array('sesskey'=>$sesskey, 'cid'=>$course->id,'m'=>0)), $iconunmakemaster);
            } else {
                $cell->text = html_writer::link(new moodle_url('', array('sesskey'=>$sesskey, 'cid'=>$course->id,'m'=>1)), $iconmakemaster);
            }
            $row->cells[] = $cell;

            $table->data[] = $row;
        }
        
        $html  = $this->header();
        $html .= $this->heading(get_string('managemastercourses', 'local_isfdup'));
        $html .= html_writer::table($table);
        $html .= $this->footer();

        return $html;
    }
}
