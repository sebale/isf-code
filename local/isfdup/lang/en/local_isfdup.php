<?php
/**
 * ISF Activity Duplication
 * View the associated README.md file for more information.
 *
 * Language string definitions, lang=en
 *
 * @package    local_isfdup
 * @author     Shane Elliott (@link shane@pukunui.com)
 * @copyright  2017 Pukunui (@link pukunui.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Activity duplication';
$string['actions'] = 'Actions';
$string['addnewsetting'] = 'Add new setting';
$string['choosecourses'] = 'Choose Courses';
$string['choosemodules'] = 'Choose Modules';
$string['confirmlinkremoval'] = 'This activity is linked to a master activity and some settings may be locked so that changes in the master activity will be reflected locally. If you continue the link to the master activity will be removed.';
$string['delete'] = 'Delete';
$string['description'] = '<p>Concept: <strong>ISF</strong><br />Development: <strong>Pukunui</strong><br />Allow activities to be duplicated from master courses and maintain links between master activities and copies so that settings changes are propagated.</p>';
$string['duplicatewholesection'] = 'Duplicate whole section';
$string['duplicatewholesectiondescription'] = 'Duplicate the section title, summary and all the modules within the section. Alternatively you can select which modules to duplicate';
$string['duplicatingfromcourse'] = 'Duplicating from {$a}';
$string['duplicatemodulesto'] = 'Duplicate {$a} to ...';
$string['duplicateto'] = 'Duplicate to ...';
$string['duplicatedmodule'] = 'Duplicated module';
$string['error_unknownmodule'] = '{$a} is not a known module';
$string['ignoreexistingduplicates'] = 'Ignore existing duplicates';
$string['ignoreexistingduplicatesdescription'] = 'If selected then activities will only be duplicated if a copy does NOT already exist in the source course/s.';
$string['makemaster'] = 'Make master course';
$string['manageactivitysettings'] = 'Manage Activity Settings';
$string['managemastercourses'] = 'Manage Master Courses';
$string['managesettingsdescription'] = 'These settings will be locked in duplicated activities. Any changes to these settings in the master activities will be propagated to associated duplicated activities.';
$string['modulename'] = 'Module name';
$string['settingdeleted'] = 'Setting has been removed';
$string['settingname'] = 'Setting name';
$string['settingnamedescription'] = 'The short code name for the module setting eg "assign_name"';
$string['settingnotdeleted'] = 'Setting could not be removed';
$string['settingsaved'] = 'New whitelist setting saved';
$string['settingsaveproblem'] = 'There was a problem saving the new whitelist setting';
$string['unmakemaster'] = 'Remove master course status';
