<?php
/**
 * ISF Activity Duplication
 * View the associated README.md file for more information.
 *
 * Local library functions
 *
 * @package    local_isfdup
 * @author     Shane Elliott (@link shane@pukunui.com)
 * @copyright  2017 Pukunui (@link pukunui.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/backup/util/includes/backup_includes.php');
require_once($CFG->dirroot . '/backup/util/includes/restore_includes.php');


/**
 * A helper function to separate the module anem from the setting.
 * We also do a database check and only return validated name.
 *
 * @param string $setting
 * @return string
 */
function local_isfdup_get_modulename($setting) {
    global $DB;

    $parts = explode('_', $setting);
    return ($DB->record_exists('modules', array('name'=>$parts[0]))) ? $parts[0] : '';
}

/**
 * Retrieve simplifed list of settings
 *
 * @param integer $moduleid  (optional) module id
 * @param boolean $stripprefix  (optional) remove the module name
 * @return array
 */
function local_isfdup_get_simplified_settings($moduleid=0, $stripprefix=false) {
    $settings = array();
    if ($fullsettings = local_isfdup_get_whitelist_settings($moduleid)) {
        foreach ($fullsettings as $id=>$rec) {
           if (!empty($moduleid) and $stripprefix) {
                $settings[$id] = preg_replace("/^{$rec->module}_/", '', $rec->setting);
            } else {
                $settings[$id] = $rec->setting;
            }
        }
    }
    return $settings;
}

/**
 * Retrieve the white-listed module settings
 *
 * @param integer $moduleid  (optional) module id
 * @return array
 */
function local_isfdup_get_whitelist_settings($moduleid=0) {
    global $DB;

    $modules = array();

    $params = array();
    $sql = "SELECT s.id, s.moduleid, m.name AS module, s.name AS setting
            FROM {local_isfdup_settings} s
            JOIN {modules} m ON m.id=s.moduleid";
    if (!empty($moduleid)) {
        $sql .= " WHERE s.moduleid = ?";
        $params[] = $moduleid;
    }
    $sql .= " ORDER BY s.moduleid, setting";

    return $DB->get_records_sql($sql, $params);
}

/**
 * Change the master course status
 *
 * @param integer $courseid  id of course
 * @param integer $status  new status where 1 == master course
 * @return boolean  success?
 */
function local_isfdup_change_master_status($courseid, $status) {
    global $DB;

    $return = true;
    $newstatus = ($status) ? 1 : 0; // normalise value;
    if ($record = $DB->get_record('local_isfdup_courses', array('courseid'=>$courseid))) {
        // Check if an update is waranted.
        if ($record->master != $newstatus) {
            unset($record->courseid);
            $record->master = $newstatus;
            $return = $DB->update_record('local_isfdup_courses', $record);
        }
    } else if ($newstatus) {
        $record = new stdClass;
        $record->courseid = $courseid;
        $record->master = $newstatus;
        $return = $DB->insert_record('local_isfdup_courses', $record);
    }
    return $return;
}

/**
 * Get a list of courses with master flag
 *
 * @return array
 */
function local_isfdup_get_courses() {
    global $DB;

    $sql = "SELECT c.id, c.fullname, COALESCE(ic.master, 0) AS master
            FROM {course} c
            LEFT JOIN {local_isfdup_courses} ic ON ic.courseid=c.id";

    return $DB->get_records_sql($sql);
}

/**
 * Given a course section id, return a list of course modules
 *
 * @param integer $sid  section id number
 * @return array  cmid=>name
 */
function local_isfdup_get_section_modules($sid) {
    global $DB;

    $cms = array();
    if ($section = $DB->get_record('course_sections', array('id'=>$sid), 'id, course, sequence', MUST_EXIST)) {
        if (!empty($section->sequence)) {
            $modinfo = get_fast_modinfo($section->course);
            $cmids = explode(',', $section->sequence);
            foreach ($cmids as $cmid) {
                $cm = $modinfo->get_cm($cmid);
                $cms[$cmid] = $cm->name;
            }
        }
    }
    return $cms;
}

/**
 * Link two course modules together
 *
 * @param cm_info $sourcecm  source course module
 * @param cm_info $destcm  destination course module
 * @return boolean  success?
 */
function local_isfdup_link_modules($sourcecm, $destcm) {
    global $USER, $DB;

    $record = new stdClass;
    $record->sourcecmid = $sourcecm->id;
    $record->destinationcmid = $destcm->id;
    $record->moduleid = $sourcecm->module;
    $record->sourcecourseid = $sourcecm->course;
    $record->destinationcourseid = $destcm->course;
    $record->userid = $USER->id;
    $record->timecreated = time();

    return $DB->insert_record('local_isfdup', $record);
}

/**
 * Return a list of courses for which the givenuser has editing rights
 *
 * @param integer $userid (optional)  user id
 * @return array  courseid=>course fullname
 */
function local_isfdup_get_external_courses($userid=0) {
    global $USER, $DB;

    if (empty($userid)) {
        $userid = $USER->id;
    }

    $sql = "SELECT x.id, x.fullname
            FROM (
                SELECT c.id, c.fullname, COALESCE(ic.master, 0) AS master
                FROM {course} c
                LEFT JOIN {local_isfdup_courses} ic ON ic.courseid=c.id
                ) x
            WHERE x.master=0";
    $rcourses = $DB->get_records_sql($sql);

    // Cycle through checking capabilities.
    // TODO create a single database query to do all of this.
    // TODO session cache results
    $courses = array();
    foreach ($rcourses as $rc) {
        if (has_capability('moodle/course:update', context_course::instance($rc->id), $userid)) {
            $courses[$rc->id] = $rc->fullname;
        }
    }

    return $courses;
}

/**
 * Copy a topic section to external courses.
 *
 * To get here we assume the section is to be created in destination
 * courses before the cms are copied over.
 *
 * @param integer $sid      section id form {course_sections}
 * @param array $courseids  destination course ids
 * @param boolean $ignoreexisting  (optional) ignore modules where a linked copy already exists
 * @return integer          number of course sections duplicated
 */
function local_isfdup_duplicate_section_to_courses($sid, $courseids, $ignoreexisting=false) {
    $count = 0;
    foreach ($courseids as $courseid) {
        $count += (int)local_isfdup_duplicate_section($sid, $courseid, $ignoreexisting);
    }
    return $count;
}

/**
 * Copy module instances to external courses
 *
 * TODO Do this in a more efficient way than double loop the import process
 *
 * @param array cmids  course module ids
 * @param array courseids  destination course ids
 * @return integer  number of duplicates
 */
function local_isfdup_duplicate_cms($cmids, $courseids) {
    $duplicates = 0;
    foreach ($cmids as $cmid) {
        foreach ($courseids as $courseid) {
            if (local_isfdup_duplicate_cm($cmid, $courseid)) {
                $duplicates++;
            }
        }
    }
    return $duplicates;
}

/**
 * Copy a module instance to an external course
 *
 * @param integer cmid  course module id
 * @param integer courseid  destination course id
 * @return mixed  new course module id or false
 */
function local_isfdup_duplicate_cm($cmid, $courseid) {
    global $USER, $DB;

    // Load up the course module object.
    $cm = get_coursemodule_from_id('', $cmid, 0, true, MUST_EXIST);
    $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);

    // Check the module supports backup/restore.
    $a          = new stdClass();
    $a->modtype = get_string('modulename', $cm->modname);
    $a->modname = format_string($cm->name);
    if (!plugin_supports('mod', $cm->modname, FEATURE_BACKUP_MOODLE2)) {
        throw new moodle_exception('duplicatenosupport', 'error', '', $a);
    }

    // Perform the backup.
    $bc = new backup_controller(backup::TYPE_1ACTIVITY, $cmid, backup::FORMAT_MOODLE,
            backup::INTERACTIVE_NO, backup::MODE_IMPORT, $USER->id);
    $backupid       = $bc->get_backupid();
    $backupbasepath = $bc->get_plan()->get_basepath();
    $bc->execute_plan();
    $bc->destroy();


    // Restore the backup
    $rc = new restore_controller($backupid, $course->id,
            backup::INTERACTIVE_NO, backup::MODE_IMPORT, $USER->id, backup::TARGET_CURRENT_ADDING);

    $cmcontext = context_module::instance($cm->id);
    if (!$rc->execute_precheck()) {
        $precheckresults = $rc->get_precheck_results();
        if (is_array($precheckresults) && !empty($precheckresults['errors'])) {
            if (empty($CFG->keeptempdirectoriesonbackup)) {
                fulldelete($backupbasepath);
            }
        }
    }

    $rc->execute_plan();

    // Now a bit hacky part follows - we try to get the cmid of the newly
    // restored copy of the module.
    $newcmid = null;
    $tasks = $rc->get_plan()->get_tasks();
    foreach ($tasks as $task) {
        if (is_subclass_of($task, 'restore_activity_task')) {
            if ($task->get_old_contextid() == $cmcontext->id) {
                $newcmid = $task->get_moduleid();
                break;
            }
        }
    }

    // If we know the cmid of the new course module, let us move it
    // right below the original one. otherwise it will stay at the
    // end of the section.
    if ($newcmid) {
        $info = get_fast_modinfo($course);
        $newcm = $info->get_cm($newcmid);

        // Update calendar events with the duplicated module.
        $refresheventsfunction = $newcm->modname . '_refresh_events';
        if (function_exists($refresheventsfunction)) {
            call_user_func($refresheventsfunction, $newcm->course);
        }

        // Trigger course module created event. We can trigger the event only if we know the newcmid.
        $event = \core\event\course_module_created::create_from_cm($newcm);
        $event->trigger();

        // Link modules together.
        local_isfdup_link_modules($cm, $newcm);
    }
    rebuild_course_cache($newcm->course);

    // SEBALE copy activity images
    \format_isf\hooks::duplicate_activityimage($cm, $newcm);

    $rc->destroy(); // Testing this on the outside of the loop.

    if (empty($CFG->keeptempdirectoriesonbackup)) {
        fulldelete($backupbasepath);
    }

    return isset($newcm) ? $newcm : null;
}

/**
 * Copy a section instance to an external course
 *
 * @param integer $sid             section  id
 * @param integer $courseid        destination course id
 * @param boolean $ignoreexisting  (optional) ignore modules where a linked copy already exists
 * @return boolean                 success?
 */
function local_isfdup_duplicate_section($sid, $courseid, $ignoreexisting=false) {
    global $USER, $DB;

    // Load up the section, course and course module objects.
    $oldsection = $DB->get_record('course_sections', array('id'=>$sid), '*', MUST_EXIST);
    $oldcourse  = $DB->get_record('course', array('id'=>$oldsection->course), '*', MUST_EXIST);
    $newcourse  = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);

    // Load up the old course module info using the context id as the array key.
    // We need to search by context id later on.
    $oldcms = array();
    $oldcmids = explode(',', $oldsection->sequence);
    foreach ($oldcmids as $oldcmid) {
        $tmpcm = get_coursemodule_from_id('', $oldcmid, 0, true, MUST_EXIST);
        $tmpcm->context = context_module::instance($oldcmid);
        $oldcms[$tmpcm->context->id] = $tmpcm;
    }

    // Perform the backup.
    $bc = new backup_controller(backup::TYPE_1SECTION, $sid, backup::FORMAT_MOODLE,
            backup::INTERACTIVE_NO, backup::MODE_IMPORT, $USER->id);
    $backupid       = $bc->get_backupid();
    $backupbasepath = $bc->get_plan()->get_basepath();
    $bc->execute_plan();
    $bc->destroy();


    // Restore the backup
    $rc = new restore_controller($backupid, $newcourse->id,
            backup::INTERACTIVE_NO, backup::MODE_IMPORT, $USER->id, backup::TARGET_CURRENT_ADDING);

    if (!$rc->execute_precheck()) {
        $precheckresults = $rc->get_precheck_results();
        if (is_array($precheckresults) && !empty($precheckresults['errors'])) {
            if (empty($CFG->keeptempdirectoriesonbackup)) {
                fulldelete($backupbasepath);
            }
        }
    }

    $rc->execute_plan();


    // Now we need to deal with the new course modules.
    $newcourseinfo = get_fast_modinfo($newcourse);
    $tasks = $rc->get_plan()->get_tasks();

    $newcmcount = 0;
    foreach ($tasks as $task) {
        if (is_subclass_of($task, 'restore_activity_task')) {
            $oldcontextid = $task->get_old_contextid();
            if (!empty($oldcms[$oldcontextid])) {
                $newcmcount++;

                $newcm = $newcourseinfo->get_cm($task->get_moduleid());

                // Update calendar events with the duplicated module.
                $refresheventsfunction = $newcm->modname . '_refresh_events';
                if (function_exists($refresheventsfunction)) {
                    call_user_func($refresheventsfunction, $newcm->course);
                }

                // Trigger course module created event. We can trigger the event only if we know the newcmid.
                $event = \core\event\course_module_created::create_from_cm($newcm);
                $event->trigger();

                // Link modules together.
                local_isfdup_link_modules($oldcms[$oldcontextid], $newcm);

                // SEBALE copy activity images
                \format_isf\hooks::duplicate_activityimage($oldcms[$oldcontextid], $newcm);
            }
        }
    }

    rebuild_course_cache($courseid);

    $rc->destroy(); // Testing this on the outside of the loop.

    if (empty($CFG->keeptempdirectoriesonbackup)) {
        fulldelete($backupbasepath);
    }

    return true;
}
