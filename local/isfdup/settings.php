<?php
/**
 * ISF Activity Duplication
 * View the associated README.md file for more information.
 *
 * Settings page
 *
 * @package    local_isfdup
 * @author     Shane Elliott (@link shane@pukunui.com)
 * @copyright  2017 Pukunui (@link pukunui.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$ADMIN->add('localplugins', new admin_externalpage('local_isfdup', get_string('pluginname', 'local_isfdup'), $CFG->wwwroot.'/local/isfdup/index.php'));

$settings = null;
