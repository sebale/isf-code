<?php
/**
 * ISF Activity Duplication
 * View the associated README.md file for more information.
 *
 * This is the main duplication control page
 *
 * @package    local_isfdup
 * @author     Shane Elliott (@link shane@pukunui.com)
 * @copyright  2017 Pukunui (@link pukunui.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../config.php');
require_once($CFG->dirroot.'/local/isfdup/locallib.php');
require_once($CFG->dirroot.'/local/isfdup/classes/forms.php');

$courseid = required_param('id', PARAM_INT); // Course id.
$cmid     = optional_param('cm', 0, PARAM_INT); // Course module id.
$sid      = optional_param('ts', 0, PARAM_INT); // Topic section id.

//TODO confirm sesskey

$url = new moodle_url('/local/isfdup/dupto.php');
$url->param('id', $courseid);
$url->param('cm', $cmid);
$url->param('ts', $sid);
$courseurl = new moodle_url('/course/view.php');
$courseurl->param('id', $courseid);

// Load up the course object.
if (!($course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST))) {
    die('Course not found'); //TODO some better error handling than this.
}

require_login($course);

// Load up course module if applicable.
if (!empty($cmid)) {
    $cm = get_coursemodule_from_id('', $cmid, 0, true, MUST_EXIST);
    $dupname = $cm->modname;
    $section = null;
} else if (!empty($sid)) {
    $section = $DB->get_record('course_sections', array('id' => $sid), '*', MUST_EXIST);
    $dupname = (empty($section->name)) ? 'Section' : $section->name;
} else {
    // Houston we have a problem!
    die('Either cm or ts should be set'); //TODO some better error handling than this.
}


$title = get_string('duplicatemodulesto', 'local_isfdup', $dupname);
$coursecontext = context_course::instance($courseid);

$PAGE->set_url($url);
$PAGE->set_pagetype('mod');
$PAGE->set_context($coursecontext);
$PAGE->set_pagelayout('admin');
$PAGE->set_heading(get_string('duplicatingfromcourse', 'local_isfdup', $course->fullname));
$PAGE->set_title($title);
$PAGE->set_cacheable(false);
// TODO Add navbar navigation string

$formcustomdata = array('courses' => local_isfdup_get_external_courses(),
                        'section' => $section);
$coursesearchform = new local_isfdup_coursesearchform(null, $formcustomdata);

// If form cancelled then simply redirect back to course view page.
if ($coursesearchform->is_cancelled()) {
    redirect($courseurl);
    exit;
}

// If data submitted then process accordingly.
if ($data = $coursesearchform->get_data()) {
    // Get the data into a manageable format.
    $cmids = array();
    $courseids = array();
    foreach ($data as $id=>$value) {
        if (preg_match('@^cid_(.*)$@i', $id, $matches)) {
            $courseids[] = $matches[1];
        }
        if (preg_match('@^cmid_(.*)$@i', $id, $matches)) {
            $cmids[] = $matches[1];
        }
    }

    // Now let's work out what we're duplicating to where?
    if (!empty($data->cm) and !empty($courseids)) {
        $cmids = array($data->cm);
        $copies = local_isfdup_duplicate_cms($cmids, $courseids);
    } else if (!empty($data->ts) and !empty($data->wholesection) and !empty($courseids)) {
        $ignoreexisting = (!empty($data->ignoreexisting));
        $copies = local_isfdup_duplicate_section_to_courses($data->ts, $courseids, $ignoreexisting);
    } else if (!empty($cmids) and !empty($courseids)) {
        $copies = local_isfdup_duplicate_cms($cmids, $courseids);
    } else {
        $copies = 0;
    }
    
    redirect($courseurl, "$copies duplicates done");
    exit;
}

$data = array('cm' => $cmid,
              'ts' => $sid,
              'id' => $courseid
             );
$coursesearchform->set_data($data);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);
$coursesearchform->display();
echo $OUTPUT->footer();
