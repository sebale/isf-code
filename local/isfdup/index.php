<?php
/**
 * ISF Activity Duplication
 * View the associated README.md file for more information.
 *
 * Main landing page
 *
 * @package    local_isfdup
 * @author     Shane Elliott (@link shane@pukunui.com)
 * @copyright  2017 Pukunui (@link pukunui.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../config.php');
require_once($CFG->libdir . '/adminlib.php');

admin_externalpage_setup('local_isfdup');

$url = new moodle_url('/local/isfdup/index.php');
$systemcontext = context_system::instance();
$strpluginname = get_string('pluginname', 'local_isfdup');


$out = html_writer::start_tag('ul');
$out.= html_writer::start_tag('li');
$out.= html_writer::link(new moodle_url('/local/isfdup/managemasters.php'),
                         new lang_string('managemastercourses', 'local_isfdup'));
$out.= html_writer::end_tag('li');
$out.= html_writer::start_tag('li');
$out.= html_writer::link(new moodle_url('/local/isfdup/managesettings.php'),
                         new lang_string('manageactivitysettings', 'local_isfdup'));
$out.= html_writer::end_tag('li');
$out.= html_writer::end_tag('ul');

echo $OUTPUT->header();
echo $OUTPUT->heading($strpluginname);

echo $OUTPUT->notification(get_string('description', 'local_isfdup'), 'info');
echo $out;

echo $OUTPUT->footer();
