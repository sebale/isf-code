<?php
/**
 * ISF Activity Duplication
 * View the associated README.md file for more information.
 *
 * A simple pass through script to perform an action and return to a given page.
 * TODO: If a confirmation with sesskey is not received then display a
 * relevant confirmation link.
 *
 * @package    local_isfdup
 * @author     Shane Elliott (@link shane@pukunui.com)
 * @copyright  2017 Pukunui (@link pukunui.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../config.php');
require_once($CFG->dirroot.'/local/isfdup/locallib.php');

$action   = required_param('a', PARAM_ALPHA); // Action.
$cmid     = optional_param('cm', 0, PARAM_INT); // Course module id.
$confirm  = optional_param('confirm', false, PARAM_BOOL); // Confirm action.


if ($confirm and confirm_sesskey()) {
    switch ($action) {
        // Remove course module link to master.
        case 'rmcmln':
            if ($courseid = $DB->get_field('course_modules', 'course', array('id'=>$cmid))) {
                \local_isfdup\hooks::remove_course_module_link($cmid);
                redirect(new moodle_url('/course/view.php', array('id'=>$courseid)));
                exit;
            }
            break;

        // Remove course module link to master and redirect back to cm edit.
        case 'rmcmlnem':
            if ($courseid = $DB->get_field('course_modules', 'course', array('id'=>$cmid))) {
                \local_isfdup\hooks::remove_course_module_link($cmid);
                redirect(new moodle_url('/course/mod.php', array('sesskey'=>sesskey(),
                                                                 'update'=>$cmid,
                                                                 'sr'=>0
                                                                )));
                exit;
            }
            break;

        default:
    }
}

redirect($CFG->wwwroot);

