// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is an empty module, that is required before all other modules.
 * Because every module is returned from a request for any other module, this
 * forces the loading of all modules with a single request.
 *
 * This function also sets up the listeners for ajax requests so we can tell
 * if any requests are still in progress.
 *
 * @package    local_quickadd
 * @module     local_quickadd/quickadd
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define(['jquery', 'core/ajax', 'core/log', 'local_quicklinks/sortable', 'core/modal_factory'], function($, ajax, log, Sortable, ModalFactory) {

    Sortable.init($, window, 'sortable', undefined);

    var QuickAdd = {

        save: function(eid, new_eid){

            ajax.call([{
                methodname: 'local_quickadd_save_ordering', args: { move: eid, moveafter: new_eid }
            }])[0]
            .done(function(response) {
              try {
                response = JSON.parse(response);
                // maybe add here notifications
              } catch (Error){
                log.debug(Error.message);
              }
            }).fail(function(ex) {
              log.debug(ex.message);
            });
        },

        init: function() {

            var group = $('.sorting > tbody').sortable({
                containerSelector: '.sorting > tbody',
                itemSelector: 'tr',
                group: 'sort_1',
                handle: 'i.move-action',
                placeholder: '<tr class=\"placeholder\"><td colspan="4"></td></tr>',
                onDrag: function (item, group, _super) {
                    item.addClass('active');
                    $('table.sorting tr').addClass('not-active');
                },
                onDrop: function (item, container, _super) {
                    $('table.sorting tr').removeClass('not-active');
                    item.removeClass('active');
                    newIndex = item.index()
                    var eid = item.attr('class').replace(" dragged", "");
                    var prep = newIndex;
                    if (prep > 0){
                        var row = $('table.sorting tr').eq(prep);
                        var new_eid = parseInt(row.attr('class'));
                    } else {
                        new_eid = 0;
                    }

                    if (new_eid || newIndex == 0){
                        QuickAdd.save(eid, new_eid);
                    }
                    _super(item, container);
                },
                onCancel: function (item, container, _super, event) {
                    $('table.sorting tr').removeClass('not-active');
                    item.removeClass('active');
                    _super(item)
                },
            });

        },

        edit: function(str){
            $('#id_submitbutton2').val(str);
        },
    };

    /**
    * @alias module:local_quicklinks/qlinks
    */
    return QuickAdd;

});

