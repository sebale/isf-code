<?php

define('AJAX_SCRIPT', true);

require_once('../../config.php');
require_once($CFG->dirroot.'/local/quickadd/lib.php');

$id         = optional_param('id', 0, PARAM_INT);
$courseid   = optional_param('courseid', 0, PARAM_INT);
$section    = optional_param('sectionid', 0, PARAM_INT);
$cmid       = optional_param('cmid', 0, PARAM_INT);
$action     = optional_param('action', '', PARAM_RAW);
$form       = (object)optional_param_array('form', array(), PARAM_RAW);

if ($action == 'get_module_form' and $id > 0) {
    $output = '';

    $qmodule = $DB->get_record_sql("SELECT q.*, m.name as modname, cm.instance
                                      FROM {local_quickmodules} q
                                 LEFT JOIN {course_modules} cm ON cm.id = q.cmid
                                 LEFT JOIN {modules} m ON m.id = cm.module
                                     WHERE cm.id = :id", array('id'=>$id));
    if (!$qmodule) die('wrong module id');

    $module = $DB->get_record($qmodule->modname, array('id'=>$qmodule->instance));

    $fields = local_quickadd_get_fields($qmodule->modname);

    if (count($fields)) {
        $output .= html_writer::empty_tag("input", array('type'=>'hidden', 'name'=>'id', 'value'=>$id));
        $output .= html_writer::empty_tag("input", array('type'=>'hidden', 'name'=>'courseid', 'value'=>$courseid));
        $output .= html_writer::empty_tag("input", array('type'=>'hidden', 'name'=>'section', 'value'=>$section));
        $output .= html_writer::empty_tag("input", array('type'=>'hidden', 'name'=>'cmid', 'value'=>$cmid));

        foreach ($fields as $fieldname => $field) {

            $output .= html_writer::start_tag("div", array('class'=>'form-field-item'));
            $output .= html_writer::tag("label", $field['title'], array('for'=>'form['.$fieldname.']'));

            $value = (isset($module->{$fieldname})) ? html_entity_decode($module->{$fieldname}) : '';

            if ($field['type'] == 'textarea'){
                $output .= html_writer::tag("textarea", $value, array('name' => 'form['.$fieldname.']', 'class' => 'form-control field-'.$fieldname));
            } else {
                $output .= html_writer::empty_tag("input", array('name' => 'form['.$fieldname.']', 'class' => 'form-control field-'.$fieldname, 'type'=>'text', 'value'=>$value));
            }

            $output .= html_writer::end_tag("div");
        }
    }


    echo $output;
}

exit;
