<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Displays information about all the assignment modules in the requested course
 *
 * @package   local_quickadd
 * @copyright 2017 ISF
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once("../../config.php");
require_once("lib.php");
require_once("locallib.php");
require_once("classes/tables.php");

require_login();

$id         = optional_param('id', 0, PARAM_INT);
$search     = optional_param('search', '', PARAM_CLEAN);

$title = get_string('pluginname', 'local_quickadd');

$course = $DB->get_record('course', array('id'=>1));
$context = context_system::instance();

require_capability('local/quickadd:manage', $context);
$PAGE->set_context($context);
$PAGE->set_url(new moodle_url("/local/quickadd/index.php"));
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_pagelayout('standard');
$PAGE->set_heading($title);

$table = new modules_table('modules_table', $search);
$table->attributes['class'] .= ' sorting';
$table->is_collapsible = false;
$table->sortable(true, 'sortorder');

$renderer = $PAGE->get_renderer('local_quickadd');

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo $renderer->startbox('quickmodules-list');
echo $renderer->search_form($course);
$table->out(50, true);
echo $renderer->endbox();

$PAGE->requires->js_call_amd('local_quickadd/quickadd', 'init', array() );

echo $OUTPUT->footer();
