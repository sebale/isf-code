<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * ISF quickadd lang file
 *
 * @package    local_quickadd
 * @copyright  ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

$string['pluginname'] = 'Quick Modules';
$string['quickadd:add'] = 'Quick Add View';
$string['quickadd:manage'] = 'Manage Quick modules';
$string['quickadd:seeallmodules'] = 'Can see all modules';
$string['search'] = 'Search...';
$string['requiredfield'] = 'This field is required';
$string['creator'] = 'Creator';
$string['actions'] = 'Actions';
$string['confirmdelete'] = 'Are you sure want to delete Quick module "{$a}"';
$string['quickaddcreated'] = 'Quick Module created';
$string['quickaddupdated'] = 'Quick Module updated';
$string['createnew'] = 'Create New Quick module';
$string['creatingnew'] = 'Creating New Quick module {$a}';
$string['updating'] = 'Updating Quick module {$a}';
$string['editingquickmodule'] = 'Editing Quick module';
$string['update'] = 'Update Quick module';
$string['deletemodule'] = 'Delete Quick Module';
$string['deleted'] = 'Quick Module Deleted';
$string['manage'] = 'Manage';
$string['managequickadd'] = 'Manage Quick Modules';
$string['title'] = 'Title';
$string['module'] = 'Module';
$string['save'] = 'Save';

