<?php

$string['pluginname'] = '快捷模組（Quick Modules）';
$string['quickadd:add'] = '快捷增加視圖';
$string['quickadd:manage'] = '管理快捷模組（Quick modules）';
$string['quickadd:seeallmodules'] = '可視所有模組';
$string['search'] = '搜尋...';
$string['requiredfield'] = '需要這個範圍';
$string['creator'] = '創建者';
$string['actions'] = '指令';
$string['confirmdelete'] = '你確定要刪除快捷模組嗎 ""{$a}""';
$string['quickaddcreated'] = '已創建的快捷模組';
$string['quickaddupdated'] = '已更新的快捷模組';
$string['createnew'] = '創建新的快捷模組';
$string['creatingnew'] = '正在創建新的快捷模組 {$a}';
$string['updating'] = '正在更新快捷模組 {$a}';
$string['editingquickmodule'] = '修改快捷模組';
$string['update'] = '更新快捷模組';
$string['deletemodule'] = '刪除快捷模組';
$string['deleted'] = '已刪除的快捷模組';
$string['manage'] = '管理';
$string['managequickadd'] = '管理快捷模組';
$string['title'] = '標題';
$string['module'] = '模組';
$string['save'] = '儲存';
