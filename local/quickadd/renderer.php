<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Displays information about all the assignment modules in the requested course
 *
 * @package   local_quickadd
 * @copyright 2017 ISF
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class local_quickadd_renderer extends plugin_renderer_base {

    // Print box start
    public function startbox($boxclass = '') {
        return html_writer::start_tag('div', array('class'=>'content-wrapper '.$boxclass));
    }

    // Print box end
    public function endbox() {
        return html_writer::end_tag('div');
    }

    public function search_form($course) {
        global $PAGE, $CFG, $OUTPUT;

        $course_renderer = $PAGE->get_renderer('course');

        $output = '';

        $output .= html_writer::start_tag("div",  array('class'=>'search-form mform clearfix'));

        $modnames = get_module_types_names();
        $modules = get_module_metadata($course, $modnames, null);

        if (has_capability('local/quickadd:add', context_system::instance())){

            // Make sure section with number 1 exists.
            course_create_sections_if_missing($course, 1);
            // Re-request modinfo in case section was created.
            $modinfo = get_fast_modinfo($course);

            $output .= $OUTPUT->box_start('generalbox sitetopic');

            $section = $modinfo->get_section_info(1);

            $courserenderer = $PAGE->get_renderer('core', 'course');

            $span = html_writer::tag('span', get_string('createnew', 'local_quickadd'), array('class' => 'section-modchooser-text'));
            $modchooser = html_writer::tag('span', $span, array('class' => 'section-modchooser-link btn btn-warning'));
            $modchooser = html_writer::tag('div', $modchooser, array('class' => 'visibleifjs addresourcemodchooser'));
            $output .= $this->course_modchooser($modules, $course) . $modchooser;

            $output .= $OUTPUT->box_end();

        }

        $output .= html_writer::end_tag("div");

        return $output;
    }

     /**
     * Build the HTML for the module chooser javascript popup
     *
     * @param array $modules A set of modules as returned form @see
     * get_module_metadata
     * @param object $course The course that will be displayed
     * @return string The composed HTML for the module
     */
    public function course_modchooser($modules, $course) {
        if (!$this->page->requires->should_create_one_time_item_now('core_course_modchooser')) {
            return '';
        }
        $modchooser = new \core_course\output\modchooser($course, $modules);
        return $this->render($modchooser);
    }

    /**
     * Render a modchooser.
     *
     * @param renderable $modchooser The chooser.
     * @return string
     */
    public function render_modchooser(\renderable $modchooser) {
        global $CFG;

        $data = $modchooser->export_for_template($this);
        $currentlang = current_language();

        $items_cat = array();
        foreach($data->sections as &$section){
            foreach($section->items as $item){
                if($currentlang == 'zh_tw' || $currentlang == 'zh_cn'){
                    $help_link = get_config('local_manager', 'chooser_mod_help_ch_' . $item->id);
                    $example_link = get_config('local_manager', 'chooser_mod_example_ch_' . $item->id);
                    $cat = get_config('local_manager', 'chooser_mod_cat_ch_'.$item->id);
                }else{
                    $help_link = get_config('local_manager', 'chooser_mod_help_' . $item->id);
                    $example_link = get_config('local_manager', 'chooser_mod_example_' . $item->id);
                    $cat = get_config('local_manager', 'chooser_mod_cat_'.$item->id);
                }

                $item->color = get_config('local_manager', 'chooser_mod_color_'.$item->id);
                $item->help_link = (!empty($help_link))?html_writer::link($help_link,get_string('help'),array('target'=>'_blank')):'';
                $item->example_link = (!empty($example_link))?html_writer::link($example_link,get_string('example','local_manager'),array('target'=>'_blank')):'';
                $item->description = strip_tags($item->description);
                $item->description = str_replace('"',"'",$item->description);
                $item->value = $CFG->wwwroot.'/local/quickadd/modedit.php?add='.$item->id;

                $cat = (empty($cat))?' '.get_string('other'):$cat;
                if(!isset($items_cat[$cat])){
                    $items_cat[$cat] = new stdClass();
                    $items_cat[$cat]->name = $cat;
                    $items_cat[$cat]->only_advanced = true;
                }

                if(get_config('local_manager', 'chooser_mod_advanced_'.$item->id)){
                    $item->advanced = true;
                }else{
                    $items_cat[$cat]->only_advanced = false;
                }



                $items_cat[$cat]->items[] = $item;
            }
        }
        asort($items_cat);
        $section = new stdClass();
        $section->cat = array_values($items_cat);
        $data->sections = array($section);
        $data->resources = get_string('resources');
        $data->activities = get_string('activities');

        return $this->render_from_template('core_course/isf_modchoser', $data);
    }

    public function modalForm($course) {
        global $PAGE, $CFG, $OUTPUT, $DB;

        require_once($CFG->dirroot.'/local/quickadd/lib.php');
        $qmodules = local_quickadd_get_modules();

        $systemcourse = $DB->get_record('course', array('id'=>1));
        $modinfo = get_fast_modinfo($systemcourse);
        $modules = array();

        $PAGE->requires->js_call_amd('local_quickadd/quickadd_modal', 'init', array('courseid'=>$course->id, 'formid'=>'quickadd_formbox'));

        if (count($qmodules)){
            foreach($qmodules as $item) {
                $module = array();
                $mod = $modinfo->cms[$item->cmid];

                if ($mod){
                    $module['id'] = $mod->id;
                    $module['typename'] = get_string('pluginname', 'mod_'.$item->modname);
                    $module['name'] = $mod->get_formatted_name();
                    $module['icon'] = html_writer::empty_tag('img', array('src' => $mod->get_icon_url(),  'class' => 'iconlarge activityicon', 'alt' => ' ', 'role' => 'presentation'));
                    $modules[] = $module;
                }
            }
        }

        $context = array(
            'formaction' => new moodle_url('/local/quickadd/modedit.php', array('action'=>'create_quickmodule')),
            'courseid' => $course->id,
            'modules' => $modules
        );

        return $this->render_from_template('local_quickadd/modal_form', $context);
    }

}

