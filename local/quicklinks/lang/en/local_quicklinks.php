<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * ISF quicklinks lang file
 *
 * @package    local_quicklinks
 * @copyright  ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

$string['pluginname'] = 'Quick Links';
$string['quicklinks:view'] = 'Quick Links View';
$string['quicklinks:manage'] = 'Manage Quick Links';
$string['quicklinks:editcategory'] = 'Create/Edit Quick Links Categories';
$string['quicklinks:editquicklink'] = 'Create/Edit Quick Links';
$string['quicklinks:editprivatetype'] = 'Create/Edit Private Quick Links';
$string['quicklinks:editcoursetype'] = 'Create/Edit Course Quick Links';
$string['quicklinks:editsystemtype'] = 'Create/Edit System Quick Links';
$string['quicklinks:seeallcourses'] = 'Can see all courses';
$string['search'] = 'Search...';
$string['createnewcategory'] = 'Create New Category';
$string['updatecategory'] = 'Update Category';
$string['requiredfield'] = 'This field is required';
$string['title'] = 'Title';
$string['type'] = 'Type';
$string['link'] = 'Link';
$string['selecttype'] = 'Select type';
$string['create'] = 'Create';
$string['save'] = 'Save';
$string['privatetype'] = 'Private';
$string['coursetype'] = 'Course';
$string['systemtype'] = 'System';
$string['selectcourse'] = 'Select Course';
$string['course'] = 'Course';
$string['creator'] = 'Creator';
$string['links'] = 'Links';
$string['actions'] = 'Actions';
$string['categoryupdated'] = 'Category updated';
$string['categorydeleted'] = 'Category deleted';
$string['deletecategory'] = 'Delete Category';
$string['confirmcategorydelete'] = 'Are you sure want to delete category "{$a}"';
$string['categories'] = 'Categories';
$string['createnew'] = 'Create New';
$string['qlinkcreated'] = 'Quick link created';
$string['qlinkupdated'] = 'Quick link updated';
$string['createnewlink'] = 'Create New Quick Link';
$string['updateqlink'] = 'Update Quick Link';
$string['selecticon'] = 'Select an icon';
$string['fa_info'] = '<a href="http://fontawesome.io/icons/" target="_blank">Click here to view icons</a><br />*Use the Font Awesome code for the icon selected. For example, fa-book.';
$string['optionalimage'] = 'Optional Image';
$string['deleteqlink'] = 'Delete Quick Link';
$string['qlinkdeleted'] = 'Quick Link Deleted';
$string['confirmcdeleteqlink'] = 'Are you sure want to delete Quick Link "{$a}"';
$string['togglequicklinks'] = 'Toggle Quick Links';
$string['manage'] = 'Manage';
$string['managequicklinks'] = 'Manage Quick Links';
