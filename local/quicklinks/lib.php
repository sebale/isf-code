<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Displays information about all the assignment modules in the requested course
 *
 * @package   local_quicklinks
 * @copyright 2017 ISF
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

define('LOCAL_QUICKLINKS_PRIVATETYPE', 0);
define('LOCAL_QUICKLINKS_COURSETYPE', 1);
define('LOCAL_QUICKLINKS_SYSTEMTYPE', 2);


function local_quicklinks_insert_category($data, $context = null){
    global $DB, $USER;

    $last_category = local_quicklinks_get_last_category($context);

    $data->userid = $USER->id;
    $data->state = 1;
    $data->timemodified = time();

    $data->sortorder = ($last_category->last) ? $last_category->last+1 : 1;
    $data->id = $DB->insert_record("local_quicklinks_categories", $data);

    return $data->id;
}

function local_quicklinks_update_category($data, $context = null){
    global $DB;

    $data->timemodified = time();
    $DB->update_record("local_quicklinks_categories", $data);
}

function local_quicklinks_delete_category($category) {
    global $DB, $USER;
    $DB->delete_records('local_quicklinks', array('category'=>$category->id));
    $DB->delete_records('local_quicklinks_categories', array('id'=>$category->id));
}

function local_quicklinks_insert($data) {
    global $DB, $USER;

    $last_quicklink = $DB->get_record_sql("SELECT MAX(sortorder) as last FROM {local_quicklinks} WHERE category = :category", array('category'=>$data->cid));

    $data->userid = $USER->id;
    $data->state = 1;
    $data->category = $data->cid;
    $data->timemodified = time();

    $data->sortorder = ($last_quicklink->last) ? $last_quicklink->last+1 : 1;
    $data->id = $DB->insert_record("local_quicklinks", $data);

    return $data->id;
}

function local_quicklinks_update($data) {
    global $DB;

    $data->timemodified = time();
    $DB->update_record("local_quicklinks", $data);
}

function local_quicklinks_delete($qlink) {
    global $DB;
    $DB->delete_records('local_quicklinks', array('id'=>$qlink->id));
}


/**
 * Renders the popup.
 *
 * @param renderer_base $renderer
 * @return string The HTML
 */
function local_quicklinks_render_navbar_output(\renderer_base $renderer) {
    global $USER, $CFG;
    $output = '';

    if (!isloggedin()){
        return $output;
    }

    $context = [];
    $output .= $renderer->render_from_template('local_quicklinks/quicklinks_popover', $context);

    return $output;
}

function local_quicklinks_getqlinks(){
    global $DB, $CFG, $USER;
    $result = array(); $courses_params = array();
    $params = array('userid'=>$USER->id, 'private'=>LOCAL_QUICKLINKS_PRIVATETYPE, 'course'=>LOCAL_QUICKLINKS_COURSETYPE, 'system'=>LOCAL_QUICKLINKS_SYSTEMTYPE);
    $courses = enrol_get_my_courses('id, fullname', 'fullname', 0);
    $courses = (count($courses)) ? array_keys($courses) : array();
    list($sql_in, $courses_params) = $DB->get_in_or_equal(implode(', ', $courses), SQL_PARAMS_NAMED);

    $categories = $DB->get_records_sql("SELECT qlc.*
                                          FROM {local_quicklinks_categories} qlc
                                     LEFT JOIN {course} c ON c.id = qlc.courseid
                                         WHERE (qlc.type = :system OR (qlc.type = :private AND qlc.userid = :userid) OR (qlc.type = :course AND c.id $sql_in)) AND qlc.state = 1
                                         ORDER BY qlc.sortorder ASC
                                    ", $params+$courses_params);

    $quicklinks = $DB->get_records_sql("SELECT ql.*, qlc.name, qlc.courseid
                                          FROM {local_quicklinks} ql
                                     LEFT JOIN {local_quicklinks_categories} qlc ON ql.category = qlc.id
                                     LEFT JOIN {course} c ON c.id = qlc.courseid
                                         WHERE (qlc.type = :system OR (qlc.type = :private AND qlc.userid = :userid) OR (qlc.type = :course AND c.id $sql_in)) AND qlc.state = 1 AND ql.state = 1
                                         ORDER BY ql.sortorder ASC
                                    ", $params+$courses_params);
    $fs = get_file_storage();

    if (count($categories) > 0){
        $i = 0;
        foreach ($categories as $category){
            if (!$i) $category->active = 1;
            $qlinks = array();

            if (count($quicklinks) > 0){
                foreach ($quicklinks as $quicklink){
                    if ($quicklink->category == $category->id){
                        $icon = 'fa fa-link';

                        if ($quicklink->courseid){
                            $context = context_course::instance($quicklink->courseid);
                        } else {
                            $context = context_system::instance();
                        }

                        $file = null;
                        $files = $fs->get_area_files($context->id, 'local_quicklinks', 'image', $quicklink->id);
                        foreach ($files as $f) {
                            if ($f->get_filename() != '.'){
                                $file = $f;
                            }
                        }

                        if ($file) {
                            $imageurl = moodle_url::make_pluginfile_url($context->id, 'local_quicklinks', 'image', $quicklink->id, '/', $file->get_filename());
                            $quicklink->imageurl = $imageurl->out();
                        } else if (!empty($quicklink->icon)) {
                            $icon = 'fa '.$quicklink->icon;
                        }

                        $quicklink->link = (!stristr($quicklink->link, 'http://') and !stristr($quicklink->link, 'https://')) ? 'http://'.$quicklink->link : $quicklink->link;

                        $quicklink->icon = $icon;
                        $qlinks[] = (array)$quicklink;
                    }
                }
            }
            if (count($qlinks)){
                $category->qlinks = 1;
            }
            $category->qlink = $qlinks;
            if (count($qlinks) > 0){
                $result['categories'][] = (array)$category;
                $i++;
            }
        }

    }
    if (has_capability('local/quicklinks:manage', context_system::instance())){
        $result['canedit'] = 1;
        $result['managelink'] = $CFG->wwwroot.'/local/quicklinks/index.php';
    }

    return $result;
}

function local_quicklinks_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options=array()) {
    global $CFG;
    require_once($CFG->dirroot . '/repository/lib.php');

    // Leave this line out if you set the itemid to null in make_pluginfile_url (set $itemid to 0 instead).
    $itemid = array_shift($args); // The first item in the $args array.

    // Use the itemid to retrieve any relevant data records and perform any security checks to see if the
    // user really does have access to the file in question.

    // Extract the filename / filepath from the $args array.
    $filename = array_pop($args); // The last item in the $args array.
    if (!$args) {
        $filepath = '/'; // $args is empty => the path is '/'
    } else {
        $filepath = '/'.implode('/', $args).'/'; // $args contains elements of the filepath
    }

    // Retrieve the file from the Files API.
    $fs = get_file_storage();
    $file = $fs->get_file($context->id, 'local_quicklinks', $filearea, $itemid, $filepath, $filename);
    if (!$file) {
        return false; // The file does not exist.
    }

    send_stored_file($file, 86400, 0, $forcedownload, $options);
}
