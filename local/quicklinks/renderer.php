<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Displays information about all the assignment modules in the requested course
 *
 * @package   local_quicklinks
 * @copyright 2017 ISF
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


class local_quicklinks_renderer extends plugin_renderer_base {

    // Print box start
    public function startbox($boxclass = '') {
        return html_writer::start_tag('div', array('class'=>'content-wrapper '.$boxclass));
    }

    // Print box end
    public function endbox() {
        return html_writer::end_tag('div');
    }

    public function categories_search_form($params, $context) {
        global $PAGE, $CFG;

        $output = '';

        $output .= html_writer::start_tag("form",  array("action"=> $PAGE->url, 'class'=>'ql-search-form mform clearfix'));

        $output .= html_writer::start_tag("label", array('class'=>'form-inline'));
        $output .= html_writer::empty_tag('input', array('type' => 'text', 'name' => 'search', 'placeholder' => get_string('search', 'local_quicklinks'), 'value' => $params->search, 'class'=>'form-control'));
        $output .= html_writer::end_tag("label");

        if (has_capability('local/quicklinks:editcategory', $context)){
            $output .= html_writer::link(new moodle_url($CFG->wwwroot.'/local/quicklinks/editcategory.php'), html_writer::tag('i', '', array('class'=>'fa fa-pencil-square-o')).get_string('createnewcategory', 'local_quicklinks'), array('class'=>'btn btn-warning'));
        }

        $output .= html_writer::end_tag("form");

        return $output;
    }

    public function qlinks_search_form($params, $context) {
        global $PAGE, $CFG;

        $output = '';

        $output .= html_writer::start_tag("form",  array("action"=> $PAGE->url, 'class'=>'ql-search-form mform clearfix'));

        $output .= html_writer::start_tag("label", array('class'=>'form-inline'));
        $output .= html_writer::empty_tag('input', array('type' => 'hidden', 'name' => 'cid', 'value' => $params->cid));
        $output .= html_writer::empty_tag('input', array('type' => 'text', 'name' => 'search', 'placeholder' => get_string('search', 'local_quicklinks'), 'value' => $params->search, 'class'=>'form-control'));
        $output .= html_writer::end_tag("label");

        if (has_capability('local/quicklinks:editquicklink', $context)){
            $output .= html_writer::link(new moodle_url($CFG->wwwroot.'/local/quicklinks/editquicklink.php', array('cid'=>$params->cid)), html_writer::tag('i', '', array('class'=>'fa fa-pencil-square-o')).get_string('createnew', 'local_quicklinks'), array('class'=>'btn btn-warning'));
        }

        $output .= html_writer::end_tag("form");

        return $output;
    }

    public function export_for_template() {
        global $PAGE, $CFG;

        $quicklinks = array();
        $quicklinks = local_quicklinks_getqlinks();

        return $quicklinks;
    }
}

