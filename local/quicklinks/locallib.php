<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Displays information about all the assignment modules in the requested course
 *
 * @package   local_quicklinks
 * @copyright 2017 ISF
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

function local_quicklinks_get_formtypes($context = null){
    $types = array(''=>get_string('selecttype', 'local_quicklinks'));

    if (has_capability('local/quicklinks:editprivatetype', $context)){
        $types[LOCAL_QUICKLINKS_PRIVATETYPE] = get_string('privatetype', 'local_quicklinks');
    }
    if (has_capability('local/quicklinks:editcoursetype', $context)){
        $types[LOCAL_QUICKLINKS_COURSETYPE] = get_string('coursetype', 'local_quicklinks');
    }
    if (has_capability('local/quicklinks:editsystemtype', $context)){
        $types[LOCAL_QUICKLINKS_SYSTEMTYPE] = get_string('systemtype', 'local_quicklinks');
    }

    return $types;
}

function local_quicklinks_get_category_types(){
    $types = array();
    $types[LOCAL_QUICKLINKS_PRIVATETYPE] = get_string('privatetype', 'local_quicklinks');
    $types[LOCAL_QUICKLINKS_COURSETYPE] = get_string('coursetype', 'local_quicklinks');
    $types[LOCAL_QUICKLINKS_SYSTEMTYPE] = get_string('systemtype', 'local_quicklinks');

    return $types;
}

function local_quicklinks_get_courses($context = null){
    global $DB;
    $courses = array();

    if (has_capability('local/quicklinks:seeallcourses', $context)){
        $courses = $DB->get_records_select_menu('course', 'id > 1', array(), 'fullname', 'id, fullname');
    } else {
        $mycourses = enrol_get_my_courses('id, fullname', 'fullname', 0);
        if (count($mycourses)){
            foreach($mycourses as $course){
                $courses[$course->id] = $course->fullname;
            }
        }
    }

    return $courses;
}

function local_quicklinks_get_last_category($context = null){
    global $DB, $USER;

    $where = '';
    $types = array();
    $params = array();

    if (has_capability('local/quicklinks:editsystemtype', $context)){
        $types[] = " type = '".LOCAL_QUICKLINKS_SYSTEMTYPE."' ";
    }
    if (has_capability('local/quicklinks:editprivatetype', $context)){
        $types[] = " (type = '".LOCAL_QUICKLINKS_PRIVATETYPE."' AND userid = :userid) ";
        $params['userid'] = $USER->id;
    }
    if (has_capability('local/quicklinks:editcoursetype', $context)){
        $courses = 0;
        $mycourses = local_quicklinks_get_courses($context);
        if (count($mycourses)){
            $coursekeys = array_keys($mycourses);
            $courses = implode(',', $coursekeys);
        }
        $types[] = " (type = '".LOCAL_QUICKLINKS_COURSETYPE."' AND courseid IN (:courses)) ";
        $params['courses'] = $courses;
    }
    if (count($types)){
        $where .= " AND ".implode(' OR ', $types);
    }

    $last_category = $DB->get_record_sql("
        SELECT MAX(sortorder) as last
          FROM {local_quicklinks_categories}
         WHERE id > 0 $where
        ", $params);

    return $last_category;
}
