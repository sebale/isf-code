<?php
require_once('../../../config.php');

$id = optional_param('id', $USER->id, PARAM_INT);    // course id

require_login();
$systemcontext = context_system::instance();
require_capability('local/manager:managecourses', $systemcontext);

$USER->editing = 0;
$title = get_string('managecourses', 'local_manager');
$PAGE->set_url('/local/manager/courses/index.php');
$PAGE->set_context($systemcontext);
$PAGE->navbar->add($title);

$PAGE->set_pagelayout('admin');

$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->set_heading($title);
$PAGE->requires->css('/local/manager/assets/css/dataTable.css');

echo $OUTPUT->header();
/*$active_button = 'managecourses';
include('course_buttons.php');*/
echo $OUTPUT->heading($title);
echo html_writer::start_tag('div', array('class'=>'users-manage-box'));
?>
<div class="tq-users-tabs">
	<div class="users-tabs">
		<div class="users-tabs-item course-tab visible">
            <div class="users-create-panel clearfix">
                <?php if (has_capability('moodle/course:create', $systemcontext)) : ?>
                    <button class="btn btn-info" onclick="location.href = '<?php echo $CFG->wwwroot;?>/course/edit.php?category=1'"><i class="fa fa-plus"></i> <?php echo get_string('createnewcourse', 'local_manager');?></button>
                <?php endif; ?>
            </div>
			<table title="Courses" id="datatable_courses" class="generaltable sorting data-table" cellpadding="0" cellspacing="0">
				<thead>
					<tr>
						<th align="center" value="id" class="asc"><span><?php echo get_string('ID', 'local_manager'); ?></span></th>
						<th align="center" value="course_name"><span><?php echo get_string('coursename', 'local_manager'); ?></span></th>
						<th align="center" value="course_category"><span><?php echo get_string('category', 'local_manager'); ?></span></th>
						<th align="center" value="course_start"><span><?php echo get_string('startdate', 'local_manager'); ?></span></th>
						<th align="center" value="course_end"><span><?php echo get_string('enddate', 'local_manager'); ?></span></th>
						<th align="center" value="last_updated"><span><?php echo get_string('lastupdated', 'local_manager'); ?></span></th>
						<th align="center" value="teacher"><span><?php echo get_string('teacher', 'local_manager'); ?></span></th>
                        <?php if(has_capability('moodle/course:create', $systemcontext) or has_capability('moodle/course:delete', $systemcontext) or has_capability('moodle/course:reset', $systemcontext) or has_capability('enrol/manual:enrol', $systemcontext)) : ?>
                            <th align="center" class="nosort"><span><?php echo get_string('actions', 'local_manager'); ?></span></th>
                        <?php else : ?>
                            <th align="center" class="nosort"><span></span></th>
                        <?php endif; ?>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php
echo html_writer::end_tag('div');
?>
<?php
    $PAGE->requires->js_call_amd('local_manager/course', 'init', array( 'capability' => !has_capability('moodle/course:create', $systemcontext) ));
?>
<style>
    .users-manage-box .course-tab .dataTables_wrapper {padding-top: 0;}
    .users-manage-box .course-tab .dataTables_wrapper .dataTables_filter {margin-top: -54px;right: 178px;}
    .users-manage-box .course-tab .dataTables_wrapper .dataTables_filter.relative { right: 0;}
</style>
<?php 
/// and proper footer
echo $OUTPUT->footer();
