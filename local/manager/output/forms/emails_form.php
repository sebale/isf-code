<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once($CFG->dirroot.'/lib/formslib.php');

class send_emails_form extends moodleform {
    // Define the form
    function definition () {
        $mform =& $this->_form;
        $courseid = $this->_customdata['course'];
        $modinfo = get_fast_modinfo($courseid);

        $modules = array();
        foreach($modinfo->get_cms() as $cm){
            if($cm->completion > 0){
                $modules[$cm->id] = $cm->name;
            }
        }
        $mform->addElement('hidden', 'course', $courseid);
        $mform->setType('course', PARAM_INT);

        $mform->addElement('select', 'course_module', get_string('course_module', 'local_manager'), $modules);
        $mform->addRule('course_module', get_string('required'), 'required', null, 'client');

        $mform->addElement('text', 'subject', get_string('subject', 'local_manager'));
        $mform->setType('subject', PARAM_TEXT);
        $mform->addRule('subject', get_string('required'), 'required', null, 'client');

        $mform->addElement('editor', 'body', get_string('message', 'local_manager'));
        $mform->setType('body', PARAM_RAW);
        $mform->addRule('body', get_string('required'), 'required', null, 'client');
        $mform->addHelpButton('body', 'message', 'local_manager');


        $this->add_action_buttons(false, get_string('send', 'local_manager'));

    }


}


