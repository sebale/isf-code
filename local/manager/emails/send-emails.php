<?php
require_once('../../../config.php');
require_once($CFG->dirroot.'/local/manager/output/forms/emails_form.php');
require_once('../lib.php');

$course = required_param('course', PARAM_INT);

require_login($course);
$context = context_course::instance($course);
$systemcontext = context_system::instance();
require_capability('local/manager:send_emails', $systemcontext);


$title = get_string('send_emails_to_not_completed_users', 'local_manager');
$PAGE->set_url('/local/manager/emails/send-emails.php', array('course'=>$course));
$PAGE->set_context($context);
$PAGE->navbar->add($title);
$PAGE->set_pagelayout('admin');

//create form
$mform = new send_emails_form(null, array('course'=>$course));
$count = 0;

if ($mform->is_cancelled()) {
} elseif ($data = $mform->get_data()) {
    $modinfo = get_fast_modinfo($course);
    $cm = $modinfo->get_cm($data->course_module);

    $users = $DB->get_records_sql("
                SELECT
                  u.*,
                  c.fullname as course_name
                FROM mdl_context ctx
                  LEFT JOIN mdl_role_assignments ra ON ra.contextid=ctx.id
                  LEFT JOIN mdl_course_modules_completion cmc ON cmc.userid=ra.userid AND cmc.coursemoduleid=:coursemodule
                  
                  LEFT JOIN mdl_user u ON u.id=ra.userid
                  LEFT JOIN mdl_course c ON c.id=ctx.instanceid
                WHERE ctx.contextlevel=50 AND ctx.instanceid=:course AND ra.roleid=5 AND (cmc.completionstate=0 OR cmc.completionstate IS NULL)", array('coursemodule'=>$data->course_module,'course'=>$data->course));

    $userfrom = core_user::get_noreply_user();
    $userfrom->maildisplay = true;


    if (count($users)){
        foreach ($users as $user){
            // Fetch message HTML and plain text formats
            $message  = str_replace(array('[[user first name]]','[[user last name]]','[[course name]]','[[course module name]]'),array($user->firstname,$user->lastname,$user->course_name,$cm->name),$data->body['text']);
            $plaintext = format_text_email($message, FORMAT_HTML);

            // Subject
            $subject = $data->subject;

            $eventdata = new \core\message\message();
            $eventdata->courseid         = $data->course;
            $eventdata->userfrom         = $userfrom;
            $eventdata->userto           = $user;
            $eventdata->subject          = $subject;
            $eventdata->fullmessage      = $plaintext;
            $eventdata->fullmessageformat = FORMAT_HTML;
            $eventdata->fullmessagehtml  = $message;
            $eventdata->smallmessage     = '';

            // Required for messaging framework
            $eventdata->component = 'local_manager';
            $eventdata->name = 'message_not_completed_users';

            message_send($eventdata);
            $count++;
        }
    }

}

$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

if($count>0){
    echo html_writer::div(get_string('messages_sended','local_manager',$count),'alert alert-info');
}

$mform->display();

echo $OUTPUT->footer();
