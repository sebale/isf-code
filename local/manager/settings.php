<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * manager version file.
 *
 * @package    local_manager
 * @author     ISF
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die;

$settings = new admin_settingpage('local_manager', get_string('pluginname', 'local_manager'));
$ADMIN->add('localplugins', $settings);

$name = 'local_manager/enabled';
$title = get_string('enabled', 'local_manager');
$description = get_string('enabled_desc', 'local_manager');
$default = true;
$setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
$settings->add($setting);

$settings = new admin_settingpage('local_manager_activity_picker', get_string('activity_picker_settings', 'local_manager'));
$ADMIN->add('localplugins', $settings);

$all_mods = get_module_types_names();
foreach($all_mods as $mod=>$title){
    $settings->add(new admin_setting_heading('local_manager/chooser_mod_'.$mod, $title, ''));

    $description = '';
    $name = 'local_manager/chooser_mod_cat_'.$mod;
    $setting = new admin_setting_configtext($name, get_string('category','local_manager'), $description, null);
    $settings->add($setting);

    $description = '';
    $name = 'local_manager/chooser_mod_cat_ch_'.$mod;
    $setting = new admin_setting_configtext($name, get_string('category_ch','local_manager'), $description, null);
    $settings->add($setting);

    $name = 'local_manager/chooser_mod_color_'.$mod;
    $default = '#00acdf';
    $setting = new admin_setting_configcolourpicker($name, get_string('color','local_manager'), $description, $default);
    $settings->add($setting);

    $name = 'local_manager/chooser_mod_advanced_'.$mod;
    $setting = new admin_setting_configcheckbox($name, get_string('advanced','local_manager'), $description, 0);
    $settings->add($setting);

    $name = 'local_manager/chooser_mod_help_'.$mod;
    $setting = new admin_setting_configtext($name, get_string('help_link','local_manager'), $description, null);
    $settings->add($setting);

    $name = 'local_manager/chooser_mod_example_'.$mod;
    $setting = new admin_setting_configtext($name, get_string('example_link','local_manager'), $description, null);
    $settings->add($setting);

    $name = 'local_manager/chooser_mod_help_ch_'.$mod;
    $setting = new admin_setting_configtext($name, get_string('help_link_ch','local_manager'), $description, null);
    $settings->add($setting);

    $name = 'local_manager/chooser_mod_example_ch_'.$mod;
    $setting = new admin_setting_configtext($name, get_string('example_link_ch','local_manager'), $description, null);
    $settings->add($setting);
}

