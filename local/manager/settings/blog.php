<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Course images library.
 *
 * @package local_manager
 * @copyright 2017 ISF
 * @author sebale.net
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once("../../../config.php");

require_once($CFG->dirroot."/local/manager/output/forms/blog_settings_form.php");

$course = $DB->get_record('course', array('id'=>1));
$context = context_system::instance();
$usercontext = context_user::instance($USER->id);

$url = new moodle_url('/local/manager/settings/blog.php');

require_login();

$title = get_string('blogsettings', 'local_manager');
$PAGE->set_url($url);
$PAGE->set_context($context);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->set_pagelayout('admin');
$PAGE->navbar->add($title);

$data = $DB->get_record('local_manager_blog_settings', array('userid'=>$USER->id));
if (!isset($data->id)){
    $data = new stdClass();
    $data->id = 0;
}

// background image
$options = array('subdirs'=>0, 'maxfiles'=>1, 'accepted_types'=>array('.jpg', '.gif', '.png'));
file_prepare_standard_filemanager($data, 'blogbg', $options, $usercontext, 'local_manager', 'blogbg', 0);

$form = new blog_settings_form(null, array('data'=>$data, 'contextid'=>$usercontext->id,  'component'=>'local_manager'));


$data = $form->get_data();

if ($form->is_cancelled()) {
    redirect(new moodle_url('/blog/index.php', array('userid'=>$USER->id)));
} else if ($data) {

    $options = array('subdirs'=>0, 'maxfiles'=>1, 'accepted_types'=>array('.jpg', '.gif', '.png'));
    $formdata = file_postupdate_standard_filemanager($data, 'blogbg', $options, $usercontext, 'local_manager', 'blogbg', 0);

    $data->userid = $USER->id;
    $data->timemodified = time();
    if ($data->id) {
        $DB->update_record('local_manager_blog_settings', $data);
    } else {
        $DB->insert_record('local_manager_blog_settings', $data);
    }

    redirect(new moodle_url('/blog/index.php', array('userid'=>$USER->id)), get_string('settingsupdated', 'local_manager'));
}

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo $OUTPUT->container_start();
$form->display();
echo $OUTPUT->container_end();

echo $OUTPUT->footer();
