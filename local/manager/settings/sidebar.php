<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Displays information about all the assignment modules in the requested course
 *
 * @package   local_quicklinks
 * @copyright 2017 ISF
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once("../../../config.php");
require_once("../lib.php");
require_once($CFG->dirroot."/local/manager/output/tables/sidebar_table.php");

require_login();

$id          = optional_param('id', 0, PARAM_INT);
$theme       = optional_param('theme', 'primary', PARAM_NOTAGS);
$search      = optional_param('search', '', PARAM_CLEAN);

$params = array('search'=>$search, 'theme'=>$theme);

$context = context_system::instance();

require_capability('local/manager:managesidebar', $context);
$title = get_string('sidebarsettings', 'local_manager');
$PAGE->set_context($context);
$PAGE->set_url(new moodle_url("/local/manager/settings/sidebar.php", $params));
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_pagelayout('standard');
$PAGE->set_heading($title);

$table = new sidebar_table('sidebar_table', $context, $search, $theme);
$table->attributes['class'] .= ' sorting';
$table->is_collapsible = false;
$table->sortable(true, 'sortorder');

$table_core = new sidebar_table_core('sidebar_table_core', $context, $search, $theme);
$table_core->attributes['class'] .= ' sorting';
$table_core->is_collapsible = false;
$table_core->sortable(true, 'sortorder');

$renderer = $PAGE->get_renderer('local_manager');

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$renderer->manager_print_sidebar_tabs($theme);
echo $renderer->startbox('sidebaritems-list');
echo $renderer->sidebar_search_form((object)$params, $context, $theme);
$table->out(50, true);
echo $renderer->endbox();

echo $renderer->startbox('sidebaritems-list');
echo html_writer::tag('h3',get_string('core_icons', 'local_manager'),array('class'=>'table_heading'));
$table_core->out(50, true);
echo $renderer->endbox();

$PAGE->requires->js_call_amd('local_manager/sidebar', 'init', array($theme));

echo $OUTPUT->footer();
