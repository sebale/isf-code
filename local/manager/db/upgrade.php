<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * manager version file.
 *
 * @package    local_manager
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

function xmldb_local_manager_upgrade($oldversion) {
    global $CFG, $DB;

    $dbman = $DB->get_manager();

    if($oldversion < 2017061100){
        $new_record = new stdClass();
        $new_record->name = 'font-import';
        $new_record->type = 'font-import';
        $new_record->value = '';
        $new_record->defaultvalue = '';
        $new_record->state = 1;
        $new_record->sortorder = 1;
        $new_record->category = 10;
        $new_record->theme = 'primary';
        $DB->insert_record('local_manager_styles',$new_record);

        $new_record = new stdClass();
        $new_record->name = 'font-family';
        $new_record->type = 'font';
        $new_record->value = '';
        $new_record->defaultvalue = 'Open Sans';
        $new_record->state = 1;
        $new_record->sortorder = 1;
        $new_record->category = 10;
        $new_record->theme = 'primary';
        $DB->insert_record('local_manager_styles',$new_record);

        $new_record = new stdClass();
        $new_record->name = 'font-import';
        $new_record->type = 'font-import';
        $new_record->value = '';
        $new_record->defaultvalue = '';
        $new_record->state = 1;
        $new_record->sortorder = 1;
        $new_record->category = 10;
        $new_record->theme = 'secondary';
        $DB->insert_record('local_manager_styles',$new_record);

        $new_record = new stdClass();
        $new_record->name = 'font-family';
        $new_record->type = 'font';
        $new_record->value = '';
        $new_record->defaultvalue = 'Open Sans';
        $new_record->state = 1;
        $new_record->sortorder = 1;
        $new_record->category = 10;
        $new_record->theme = 'secondary';
        $DB->insert_record('local_manager_styles',$new_record);

        $DB->execute('UPDATE {local_manager_styles} SET category=category-1 WHERE category>5 ');
    }
    if($oldversion < 2017062801){
        $table = new xmldb_table('local_manager_sidebar');
        $field = new xmldb_field('core', XMLDB_TYPE_INTEGER, '10',null,null,null,0);
        $dbman->add_field($table, $field);

        $new_record = new stdClass();
        $new_record->title = 'Dashboard';
        $new_record->title_ch = 'Dashboard';
        $new_record->types = 'dashboard';
        $new_record->icon = 'fa fa-home';
        $new_record->state = 1;
        $new_record->core = 1;
        $new_record->theme = 'primary';
        $DB->insert_record('local_manager_sidebar',$new_record);

        $new_record = new stdClass();
        $new_record->title = 'Course navigation';
        $new_record->title_ch = 'Course navigation';
        $new_record->types = 'coursenavigation';
        $new_record->icon = 'fa fa-book';
        $new_record->state = 1;
        $new_record->core = 1;
        $new_record->theme = 'primary';
        $DB->insert_record('local_manager_sidebar',$new_record);

        $new_record = new stdClass();
        $new_record->title = 'Courses';
        $new_record->title_ch = 'Courses';
        $new_record->types = 'courses';
        $new_record->icon = 'fa fa-list';
        $new_record->state = 1;
        $new_record->core = 1;
        $new_record->theme = 'primary';
        $DB->insert_record('local_manager_sidebar',$new_record);

        $new_record = new stdClass();
        $new_record->title = 'Manage';
        $new_record->title_ch = 'Manage';
        $new_record->types = 'manage';
        $new_record->icon = 'fa fa-wrench';
        $new_record->state = 1;
        $new_record->core = 1;
        $new_record->theme = 'primary';
        $DB->insert_record('local_manager_sidebar',$new_record);

        $new_record = new stdClass();
        $new_record->title = 'Settings';
        $new_record->title_ch = 'Settings';
        $new_record->types = 'settings';
        $new_record->icon = 'fa fa-cogs';
        $new_record->state = 1;
        $new_record->core = 1;
        $new_record->theme = 'primary';
        $DB->insert_record('local_manager_sidebar',$new_record);

        $new_record = new stdClass();
        $new_record->title = 'Add a block';
        $new_record->title_ch = 'Add a block';
        $new_record->types = 'addblock';
        $new_record->icon = 'fa fa-plus';
        $new_record->state = 1;
        $new_record->core = 1;
        $new_record->theme = 'primary';
        $DB->insert_record('local_manager_sidebar',$new_record);

        $new_record = new stdClass();
        $new_record->title = 'Dashboard';
        $new_record->title_ch = 'Dashboard';
        $new_record->types = 'dashboard';
        $new_record->icon = 'fa fa-home';
        $new_record->state = 1;
        $new_record->core = 1;
        $new_record->theme = 'secondary';
        $DB->insert_record('local_manager_sidebar',$new_record);

        $new_record = new stdClass();
        $new_record->title = 'Course navigation';
        $new_record->title_ch = 'Course navigation';
        $new_record->types = 'coursenavigation';
        $new_record->icon = 'fa fa-book';
        $new_record->state = 1;
        $new_record->core = 1;
        $new_record->theme = 'secondary';
        $DB->insert_record('local_manager_sidebar',$new_record);

        $new_record = new stdClass();
        $new_record->title = 'Courses';
        $new_record->title_ch = 'Courses';
        $new_record->types = 'courses';
        $new_record->icon = 'fa fa-list';
        $new_record->state = 1;
        $new_record->core = 1;
        $new_record->theme = 'secondary';
        $DB->insert_record('local_manager_sidebar',$new_record);

        $new_record = new stdClass();
        $new_record->title = 'Manage';
        $new_record->title_ch = 'Manage';
        $new_record->types = 'manage';
        $new_record->icon = 'fa fa-wrench';
        $new_record->state = 1;
        $new_record->core = 1;
        $new_record->theme = 'secondary';
        $DB->insert_record('local_manager_sidebar',$new_record);

        $new_record = new stdClass();
        $new_record->title = 'Settings';
        $new_record->title_ch = 'Settings';
        $new_record->types = 'settings';
        $new_record->icon = 'fa fa-cogs';
        $new_record->state = 1;
        $new_record->core = 1;
        $new_record->theme = 'secondary';
        $DB->insert_record('local_manager_sidebar',$new_record);

        $new_record = new stdClass();
        $new_record->title = 'Add a block';
        $new_record->title_ch = 'Add a block';
        $new_record->types = 'addblock';
        $new_record->icon = 'fa fa-plus';
        $new_record->state = 1;
        $new_record->core = 1;
        $new_record->theme = 'secondary';
        $DB->insert_record('local_manager_sidebar',$new_record);
    }

    return true;
}