<?php

require_once('../../../config.php');

require_once($CFG->dirroot . '/' . $CFG->admin . '/roles/lib.php');
require_once($CFG->dirroot.'/user/editlib.php');
require_once('../lib.php');

$id = optional_param('id', $USER->id, PARAM_INT);    // user id
$action = optional_param('action', '', PARAM_RAW);

require_login();
$systemcontext = context_system::instance();
require_capability('local/manager:manageusers', $systemcontext);

$title = get_string('manageusers', 'local_manager');
$PAGE->set_url('/local/manager/users/index.php');
$PAGE->set_context($systemcontext);
$PAGE->navbar->add($title);

list($roles, $assigncounts, $nameswithcounts) = get_assignable_roles($systemcontext, ROLENAME_BOTH, true);
$PAGE->set_pagelayout('standard');

$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->requires->css('/local/manager/assets/css/dataTable.css');

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo html_writer::start_tag('div', array('class'=>'users-manage-box'));

?>
<div class="search-allusers-box">
    <label class="form-inline">
        <input type="text" id="all-search" class="all_search form-control" value="" placeholder="<?php echo get_string('search', 'local_manager'); ?>" />
    </label>
</div>
<div class="tq-users-tabs">
    <div class="users-tabs-header nav-tabs-header">
        <ul class="nav nav-tabs nav-tabs-simple" role="tablist">
            <?php $i = 0; $first_role = 0; ?>
            <?php foreach ($roles as $roleid=>$rolename) : ?>
                <?php $first_role = !($first_role) ? $roleid : $first_role; ?>
                <li class="nav-item" id="role_<?php echo $roleid; ?>"><a href="#roleid_<?php echo $roleid; ?>" data-toggle="tab" role="tab" class="nav-link <?php echo ($i == 0) ? 'active' : '' ?>"><?php echo $rolename; ?>s</a></li>
                <?php $i++; ?>
            <?php endforeach; ?>
            <li id="role_0" class="nav-item"><a href="#roleid_0" data-toggle="tab" role="tab" class="nav-link"><?php echo get_string('notassignedusers', 'local_manager'); ?></a></li>
            <li id="search-tab" data-toggle="tab" role="tab" class="hidden nav-item"><a href="#roleid_search" data-toggle="tab" role="tab" class="nav-link"><?php echo get_string('searchresults', 'local_manager'); ?></a></li>
        </ul>
    </div>
    <div class="users-tabs tab-content">
        <?php $i = 0; ?>
        <?php foreach ($roles as $roleid=>$rolename) : ?>
            <div class="users-tabs-item tab-pane<?php echo ($i == 0) ? ' active' : ''?>" id="roleid_<?php echo $roleid; ?>"></div>
            <?php $i++; ?>
        <?php endforeach; ?>
        <div class="users-tabs-item other-users-tab tab-pane" id="roleid_0"></div>
        <div class="users-tabs-item search-users-tab tab-pane" id="roleid_search">
        </div>
    </div>
</div>

<?php
    echo html_writer::end_tag('div');
    $PAGE->requires->js_call_amd('local_manager/user', 'init', array( 'firstrole' => $first_role ));
    // and proper footer
    echo $OUTPUT->footer();
