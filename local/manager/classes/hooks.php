<?php
namespace local_manager;

/**
 * ISF Activity Duplication
 * View the associated README.md file for more information.
 *
 * Hook functions for changes in core code.
 *
 * @package    local_manager
 * @author     2017 ISF
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use html_writer;
use moodle_url;
use lang_string;
use context_module;
use context_user;

/**
 * Hook functions.
 *
 * @package    local_manager
 * @author     ISF
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class hooks {

    public static function get_blog_styles() {
        global $DB, $CFG, $USER;

        $out = '';

        $user = $USER;
        $userid = optional_param('userid', null, PARAM_INT);
        if ($userid and $userid != $USER->id) {
            $user = $DB->get_record('user', array('id'=>$userid));
        }

        $usercontext = context_user::instance($user->id);

        $fs = get_file_storage(); $image_url = null;
        $imgfiles = $fs->get_area_files($usercontext->id, 'local_manager', 'blogbg', 0);
        foreach ($imgfiles as $imgfile) {
            $filename = $imgfile->get_filename();
            $filetype = $imgfile->get_mimetype();
            if ($filename == '.' or !$filetype) continue;

            $url = moodle_url::make_pluginfile_url($usercontext->id, 'local_manager', 'blogbg', 0, '/', $filename);
            $image_url = $url->out();
        }

        if ($image_url) {
            $out .= 'background:url("'.$image_url.'") no-repeat scroll center center; background-size: cover;';
        }

        $settings = $DB->get_record('local_manager_blog_settings', array('userid'=>$user->id));
        if (isset($settings->color) and !empty($settings->color)) {
            $out .= 'background-color:#'.$settings->color;
        }

        return $out;
    }

}
