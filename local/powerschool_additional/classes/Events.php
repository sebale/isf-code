<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/local/powerschool/locallib.php');
require_once($CFG->dirroot.'/local/powerschool_additional/classes/Courses.php');


class events_handler_ps_additional {

    public static function moodle_user_graded(\core\event\user_graded $instant){
        $connection_id = PowerSchool::get_connection_from_courseid($instant->courseid);
        if(!empty($connection_id)){
            $plugin = new AdditionalCourses($connection_id);
            $plugin->graded_assignments($instant);
        }
        return true;
    }
}















