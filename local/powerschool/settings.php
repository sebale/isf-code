<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * PowerSchool Settings
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

$ADMIN->add('root', new admin_category('powerschoolroot', get_string('powerschoolroot', 'local_powerschool')));
$ADMIN->add('powerschoolroot', new admin_externalpage('manage_connections', get_string('manage_connections', 'local_powerschool'),
        $CFG->wwwroot.'/local/powerschool/manage-connections.php', 'local/powerschool:manage'));

$connections = $DB->get_records('powerschool_connections');
foreach($connections as $connection){
    $ADMIN->add('powerschoolroot', new admin_externalpage('powerschoolsyncusers'.$connection->id, get_string('manual_sync', 'local_powerschool', $connection->name),
        $CFG->wwwroot.'/local/powerschool/manual-sync.php?connection='.$connection->id, 'local/powerschool:manage'));
}

		
$settings = new admin_settingpage('local_powerschool', get_string('pluginname', 'local_powerschool'));
if (!$ADMIN->locate('powerschool')){
	$ADMIN->add('localplugins', $settings);
}
$settings->add(new admin_setting_heading('local_powerschool/setting_subscription', get_string('setting_subscription', 'local_powerschool'), ''));

$name = 'local_powerschool/user_key';
$title = get_string('user_key', 'local_powerschool');
$description = '';
$setting = new admin_setting_configtext($name, $title, $description, null);
$settings->add($setting);

$name = 'local_powerschool/secret_key';
$title = get_string('secret_key', 'local_powerschool');
$description = '';
$setting = new admin_setting_configtext($name, $title, $description, null);
$settings->add($setting);

$settings->add(new admin_setting_heading('local_powerschool/setting_users', get_string('setting_users', 'local_powerschool'), ''));

$name = 'local_powerschool/temp_password';
$title = get_string('temp_password', 'local_powerschool');
$description = get_string('temp_password_desc', 'local_powerschool');
$setting = new admin_setting_configtext($name, $title, $description, null);
$settings->add($setting);

$name = 'local_powerschool/forcepasswordchange';
$title = get_string('forcepasswordchange', 'local_powerschool');
$description = get_string('forcepasswordchange_desc', 'local_powerschool');
$setting = new admin_setting_configcheckbox($name, $title, $description, true);
$settings->add($setting);

$name = 'local_powerschool/sync_moodle_user_change';
$title = get_string('sync_moodle_user_change', 'local_powerschool');
$description = get_string('sync_moodle_user_change_desc', 'local_powerschool');
$setting = new admin_setting_configcheckbox($name, $title, $description, null);
$settings->add($setting);

$name = 'local_powerschool/associate_moodle_user';
$title = get_string('associate_moodle_user', 'local_powerschool');
$description = get_string('associate_moodle_user_desc', 'local_powerschool');
$setting = new admin_setting_configselect($name, $title, $description, 'username',array('username'=>get_string('username'),'email'=>get_string('email')));
$settings->add($setting);

$settings->add(new admin_setting_heading('local_powerschool/setting_modules', get_string('setting_available_modules', 'local_powerschool'), ''));

$name = 'local_powerschool/available_modules_to_create';
$title = get_string('available_modules_to_create', 'local_powerschool');
$description = get_string('available_modules_to_create_desc', 'local_powerschool');
$settings_arr = get_module_types_names();
$setting = new admin_setting_configmultiselect($name,$title, $description, array_keys($settings_arr), $settings_arr);
$settings->add($setting);
