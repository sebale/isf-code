<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * PowerSchool Settings
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require('connections_classes.php');

$id = optional_param('id',0, PARAM_INT);
$action = optional_param('action', '', PARAM_RAW);

require_login();
$context = context_system::instance();
require_capability('local/powerschool:manage', $context);

if($id>0 && $action == 'delete'){
    $DB->delete_records('powerschool_connections',array('id'=>$id));
}

$PAGE->set_url(new moodle_url("/local/powerschool/manage-connections.php"));
$PAGE->set_pagelayout('admin');
$PAGE->set_context($context);
$PAGE->set_title(get_string('powerschoolroot', 'local_powerschool').': '.get_string('manage_connections', 'local_powerschool'));
$PAGE->set_heading(get_string('powerschoolroot', 'local_powerschool').': '.get_string('manage_connections', 'local_powerschool'));

$table = new connections_table('connections_table');

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('manage_connections', 'local_powerschool'));

$table->out(20, true);
echo '<br>'.$OUTPUT->single_button(new moodle_url('/local/powerschool/edit-connection.php'), get_string('create_new_connection', 'local_powerschool'));

echo $OUTPUT->footer();
