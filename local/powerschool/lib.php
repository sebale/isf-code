<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin version info
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
require_once ('classes/Courses.php');
require_once ('classes/Users.php');

function local_powerschool_extend_settings_navigation(settings_navigation $settingsnav, context $context){
    global $COURSE;
    $coursenode = $settingsnav->get('courseadmin');
    if ($coursenode && has_capability('local/powerschool:view',$context)) {
        $cat = $coursenode->add(get_string('powerschoolroot', 'local_powerschool'), null, navigation_node::TYPE_CONTAINER, null, 'powerschool');
        $cat->add(get_string('assignment_sync', 'local_powerschool'), new moodle_url('/local/powerschool/assignment-sync.php',array('course'=>$COURSE->id)), navigation_node::TYPE_SETTING);
    }
}

function local_powerschool_coursemodule_standard_elements($formwrapper, $mform) {
    global $COURSE, $DB, $CFG;

    if (!$DB->record_exists('powerschool_course_fields',array('courseid'=>$COURSE->id))) {
        return;
    }

    $cmid = null;
    if ($cm = $formwrapper->get_coursemodule()) {
        $cmid = $cm->id;
    }

    $current = $formwrapper->get_current();
    $enabled_modules = explode(',',get_config('local_powerschool', 'available_modules_to_create'));
    if(!in_array($current->modulename,$enabled_modules))
        return;

    $mform->addElement('header', 'powerschool', get_string('powerschoolroot', 'local_powerschool'));
    if(PowerSchool::get_coursetype_from_courseid($COURSE->id) == 'groups')
        $mform->addElement('html', '<div class="extensions alert"><h5>' . get_string('you_must_set_restrict_access_for_sync', 'local_powerschool') . '</h5></div>');
    $mform->addElement('advcheckbox', 'powerschool_mod_sync', get_string('sync','local_powerschool'), '', array('group' => 1), array(0, 1));

    $mform->addElement('advcheckbox', 'mod_count_in_final_grade', get_string('count_in_final_grade', 'local_powerschool'), '', array('group' => 1), array(0, 1));

    if(!empty($CFG->enableoutcomes)){
        $mform->addElement('advcheckbox', 'mod_standard_sync', get_string('standard_sync', 'local_powerschool'), '', array('group' => 1), array(0, 1));
    }

    if($cmid && $record = $DB->get_record('powerschool_assignment_sync',array('cmid'=>$cmid, 'courseid'=>$COURSE->id))){
        $mform->setDefault('powerschool_mod_sync', $record->sync);
        $mform->setDefault('mod_standard_sync', $record->standard_sync);
        $mform->setDefault('mod_count_in_final_grade', $record->count_in_final_grade);
    }
}

function local_powerschool_coursemodule_edit_post_actions($data, $course) {
    global $DB;

    if (!$DB->record_exists('powerschool_course_fields',array('courseid'=>$course->id)) || !isset($data->powerschool_mod_sync) || empty($data->powerschool_mod_sync)) {
        return $data;
    }

    $record = $DB->get_record('powerschool_assignment_sync',array('cmid'=>$data->coursemodule, 'courseid'=>$course->id));
    if(empty($record)){
        $record = new stdClass();
        $record->cmid = $data->coursemodule;
        $record->courseid = $course->id;
        $record->sync = $data->powerschool_mod_sync;
        $record->standard_sync = (isset($data->mod_standard_sync))?$data->mod_standard_sync:0;
        $record->count_in_final_grade = $data->mod_count_in_final_grade;
        $DB->insert_record('powerschool_assignment_sync', $record);
    }else{
        $record->sync = $data->powerschool_mod_sync;
        $record->standard_sync = (isset($data->mod_standard_sync))?$data->mod_standard_sync:0;
        $record->count_in_final_grade = $data->mod_count_in_final_grade;
        $DB->update_record('powerschool_assignment_sync', $record);
    }

    if($data->powerschool_mod_sync == 1){
        $connection_id = PowerSchool::get_connection_from_courseid($course->id);
        if(!empty($connection_id)){
            $plugin = new Courses($connection_id);
            $plugin->create_assignment_from_sync($record->id);
        }
    }

    return $data;
}

function local_powerschool_cron(){
    global $DB,$CFG;

    $connections = $DB->get_records('powerschool_connections',array(),'','id');
    foreach($connections as $connection){
        $plugin = new Users($connection->id);
        $plugin->auto_import_guardians();

        if(!empty($CFG->enableoutcomes)){
            $data = $DB->get_records_sql("SELECT pa.assignmentid
                                      FROM {powerschool_assignment_sync} pas
                                        LEFT JOIN {powerschool_assignment} pa ON pa.cmid=pas.cmid
                                        LEFT JOIN {powerschool_course_fields} pcf ON pcf.courseid=pas.courseid
                                      WHERE pas.standard_sync=1 AND pcf.connection_id=:connection_id", array('connection_id' => $connection->id));
            $assignments = array();
            foreach($data as $datum){
                $assignments[] = $datum->assignmentid;
            }

            $plugin = new Courses($connection->id);
            $plugin->import_standards($assignments);
        }
    }

    return true;
}

/* hack, confirm page for delete group if sync with PS */
function local_powerschool_confirm_del_cat(){
    global $PAGE,$DB;
    if(!$PAGE->has_set_url() || strpos($PAGE->url->get_path(), '/group/delete.php') === false)
        return true;

    $ps_confirm = optional_param('ps_confirm', 0, PARAM_BOOL);
    $params = $PAGE->url->params();
    $connection_id = Courses::get_connection_from_courseid($params['courseid']);

    if($ps_confirm || empty($connection_id) || empty($params['groups']) || empty($params['courseid']))
        return true;

    $sql = "SELECT id FROM {powerschool_course_cat} WHERE courseid_group=:courseid AND catid IN (".$params['groups'].")";
    if(PowerSchool::get_coursetype_from_courseid($params['courseid']) != 'groups' || !$DB->record_exists_sql($sql,array('courseid'=>$params['courseid'])))
        return true;

    redirect(new moodle_url('/local/powerschool/confirm-delete-group.php',$params));
    exit;
}
local_powerschool_confirm_del_cat();
