<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once('classes/Curl.php');
require_once($CFG->libdir.'/adminlib.php');

class PowerSchool{
	private $master_url;
	private $client_id;
	private $client_secret;
	private $access_token;
	private $token_type;
	private $token_expires_in;
	public $curl;
	public $school_ids;
	public $school_numbers;
	public $metadata;
	public $connection;

	public function __construct($connection_id){
	    global $DB;
		$settings = array();
		//$settings['debug'] = true;
		$this->curl = new PowerCurl($settings);

        $this->connection = $DB->get_record('powerschool_connections',array('id'=>$connection_id),'*',MUST_EXIST);

		$this->master_url = $this->connection->url;
		$this->client_id = $this->connection->client_id;
		$this->client_secret = $this->connection->client_secret;

		$this->access_token = $this->connection->access_token;
		$this->token_type = $this->connection->token_type;
		$this->token_expires_in = $this->connection->token_expires_in;
        $this->school_ids = (array)json_decode($this->connection->school_ids);
        $this->school_numbers = explode(',',clean_param($this->connection->school_numbers,PARAM_SEQUENCE));

        $this->school_ids = array_filter($this->school_ids,function($el){ return !empty($el);});

		if(empty($this->access_token) || ($this->token_expires_in - time())<10)
            $this->authenticate();

		$this->metadata = $this->request('get',"/ws/v1/metadata");
	}

	public function authenticate(){
	    global $DB;
        $count_conn = $DB->get_record_sql('SELECT COUNT(id) as connections, GROUP_CONCAT(school_numbers) as schools FROM {powerschool_connections}');
        $count_conn->schools = count(explode(',',$count_conn->schools));
		$response = json_decode($this->curl->get('http://extensions.sebale.net/subscriptions/check/'.get_config('local_powerschool', 'user_key'), array('key'=>get_config('local_powerschool', 'secret_key'),'schools'=>$count_conn->schools,'connections'=>$count_conn->connections)));
		if(isset($response->error)){
			throw new moodle_exception($response->error, 'local_powerschool', new moodle_url('/admin/'), null, $response->error_description);
		}
		if(isset($response->warning)){
            email_to_user(core_user::get_support_user(), 'Moodle', get_string('subscription_warning', 'local_powerschool'), get_string('subscription_warning_text', 'local_powerschool', $response->warning), get_string('subscription_warning_html', 'local_powerschool', $response->warning));
        }
		
		$header = array('Authorization: Basic '.base64_encode($this->client_id.':'.$this->client_secret), 
						'Content-Type: application/x-www-form-urlencoded;charset=UTF-8');
		$body = 'grant_type=client_credentials';
		
		$this->curl->setHeader($header);
		$response = json_decode($this->curl->post($this->master_url.'/oauth/access_token/', $body));
        if(!empty($this->curl->error)){
            throw new moodle_exception('curl error - '.$this->curl->errno, 'local_powerschool', new moodle_url('/admin/'), null, $this->curl->error);
        }
		
		if(isset($response->error)){
			throw new moodle_exception($response->error, 'local_powerschool', new moodle_url('/admin/'), null, $response->error_description);
		}
		
		$this->curl->resetopt();

        $this->access_token = $response->access_token;
        $this->token_type = $response->token_type;
        $this->token_expires_in = time() + $response->expires_in;
        $this->curl->resetHeader();

        $this->connection->access_token = $response->access_token;
        $this->connection->token_type = $response->token_type;
        $this->connection->token_expires_in = min($this->token_expires_in,strtotime('+13 day'));

        $this->school_ids = array();
		$schools = $this->request('get','/ws/v1/district/school');
		if(is_object($schools->schools->school)){
			if(in_array($schools->schools->school->school_number,$this->school_numbers)){
				$this->school_ids[$schools->schools->school->school_number] = $schools->schools->school->id;
			}
		}else{
			foreach($schools->schools->school as $school){
				if(in_array($school->school_number,$this->school_numbers)){
					$this->school_ids[$school->school_number] = $school->id;
				}
			}
		}

		if(empty($this->school_ids)){
			throw new moodle_exception('error_school', 'local_powerschool', new moodle_url('/admin/'), null, get_string('error_search_school', 'local_powerschool',$this->school_number));
		}
        $this->connection->school_ids = json_encode($this->school_ids);
        $this->connection->server_primary_ip = $this->curl->info['primary_ip'];

        $DB->update_record('powerschool_connections',$this->connection);
	}
	
	public function request($type, $path, $header = array(), $body = '', $include_header = false, $show_error_message = true){
		$header[] = 'Authorization: '.$this->token_type.' '.$this->access_token;
		$header[] = 'Accept:application/JSON ';
		$header[] = 'Content-Type:application/JSON ';
		$this->curl->setHeader($header);

        $data = $this->curl->$type($this->master_url.$path, $body,array(),$include_header);
        if(!empty($this->curl->error)){
            throw new moodle_exception('curl error - '.$this->curl->errno, 'local_powerschool', new moodle_url('/admin/'), null, $this->curl->error);
        }

		if(!empty($data)) {
			if($include_header){
				$res = explode("\r\n\r\n",$data);
				$data = $res[1];
				$header = $res[0];
			}
			$response = json_decode($data);
			if (json_last_error() != JSON_ERROR_NONE){
				try {
				    if(strpos($data,'timed out'))
                        throw new moodle_exception($data, 'local_powerschool', new moodle_url('/admin/'), null, print_r($data,true));
                    else
					    $response = new SimpleXMLElement($data);
				} catch (Exception $e) {
					$response = $data;
				}
			}

			if (isset($response->error) && $show_error_message) {
				throw new moodle_exception($response->error, 'local_powerschool', new moodle_url('/admin/'), null, $response->error_description);
			} elseif (isset($response->message) && $show_error_message) {
				throw new moodle_exception($response->message, 'local_powerschool', new moodle_url('/admin/'), null, print_r($response,true));
			}
		}else{
			throw new moodle_exception('Empty response', 'local_powerschool', new moodle_url('/admin/'), null, 'Empty response');
		}
		$this->curl->resetopt();
		$this->curl->resetHeader();

		if($include_header)
			return array($response,$header);
		else
			return $response;
	}

	public function create_subscribe(){
		global $CFG;

		$course_update = new stdClass();
		$course_update->resource = '/ws/v1/course/*';
		$course_update->event_type = 'UPDATE';

		$section_insert = new stdClass();
		$section_insert->resource = '/ws/v1/section/*';
		$section_insert->event_type = 'INSERT';

		$section_update = new stdClass();
		$section_update->resource = '/ws/v1/section/*';
		$section_update->event_type = 'UPDATE';

		$section_delete = new stdClass();
		$section_delete->resource = '/ws/v1/section/*';
		$section_delete->event_type = 'DELETE';

		$section_enrollment_insert = new stdClass();
		$section_enrollment_insert->resource = '/ws/v1/section_enrollment/*';
		$section_enrollment_insert->event_type = 'INSERT';

		$section_enrollment_update = new stdClass();
		$section_enrollment_update->resource = '/ws/v1/section_enrollment/*';
		$section_enrollment_update->event_type = 'UPDATE';

		$section_enrollment_delete = new stdClass();
		$section_enrollment_delete->resource = '/ws/v1/section_enrollment/*';
		$section_enrollment_delete->event_type = 'DELETE';

		$staff_insert = new stdClass();
		$staff_insert->resource = '/ws/v1/staff/*';
		$staff_insert->event_type = 'INSERT';

		$staff_update = new stdClass();
		$staff_update->resource = '/ws/v1/staff/*';
		$staff_update->event_type = 'UPDATE';

		$staff_delete = new stdClass();
		$staff_delete->resource = '/ws/v1/staff/*';
		$staff_delete->event_type = 'DELETE';

		$student_insert = new stdClass();
		$student_insert->resource = '/ws/v1/student/*';
		$student_insert->event_type = 'INSERT';

		$student_update = new stdClass();
		$student_update->resource = '/ws/v1/student/*';
		$student_update->event_type = 'UPDATE';

		$student_delete = new stdClass();
		$student_delete->resource = '/ws/v1/student/*';
		$student_delete->event_type = 'DELETE';

		$student_enroll = new stdClass();
		$student_enroll->resource = '/ws/v1/student/*';
		$student_enroll->event_type = 'SCHOOL_ENROLLMENT';


		$data = new stdClass();
		$data->event_subscriptions = new stdClass();
		$data->event_subscriptions->callback_url = $CFG->wwwroot.'/local/powerschool/events_listener.php';
		$data->event_subscriptions->event_subscription[] = $section_insert;
		$data->event_subscriptions->event_subscription[] = $section_update;
		$data->event_subscriptions->event_subscription[] = $section_delete;
		$data->event_subscriptions->event_subscription[] = $section_enrollment_insert;
		$data->event_subscriptions->event_subscription[] = $section_enrollment_update;
		$data->event_subscriptions->event_subscription[] = $section_enrollment_delete;
		$data->event_subscriptions->event_subscription[] = $staff_insert;
		$data->event_subscriptions->event_subscription[] = $staff_update;
		$data->event_subscriptions->event_subscription[] = $staff_delete;
		$data->event_subscriptions->event_subscription[] = $student_insert;
		$data->event_subscriptions->event_subscription[] = $student_update;
		$data->event_subscriptions->event_subscription[] = $student_delete;
		$data->event_subscriptions->event_subscription[] = $student_enroll;
		$data->event_subscriptions->event_subscription[] = $course_update;
		$data = json_encode($data);

		return $this->request('put','/ws/v1/event_subscription',array(),$data,true);
	}

	public function get_master_url(){
		return $this->master_url;
	}

	public function get_userid_by_powerid($powerid,$usertype){
		global $DB;
		$record = $DB->get_record_sql('SELECT d.userid
                                                FROM {user_info_field} f
                                                  JOIN {user_info_data} d ON d.fieldid=f.id
                                                  JOIN {user_info_field} fs ON fs.shortname = \'powerschool777usertype\'
                                                  JOIN {user_info_data} ds ON ds.fieldid=fs.id AND d.userid=ds.userid
                                                  JOIN {user_info_field} fc ON fc.shortname = \'powerschool777connection_id\'
                                                  JOIN {user_info_data} dc ON dc.fieldid=fc.id AND d.userid=dc.userid
                                                WHERE f.shortname = \'powerschool777id\' AND d.data=:id AND ds.data=\':usertype\' AND dc.data=:connection_id
                        ',array('id'=>$powerid,'usertype'=>$usertype,'connection_id'=>$this->connection->id));
		return (isset($record->userid) && $record->userid>0)?$record->userid:false;
	}

	public static function get_connection_from_courseid($course_id){
	    global $DB;
        $course_field = $DB->get_records('powerschool_course_fields',array('courseid'=>$course_id));
        $course_field = array_shift($course_field);

        return (isset($course_field->connection_id))?$course_field->connection_id:null;
    }

	public static function get_coursetype_from_courseid($course_id){
	    global $DB;
        $course_field = $DB->get_records('powerschool_course_fields',array('courseid'=>$course_id));
        $course_field = array_shift($course_field);

        return (isset($course_field->course_type))?$course_field->course_type:null;
    }
}




