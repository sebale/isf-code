<?php

$string['pluginname'] = 'PowerSchool';
$string['setting_title'] = '插入的所需設定';
$string['setting_users'] = '同步用戶的設定';
$string['temp_password'] = '臨時性密碼';
$string['temp_password_desc'] = '給PowerSchool新用戶的臨時性密碼';
$string['sync_moodle_user_change'] = '同步的moodle學生更改';
$string['sync_moodle_user_change_desc'] = '當moodle學生有更改時，與PowerSchool同步作出更改';
$string['master_url'] = 'PowerSchool 網站 url 地址';
$string['master_url_desc'] = 'PowerSchool 網站 url 地址';
$string['master_url_desc_help'] = '例子:""https://partner.powerschool.com""';
$string['client_id'] = '客戶ID';
$string['client_id_desc'] = '在你的網站放上客戶ID';
$string['client_id_desc_help'] = '開始頁 > 系統管理員 > 系統設定 > 插入管理儀表板 > 將Moodle及PowerSchool的設置同步化 > 同步化Moodle及PowerSchool的數據配置結構 ';
$string['client_secret'] = '客戶秘密';
$string['client_secret_desc'] = '在你的網站放上客戶秘密';
$string['client_secret_desc_help'] = '開始頁 > 系統管理員 > 系統設定 > 插入管理儀表板 > 將Moodle及PowerSchool的設置同步化 > 同步化Moodle及PowerSchool的數據配置結構 ';
$string['school_numbers'] = '學校編號';
$string['school_numbers_desc'] = '在你的網站放上學校編號';
$string['school_numbers_desc_help'] = '開始頁 > 地區設置 > 學校資料，如果你有多於一個紀錄，你必須列出學校編號。 例如： ""100,200""';
$string['setting_subscription'] = '設定同意插入';
$string['user_key'] = '用戶鑰匙';
$string['secret_key'] = '密碼鑰匙';
$string['course_not_sync'] = '這個課程並未同步至PowerSchool，請在PowerSchool創建課程及章節並去手動同步';
$string['system_course_category_desc'] = 'PowerSchool伺服器用的課程分類';
$string['system_course_category_desc_help'] = '所有在PowerSchool伺服器的課程都會被建立在這個分類之下';
$string['course_category'] = '課程分類';

$string['manual_sync'] = '手動同步 {$a}';
$string['sync_courses'] = '同步課程';
$string['users_in_school'] = '在學校 {$a} 用戶';
$string['users_for_sync'] = '同步 {$a} 用戶';
$string['powerschoolroot'] = 'PowerSchool';
$string['error_search_school'] = 'School width number {$a} not found';
$string['powerschool_coursecat_desc'] = '這是PowerSchool課程的分類';
$string['terms'] = '條款： ';
$string['missingterms'] = '欠缺的條款';
$string['users_system_role'] = '用戶系統角色';
$string['users_role'] = '用戶角色';
$string['student_role'] = '學生角色';
$string['co_teacher_role'] = '合作教師角色';
$string['lead_teacher_role'] = '領導教師角色';
$string['guardian_role'] = '監護人角色';
$string['staff_role'] = '職員角色';
$string['attendance'] = '出席';
$string['attendance_code'] = '出席代碼';
$string['attendance_code_desc'] = '管理員必須創建新的Moodle出席代碼或使用現有的出席代碼。出席代碼可在你的網站找到：開始頁 > 學校設置 > 出席代碼';
$string['attendance_enable'] = '出席啟用';
$string['attendance_enable_desc'] = '在課程登記學生活動，並創建PowerSchool出席';
$string['attendance_time'] = '出席時間';
$string['attendance_time_desc'] = 'PowerSchool AutoComm出席的時間（想知道如何創建，請看插入包裏的readme.md資料夾）';
$string['attendance_duration'] = '出席時長';
$string['attendance_duration_desc'] = '學生活動的時長，在此期限後創建的PowerSchool出席';
$string['section_setting'] = 'PowerSchool章節設定';
$string['section_type'] = '將其分為';
$string['section_type_help'] = '如果將PowerSchool上的課程章節創建成moodle課程或作為moodle課程上的用戶群組的話，只會在第一次同步前可用';
$string['course'] = '課程';
$string['groups'] = '用戶群組';
$string['assignment_sync'] = '活動同步';
$string['username_prefix'] = '用戶名稱字首';
$string['username_prefix_desc'] = '用戶名稱字首';
$string['username_prefix_desc_help'] = '如果你只有一個連接，或每個連接伺服器中的獨有用戶名稱為空，每個連接的用戶名稱字首都必須不同';
$string['course_prefix'] = '課程號碼字首';
$string['course_prefix_desc'] = '課程號碼字首';
$string['course_prefix_desc_help'] = '如果你只有一個連接，或每個連接伺服器中的獨有課程號碼為空，每個連接的課程號碼字首都必須不同';
$string['must_be_unique'] = '必須是獨有的';


/* name of profile fields */
$string['powerschool_category_name'] = 'Powerschool';
$string['powerschool777id'] = 'Powerschool ID';
$string['powerschool777usertype'] = 'User type';
$string['powerschool777local_id'] = 'Local ID';
$string['powerschool777demographics777gender'] = 'Gender';
$string['powerschool777demographics777birth_date'] = 'Birth Date';
$string['powerschool777demographics777projected_graduation_year'] = 'Projected Graduation Year';
$string['powerschool777addresses777physical777street'] = 'Addresses: Physical Street';
$string['powerschool777addresses777physical777city'] = 'Addresses: Physical City';
$string['powerschool777addresses777physical777state_province'] = 'Addresses: Physical State province';
$string['powerschool777addresses777physical777postal_code'] = 'Addresses: Physical Postal code';
$string['powerschool777addresses777mailing777street'] = 'Addresses: Mailing Street';
$string['powerschool777addresses777mailing777city'] = 'Addresses: Mailing City';
$string['powerschool777addresses777mailing777state_province'] = 'Addresses: Mailing State province';
$string['powerschool777addresses777mailing777postal_code'] = 'Addresses: Mailing Postal code';
$string['powerschool777addresses777mailing777grid_location'] = 'Addresses: Grid Location';
$string['powerschool777alerts777legal777description'] = 'Alerts: Legal Description';
$string['powerschool777alerts777legal777expires_date'] = 'Alerts: Legal Expires date';
$string['powerschool777alerts777discipline777description'] = 'Alerts: Discipline Description';
$string['powerschool777alerts777discipline777expires_date'] = 'Alerts: Discipline Expires date';
$string['powerschool777alerts777medical777description'] = 'Alerts: Medical Description';
$string['powerschool777alerts777medical777expires_date'] = 'Alerts: Medical Expires date';
$string['powerschool777alerts777other777description'] = 'Alerts: Other Description';
$string['powerschool777alerts777other777expires_date'] = 'Alerts: Other Expires date';
$string['powerschool777phones777main777number'] = 'Phones: Main Number';
$string['powerschool777school_enrollment777enroll_status'] = 'School enrollment: Enroll status';
$string['powerschool777school_enrollment777enroll_status_description'] = 'School enrollment: Enroll status description';
$string['powerschool777school_enrollment777enroll_status_code'] = 'School enrollment: enroll status code';
$string['powerschool777school_enrollment777grade_level'] = 'School enrollment: grade level';
$string['powerschool777school_enrollment777entry_date'] = 'School enrollment: entry date';
$string['powerschool777school_enrollment777exit_date'] = 'School enrollment: exit date';
$string['powerschool777school_enrollment777school_number'] = 'School enrollment: school number';
$string['powerschool777school_enrollment777school_id'] = 'School enrollment: school id';
$string['powerschool777school_enrollment777entry_code'] = 'School enrollment: entry code';
$string['powerschool777school_enrollment777entry_comment'] = 'School enrollment: entry comment';
$string['powerschool777school_enrollment777track'] = 'School enrollment: track';
$string['powerschool777school_enrollment777full_time_equivalency777fteid'] = 'School enrollment: full time equivalency fteid';
$string['powerschool777school_enrollment777full_time_equivalency777name'] = 'School enrollment: full time equivalency name';
$string['powerschool777school_enrollment777exit_code'] = 'School enrollment: exit code';
$string['powerschool777school_enrollment777district_of_residence'] = 'School enrollment: district of residence';
$string['powerschool777ethnicity_race777scheduling_reporting_ethnicity'] = 'Ethnicity race: Scheduling reporting ethnicity';
$string['powerschool777contact777emergency_contact_name1'] = 'Contact: emergency contact name1';
$string['powerschool777contact777emergency_contact_name2'] = 'Contact: emergency contact name2';
$string['powerschool777contact777emergency_phone1'] = 'Contact: emergency phone1';
$string['powerschool777contact777emergency_phone2'] = 'Contact: emergency phone2';
$string['powerschool777contact777guardian_email'] = 'Contact: guardian email';
$string['powerschool777contact777mother'] = 'Contact: mother';
$string['powerschool777contact777father'] = 'Contact: father';
$string['powerschool777contact777doctor_name'] = 'Contact: doctor name';
$string['powerschool777contact777doctor_phone'] = 'Contact: doctor phone';
$string['powerschool777contact777guardian_fax'] = 'Contact: guardian fax';
$string['powerschool777contact_info777email'] = 'Contact info: email';
$string['powerschool777initial_enrollment777district_entry_grade_level'] = 'Initial enrollment: district entry grade level';
$string['powerschool777initial_enrollment777school_entry_grade_level'] = 'Initial enrollment: school entry grade level';
$string['powerschool777initial_enrollment777school_entry_date'] = 'Initial enrollment: school entry date';
$string['powerschool777schedule_setup777home_room'] = 'Schedule setup: home room';
$string['powerschool777schedule_setup777next_school'] = 'Schedule setup: next school';
$string['powerschool777schedule_setup777sched_next_year_grade'] = 'Schedule setup: sched next year grade';
$string['powerschool777fees777fee777fee_amount'] = 'Fees Fee: amount';
$string['powerschool777fees777fee777fee_balance'] = 'Fees Fee: balance';
$string['powerschool777fees777fee777fee_description'] = 'Fees Fee: description';
$string['powerschool777fees777fee777fee_paid'] = 'Fees Fee: paid';
$string['powerschool777fees777fee777id'] = 'Fees Fee: ID';
$string['powerschool777fees777fee777transaction_date'] = 'Fees Fee: transaction date';
$string['powerschool777fees777fee777date_created'] = 'Fees Fee: date created';
$string['powerschool777fees777fee777date_modified'] = 'Fees Fee: date modified';
$string['powerschool777fees777fee777category_name'] = 'Fees Fee: category name';
$string['powerschool777fees777fee777type_name'] = 'Fees Fee: type name';
$string['powerschool777fees777fee777type_id'] = 'Fees Fee: type id';
$string['powerschool777fees777fee777priority'] = 'Fees Fee: priority';
$string['powerschool777fees777fee777pro_ratable_indicator'] = 'Fees Fee: pro ratable indicator';
$string['powerschool777fees777fee777group_transaction_id'] = 'Fees Fee: group transaction id';
$string['powerschool777fees777fee777school_id'] = 'Fees Fee: school id';
$string['powerschool777fees777fee777term_id'] = 'Fees Fee: term id';
$string['powerschool777fees777fee777year_id'] = 'Fees Fee: year id';
$string['powerschool777fees777fee777student_id'] = 'Fees Fee: student id';
$string['powerschool777fees777fee777fee_transaction777id'] = 'Fees Fee: fee transaction id';
$string['powerschool777fees777fee777fee_transaction777student_id'] = 'Fees Fee: fee transaction student id';
$string['powerschool777fees777fee777fee_transaction777transaction_amount'] = 'Fees Fee: fee transaction transaction amount';
$string['powerschool777fees777fee777fee_transaction777global_starting_balance'] = 'Fees Fee: fee transaction global starting balance';
$string['powerschool777fees777fee777fee_transaction777date'] = 'Fees Fee: fee transaction date';
$string['powerschool777fees777fee777fee_transaction777description'] = 'Fees Fee: fee transaction description';
$string['powerschool777fees777fee777fee_transaction777group_transaction_id'] = 'Fees Fee: fee transaction group transaction id';
$string['powerschool777fees777fee777fee_transaction777school_id'] = 'Fees Fee: fee transaction school id';
$string['powerschool777fees777fee777fee_transaction777start_year'] = 'Fees Fee: fee transaction start year';
$string['powerschool777fees777fee777fee_transaction777starting_balance'] = 'Fees Fee: fee transaction starting balance';
$string['powerschool777fees777fee777fee_transaction777global_net_balance'] = 'Fees Fee: fee transaction global net balance';
$string['powerschool777fees777fee777fee_transaction777transaction_type'] = 'Fees Fee: fee transaction type';
$string['powerschool777fees777fee_balance777id'] = 'Fees: fee balance id';
$string['powerschool777fees777fee_balance777student_id'] = 'Fees: fee balance student id';
$string['powerschool777fees777fee_balance777year_id'] = 'Fees: fee balance year id';
$string['powerschool777fees777fee_balance777debit'] = 'Fees: fee balance debit';
$string['powerschool777fees777fee_balance777credit'] = 'Fees: fee balance credit';
$string['powerschool777fees777fee_balance777school_id'] = 'Fees: fee balance school id';
$string['powerschool777fees777fee_balance777balance'] = 'Fees: fee balance ';
$string['powerschool777fees777fee_balance777fee_exemption_status'] = 'Fees: fee balance exemption status';
$string['powerschool777fees777fee_balance777fee_exemption_status_code'] = 'Fees: fee balance exemption status code';
$string['powerschool777lunch777balance_1'] = 'Lunch: balance 1';
$string['powerschool777lunch777balance_2'] = 'Lunch: balance 2';
$string['powerschool777lunch777balance_3'] = 'Lunch: balance 3';
$string['powerschool777lunch777balance_4'] = 'Lunch: balance 4';
$string['powerschool777lunch777last_meal'] = 'Lunch: meal';
$string['powerschool777lunch777lunch_id'] = 'Lunch: id';
$string['powerschool777addresses777home777street'] = 'Addresses: home street';
$string['powerschool777addresses777home777state_province'] = 'Addresses: home state/province';
$string['powerschool777addresses777home777postal_code'] = 'Addresses: home postal code';
$string['powerschool777addresses777home777city'] = 'Addresses: home city';
$string['powerschool777school_affiliations777school_affiliation777school_id'] = 'School affiliation: school_id';
$string['powerschool777school_affiliations777school_affiliation777type'] = 'School affiliation: type';
$string['powerschool777school_affiliations777school_affiliation777status'] = 'School affiliation: status';
$string['powerschool777demographics777district_entry_date'] = 'Demographics: district entry date';
$string['powerschool777initial_enrollment777district_entry_date'] = 'Initial enrollment: district entry date';
$string['powerschool777phones777home_phone'] = 'Phones: home';
$string['powerschool777addresses777physical777grid_location'] = 'Addresses: physical grid location';
$string['powerschool777ethnicity_race777federal_ethnicity'] = 'Ethnicity race: federal ethnicity';
$string['powerschool777state_province_id'] = 'State province id';
$string['powerschool777unique_id'] = 'Unique id';
$string['powerschool777school_id'] = 'School id';
$string['powerschool777connection_id'] = 'Connection id';
/* name of profile fields */

$string['include_expansions'] = '包括擴展';
$string['demographics'] = '個人資料';
$string['addresses'] = '地址';
$string['alerts'] = '提示';
$string['phones'] = '電話';
$string['school_enrollment'] = '學校註冊';
$string['ethnicity_race'] = '種族';
$string['contact'] = '聯絡人';
$string['contact_info'] = '聯絡人資料';
$string['initial_enrollment'] = '早期註冊';
$string['schedule_setup'] = '時間表設定';
$string['fees'] = '費用';
$string['lunch'] = '午餐';
$string['activities'] = '活動';
$string['c_studentlocator'] = 'Student locator';
$string['u_students_extension'] = 'Students extension';
$string['u_bus'] = 'Bus';
$string['sync'] = 'Sync';
$string['include_users'] = 'Include Users';
$string['students'] = 'Students';
$string['staffs'] = 'Staff';
$string['emails'] = 'Emails';
$string['school_affiliations'] = 'School affiliations';
$string['processing'] = 'Processing...';
$string['must_exist_students_or_staffs'] = 'Must be included students or staffs';
$string['sync_error'] = 'Sync Error';
$string['sync_error_desc'] = 'Please reload page after 1 minute or contact to administrator';
$string['associate_category'] = 'Associate category ';
$string['associate_category_full'] = 'Associate PowerSchool category to Moodle course module (only for new PowerSchool Assignments)';
$string['not_set'] = 'Not Set';
$string['school_category'] = 'Course category for school';
$string['courses_sync_per_school'] = 'Courses sync for school: {$a}';
$string['sync_new_ps_moodle_group'] = 'Sync new assignments from Powerschool section: {$a}';
$string['sync_new_ps_moodle'] = 'Sync new assignments from Powerschool section';
$string['associate_moodle_user'] = 'Association users';
$string['associate_moodle_user_desc'] = 'Synchronization new users on email or username';
$string['ps_grade_scale'] = 'Grade scale from PowerSchool';
$string['save'] = 'Save Only';
$string['save_sync_all'] = 'Save and Sync all data';
$string['save_sync_users'] = 'Save and Sync users';
$string['save_sync_courses'] = 'Save and Sync courses';
$string['terms_sync_per_school'] = 'Terms sync per school: {$a}';
$string['manage_connections'] = 'Manage connections';
$string['create_new_connection'] = 'Create new connection';
$string['edit_connection'] = 'Edit connection';
$string['create_connection'] = 'Create connection';
$string['server_connected'] = 'This server is already connected';
$string['forcepasswordchange'] = 'Force password change';
$string['forcepasswordchange_desc'] = 'When user first time login he must change temporary password';
$string['first_server'] = 'First Server';
$string['custom_fields'] = 'Legacy сustom fields';
$string['student_custom_fields_desc'] = 'Legacy сustom fields from PowerSchool for student';
$string['student_custom_fields_desc_help'] = 'Legacy сustom fields locate in PowerSchool: Start Page > System Administrator > Page and Data Management > Custom Student Screens on your screen Edit Fields and you can see fields list. You must enter as example: ""Field Name|label,Field Name 2|label_2""';
$string['staff_custom_fields_desc'] = 'Legacy сustom fields from PowerSchool for staff';
$string['staff_custom_fields_desc_help'] = 'Legacy сustom fields locate in PowerSchool: Start Page > System Administrator > Page and Data Management > Custom Staff Screens on your screen Edit Fields and you can see fields list. You must enter as example: ""Field Name|label,Field Name 2|label_2""';
$string['deleteselectedgroup'] = 'Confirm delete selected group';
$string['confirm_delete_groups'] = 'WARNING: one or more groups is synced with PowerSchool, if you deleted this group sync to be stopped.';
$string['section_fullname'] = 'Schema for section name <br>(if section synced as course)';
$string['section_fullname_group'] = 'Schema for section name <br>(if section synced as course group)';
$string['section_fullname_desc'] = 'Description: Schema for section name';
$string['section_fullname_desc_help'] = 'Schema indicates how it will emerge course name, allowed tags: <br>"
<ol>
<li>[[course_name]]</li>
<li>[[course_number]]</li>
<li>[[section_number]]</li>
<li>[[section_expression]]</li>
<li>[[terms_abbreviation]]</li>
</ol>
"For section as course default: ""[[course_name]] [[section_number]] / [[section_expression]]"""
<br>
"For section as group default: ""[[section_number]] / [[section_expression]] [[terms_abbreviation]]""';

$string['field_for_student_cohort'] = 'Profile fields for Students cohort';
$string['field_for_staff_cohort'] = 'Profile fields for Staff cohort';
$string['no_create_cohorts'] = 'Not create cohorts';
$string['not_synced_profile_fields'] = 'Profile fields not synced';
$string['extensions'] = 'Extensions';
$string['subscription_warning'] = 'Moodle plugin PowerSchool: subscription warning';
$string['subscription_warning_text'] = 'Moodle plugin PowerSchool: subscription warning

{$a}';
$string['subscription_warning_html'] = 'Moodle plugin PowerSchool: subscription warning <div style=""background-color: #fcf8e3; border: 1px solid #fbeed5;border-radius: 4px;margin-bottom: 20px;padding: 8px 35px 8px 14px;text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);"">{$a}</div>';
$string['migrate_sections'] = 'Migrate sections';
$string['migrate_sections_extension'] = 'Extension';
$string['migrate_sections_field'] = 'Field name';
$string['setting_available_modules'] = 'Available modules';
$string['available_modules_to_create'] = 'Available modules to sync with PowerSchool';
$string['available_modules_to_create_desc'] = 'List of course modules available to sync with PowerSchool';
$string['you_must_set_restrict_access_for_sync'] = 'You must set ""Restrict access"" available for group for sync with PowerSchool';
$string['alert_migrate_sections'] = 'Warning: courses for migrating section should be synchronized equally (as User group or as Course)';
$string['parents'] = 'Parents';
$string['parent_role'] = 'Parent role';
$string['parent_course_role'] = 'Parent role on course';
$string['not_assign'] = 'Not assign';
$string['update_course_cat'] = 'Update course category for existing course';
$string['standard_sync'] = 'Sync assignment standard';
$string['count_in_final_grade'] = 'Count in Final Grade';
$string['standards'] = 'Standards';
$string['import_standards'] = 'Import standards as outcome';
