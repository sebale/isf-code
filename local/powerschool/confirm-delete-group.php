<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * PowerSchool Settings
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require_once('classes/Courses.php');
require_once('assignment_sync_form.php');

$courseid = required_param('courseid', PARAM_INT);
$groupids = required_param('groups', PARAM_SEQUENCE);

$PAGE->set_url('/local/powerachool/confirm-delete-group.php', array('courseid'=>$courseid,'groups'=>$groupids));
$PAGE->set_pagelayout('standard');

// Make sure course is OK and user has access to manage groups
if (!$course = $DB->get_record('course', array('id' => $courseid))) {
    print_error('invalidcourseid');
}

require_login($course);
$context = context_course::instance($courseid);
require_capability('local/powerschool:view', $context);
require_capability('moodle/course:managegroups', $context);

$PAGE->set_context($context);
$PAGE->set_title(get_string('powerschoolroot', 'local_powerschool').': '.get_string('deleteselectedgroup', 'local_powerschool'));
$PAGE->set_heading($course->fullname . ': '. get_string('deleteselectedgroup', 'local_powerschool'));

$course_field = $DB->get_records('powerschool_course_fields',array('courseid'=>$course->id));
$course_field = array_shift($course_field);
if(!isset($course_field->connection_id)){
    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('assignment_sync', 'local_powerschool'));

    echo html_writer::div(html_writer::span(get_string('course_not_sync','local_powerschool')), 'alert alert-error');

    echo $OUTPUT->footer();
    exit;
}

$plugin = new Courses($course_field->connection_id);

echo $OUTPUT->header();
$optionsyes = array('courseid'=>$courseid, 'groups'=>$groupids, 'ps_confirm'=>1);
$optionsno = array('id'=>$courseid);
$message = html_writer::tag('p',get_string('confirm_delete_groups', 'local_powerschool'),array('class'=>'alert alert-error'));

$formcontinue = new single_button(new moodle_url('/group/delete.php', $optionsyes), get_string('yes'), 'post');
$formcancel = new single_button(new moodle_url('/group/index.php', $optionsno), get_string('no'), 'get');
echo $OUTPUT->confirm($message, $formcontinue, $formcancel);
echo $OUTPUT->footer();
