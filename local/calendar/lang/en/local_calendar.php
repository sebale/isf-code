<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * ISF calendar lang file
 *
 * @package    local_calendar
 * @copyright  ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

$string['pluginname'] = 'Calendar';
$string['calendar:view'] = 'Calendar View';
$string['calendar:manage'] = 'Manage Calendar Events';
$string['calendar:addsubscription'] = 'Сalendar Manage Subscriptions';
$string['createevent'] = 'Create event';
$string['noncourseevents'] = 'Non Course Events';
$string['managecalendars'] = 'Manage Calendars';
$string['today'] = 'Today';
$string['filter'] = 'Filter';
$string['systemevents'] = 'System Events';
$string['due'] = 'Due';
$string['createevent'] = 'Create new event';
$string['updateevent'] = 'Update event';
$string['eventupdated'] = 'Event has been successfully updated';
$string['eventcreated'] = 'Event has been successfully created';
$string['noevents'] = 'No events';
$string['showmore'] = 'Show More';
$string['hide'] = 'Hide';
$string['apply'] = 'Apply';
$string['save'] = 'Save';
$string['cancel'] = 'Cancel';
$string['duedate'] = 'Due date';
$string['startson'] = 'Starts on';
$string['endson'] = 'Ends on';
$string['description'] = 'Description';
$string['courses'] = 'Courses';
$string['selectcourse'] = 'Select course...';
$string['visible'] = 'Visible';
$string['hidden'] = 'Hidden';
$string['eventtype'] = 'Event type';
$string['privateevent'] = 'Private Event';
$string['courseevent'] = 'Course Event';
$string['duedateevent'] = 'Due date event';
$string['name'] = 'Name';
$string['selectcolor'] = 'Select color';
