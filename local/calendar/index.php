<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Displays information about all the assignment modules in the requested course
 *
 * @package   local_calendar
 * @copyright 2017 ISF
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once("../../config.php");
require_once("lib.php");
require_once("locallib.php");
require_once($CFG->dirroot.'/calendar/lib.php');

require_login();

$id         = optional_param('id', 0, PARAM_INT);
$courseid   = optional_param('courseid', 0, PARAM_INT);
$date       = optional_param('date', date('Y-m-d'), PARAM_RAW);

$title = get_string('pluginname', 'local_calendar');

$course = null;
$params = array();

if ($courseid) {
    $course = $DB->get_record('course', array('id'=>$courseid));
}

if (isset($course->id)){
    require_login($course);
    $context = context_course::instance($course->id);
    $params['courseid'] = $course->id;
} else {
    $context = context_system::instance();
}

require_capability('local/calendar:view', $context);
$PAGE->set_context($context);
$PAGE->set_url(new moodle_url("/local/calendar/index.php", $params));
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_pagelayout((isset($course->id)) ? 'course' : 'standard');
$PAGE->set_heading($title);

$data = local_calendar_get_events();
$renderer = $PAGE->get_renderer('local_calendar');

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo $renderer->print_calendar($data);

$PAGE->requires->js_call_amd('local_calendar/calendar', 'init', array('today'=>$date, 'eventid'=>$id));

echo $OUTPUT->footer();
