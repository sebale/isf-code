<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Displays information about all the assignment modules in the requested course
 *
 * @package   local_quicklinks
 * @copyright 2017 ISF
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

function local_calendar_get_events() {
    global $CFG, $DB, $USER;

    $data = new stdClass();
    $data->filtercourses = array();
    $filtercourse = array();
    $courseshown = false;

    list($data->courses, $group, $user) = calendar_set_filters($filtercourse);

    $defaultlookahead = CALENDAR_DEFAULT_UPCOMING_LOOKAHEAD;
    if (isset($CFG->calendar_lookahead)) {
        $defaultlookahead = intval($CFG->calendar_lookahead);
    }
    $lookahead = get_user_preferences('calendar_lookahead', $defaultlookahead);
    $mycourses = enrol_get_my_courses();
    if (count($mycourses) > 0){
        $data->courses = array(1=>1);
        foreach($mycourses as $mc){
            $data->courses[$mc->id] = $mc->id;
            $data->filtercourses[$mc->id] = $mc->fullname;
        }
    }

    // course colors and settings
    $data->course_disables = array(); $data->course_colors = array();
    $courses_state = $DB->get_records('local_calendar_courses', array('userid'=>$USER->id));
    foreach ($courses_state as $cstate){
        if (!empty($cstate->color) and (strlen($cstate->color) == 3 or strlen($cstate->color) == 6)){
            $data->course_colors[$cstate->courseid] = $cstate->color;
        }
        if ($cstate->state == 0){
            $data->course_disables[$cstate->courseid] = $cstate->state;
        }
    }

    // subscriptions colors and settings
    $subscriptions = $DB->get_records_sql("
        SELECT s.*, cs.color, cs.state
          FROM {event_subscriptions} s
     LEFT JOIN {local_calendar_subscriptions} cs ON cs.subscriptionid = s.id AND cs.userid = s.userid
         WHERE s.userid = :userid", array('userid'=>$USER->id));

    $data->sub_disables = $DB->get_records_select_menu("local_calendar_subscriptions", "userid = :userid AND state = 0", array('userid'=>$USER->id), '', $fields='subscriptionid, state');
    $data->subscriptions = array(0=>'3a87ad');
    foreach ($subscriptions as $subscription){
        $data->subscriptions[$subscription->id] = $subscription;
    }

    $events = calendar_get_events(time()-31536000, time()+31536000, $user, $group, $data->courses, true, true);

    $data->events = array();
    if (count($events)) {
        foreach($events as $event){
            if ($event->eventtype == 'due'){
                $event->timestart = strtotime(userdate($event->timestart));
                $event->timeduration = $event->timestart;
            }
            $event->subscriptionid = ($event->subscriptionid) ? $event->subscriptionid : 0;
            $eventname = trim(preg_replace("/&#?[a-z0-9]+;/i",'', addslashes(strip_tags($event->name))));

            $data->events[] = array('id'=>$event->id,
                                    'title'=>$eventname,
                                    'start'=>date('c', $event->timestart),
                                    'end'=>(($event->timeduration and $event->eventtype != 'due') ? '"'.date('c', ($event->timestart + $event->timeduration)).'"' :  '""'),
                                    'allDay'=>(($event->timeduration) ? "false" :  "true"),
                                    'color'=>((isset($data->subscriptions[$event->subscriptionid]->color)) ? "#".$data->subscriptions[$event->subscriptionid]->color : (($event->courseid > 0 and isset($data->course_colors[$event->courseid])) ? "#".$data->course_colors[$event->courseid] : "#3a87ad")),
                                    'className'=>(($event->subscriptionid > 0) ? 'ev_'.$event->subscriptionid : 'ev_0').((isset($data->sub_disables[$event->subscriptionid]) or isset($data->course_disables[$event->courseid])) ? ' hidden' : ' ').(($event->courseid > 0) ? ' ec_'.$event->courseid : (($event->subscriptionid > 0) ? '' : ' ec_0'))
                                   );
        }
    }

    return $data;
}

function local_calendar_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options=array()) {
    global $CFG;
    require_once($CFG->dirroot . '/repository/lib.php');

    // Leave this line out if you set the itemid to null in make_pluginfile_url (set $itemid to 0 instead).
    $itemid = array_shift($args); // The first item in the $args array.

    // Use the itemid to retrieve any relevant data records and perform any security checks to see if the
    // user really does have access to the file in question.

    // Extract the filename / filepath from the $args array.
    $filename = array_pop($args); // The last item in the $args array.
    if (!$args) {
        $filepath = '/'; // $args is empty => the path is '/'
    } else {
        $filepath = '/'.implode('/', $args).'/'; // $args contains elements of the filepath
    }

    // Retrieve the file from the Files API.
    $fs = get_file_storage();
    $file = $fs->get_file($context->id, 'local_calendar', $filearea, $itemid, $filepath, $filename);
    if (!$file) {
        return false; // The file does not exist.
    }

    send_stored_file($file, 86400, 0, $forcedownload, $options);
}

function local_calendar_hex2rgb($hex, $type = '') {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
    if ($type == 'string'){
        $rgb = "$r, $g, $b";
    } else {
        $rgb = array($r, $g, $b);
    }
   //return implode(",", $rgb); // returns the rgb values separated by commas
   return $rgb; // returns an array with the rgb values
}


