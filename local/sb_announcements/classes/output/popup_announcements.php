<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Contains class used to prepare a popup announcements for display.
 *
 * @package   local_sb_announcements
 * @copyright 2017 ISF
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_sb_announcements\output;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/local/sb_announcements/lib.php');

use renderable;
use templatable;
use moodle_url;
use core_user;

/**
 * Class to prepare a popup announcements for display.
 *
 * @package   local_sb_announcements
 * @copyright 2017 ISF
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class popup_announcements implements templatable, renderable {

    /**
     * @var \stdClass The announcement.
     */
    protected $announcement;

    /**
     * Constructor.
     *
     * @param \stdClass $announcement
     */
    public function __construct($announcement) {
        $this->announcement = $announcement;
    }

    public function export_for_template(\renderer_base $output) {
        global $USER, $CFG;
        require_once($CFG->dirroot.'/local/sb_announcements/locallib.php');

        $context = clone $this->announcement;

        if ($context->useridto == $USER->id && $context->timeusertodeleted) {
            $context->deleted = true;
        } else {
            $context->deleted = false;
        }

        $context->startdatepretty = get_string('ago', 'message', format_time(time() - $context->startdate));
        $context->text = format_text($context->body, FORMAT_HTML, array());
        $context->body = local_sb_announcements_formatted_text($context);
        $context->read = $context->new ? false : true;
        $context->shortenedtitle = shorten_text($context->title, 125);

        $iconurl = $output->pix_url('i/marker', 'core');
        $context->iconurl = $iconurl->out();

        return $context;
    }
}
