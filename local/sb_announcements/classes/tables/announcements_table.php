<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Announcements version file.
 *
 * @package    local_sb_announcements
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/tablelib.php');

class announcements_table extends table_sql {

    public $_roles = array();
    public $_courses = array();
    public $_cohorts = array();

    function __construct($uniqueid, $search) {
        global $CFG, $USER, $PAGE;

        parent::__construct($uniqueid);
        $this->_roles = local_announcemets_get_roles();
        $this->_courses = local_announcemets_get_courses();
        $this->_cohorts = local_announcemets_get_cohorts();

        $columns = array('type', 'data', 'title', 'startdate', 'enddate', 'created');

        $headers = array(
            get_string('type', 'local_sb_announcements'),
            get_string('data', 'local_sb_announcements'),
            get_string('title', 'local_sb_announcements'),
            get_string('startdate', 'local_sb_announcements'),
            get_string('enddate', 'local_sb_announcements'),
            get_string('created', 'local_sb_announcements')
        );

        if (has_capability('local/sb_announcements:edit', context_system::instance())) {
            $columns[] = 'actions';
            $headers[] = get_string('actions', 'local_sb_announcements');
        }

        $this->define_columns($columns);
        $this->define_headers($headers);
        $this->sortable(true, 'startdate', SORT_DESC);

        $sql_search = ($search) ? " AND (a.title LIKE '%$search%' OR a.body LIKE '%$search%')" : "";

        $fields = "a.id, a.type, a.data, a.title, a.class, a.startdate, a.enddate, a.timecreated, CONCAT(u.firstname, ' ', u.lastname) as created, a.userid, a.state, a.id as actions ";
        $from = "{local_sb_announcements} a LEFT JOIN {user} u ON u.id = a.userid";
        $where = 'a.id > 0' . $sql_search;

        $params = array();
        if (!has_capability('local/sb_announcements:canseeallannouncements', context_system::instance())) {
            $where .= " AND a.userid = :userid ";
            $params['userid'] = $USER->id;
        }

        $this->set_sql($fields, $from, $where, $params);
        $this->define_baseurl($PAGE->url);
    }

    function col_type($values)
    {
        return ($values->type) ? get_string($values->type.'type', 'local_sb_announcements') : '-';
    }

    function col_created($values)
    {
        return ($values->created) ? html_writer::link(new moodle_url('/user/profile.php', array('id' => $values->userid)), $values->created) : '-';
    }

    function col_startdate($values)
    {
        return ($values->startdate) ? userdate($values->startdate, get_string('strftimedatetime', 'langconfig')) : '-';
    }

    function col_enddate($values)
    {
        return ($values->enddate) ? userdate($values->enddate, get_string('strftimedatetime', 'langconfig')) : '-';
    }

    function col_data($values)
    {
        global $DB;
        $data = '';
        if ($values->data != '') {
            if ($values->type == LOCAL_ANNOUNCEMENTS_COURSETYPE) {
                $courses = explode(',', $values->data);
                if (count($courses) > 0) {
                    $cn = array();
                    foreach ($courses as $course) {
                        if (isset($this->_courses[$course])){
                            $cn[] = html_writer::link(new moodle_url('/course/view.php', array('id' => $course)), $this->_courses[$course]);
                        }
                    }
                    $data = implode(', ', $cn);
                }
            } elseif ($values->type == LOCAL_ANNOUNCEMENTS_SYSTEMTYPE) {
                $roles = explode(',', $values->data);
                if (count($roles) > 0) {
                    $cn = array();
                    foreach ($roles as $role) {
                        if (isset($this->_roles[$role])){
                            $cn[] = $this->_roles[$role];
                        }
                    }
                    $data = implode(', ', $cn);
                }
            } elseif ($values->type == LOCAL_ANNOUNCEMENTS_COHORTTYPE) {
                $cohorts = explode(',', $values->data);
                if (count($cohorts) > 0) {
                    $cn = array();
                    foreach ($cohorts as $cohort) {
                        if (isset($this->_cohorts[$cohort])){
                            $cn[] = $this->_cohorts[$cohort];
                        }
                    }
                    $data = implode(', ', $cn);
                }
            }
        }

        return $data;
    }

    function col_actions($values)
    {
        global $OUTPUT, $PAGE;

        if ($this->is_downloading() or !has_capability('local/sb_announcements:edit', context_system::instance())) {
            return '';
        }

        $strdelete = get_string('delete');
        $stredit = get_string('edit');
        $strshow = get_string('show');
        $strhide = get_string('hide');

        $edit = array();
        $aurl = new moodle_url('/local/sb_announcements/edit.php', array('id' => $values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/edit', $stredit, 'core', array('class' => 'iconsmall')));

        $aurl = new moodle_url('/local/sb_announcements/edit.php', array('action' => 'delete', 'id' => $values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/delete', $strdelete, 'core', array('class' => 'iconsmall')));

        if ($values->state > 0) {
            $aurl = new moodle_url('/local/sb_announcements/edit.php', array('action' => 'hide', 'id' => $values->id));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/hide', $strhide, 'core', array('class' => 'iconsmall')));
        } else {
            $aurl = new moodle_url('/local/sb_announcements/edit.php', array('action' => 'show', 'id' => $values->id));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/show', $strshow, 'core', array('class' => 'iconsmall')));
        }

        $color = '';

        if ($values->class) {
            $color = '<span class="raw-color" style="display:none;">' . $values->class . '</span>';
        }

        return implode('', $edit) . $color;
    }
}
