<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Announcements version file.
 *
 * @package    local_sb_announcements
 * @author     ISF
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

$string['pluginname'] = 'Announcements';
$string['announcements'] = 'Announcements';
$string['my_announcements'] = 'My announcements';
$string['announcementsmanage'] = 'Manage announcements';
$string['announcementscreate'] = 'Create announcement';
$string['announcementsedit'] = 'Edit announcement';
$string['settings'] = 'Settings';
$string['announcementsdroot'] = 'Announcements';
$string['enabled'] = 'Enabled announcements';
$string['enabled_desc'] = 'Enable announcements';
$string['cleanup'] = 'Delete announcements after (days)';
$string['cleanup_desc'] = 'Delete announcements after expiration date (0 - do not delete)';
$string['number'] = 'Number of announcements';
$string['number_desc'] = 'Number of announcements to show on dashboard';
$string['order'] = 'Announcements ordering';
$string['order_desc'] = 'Show latest announcements or randomly';
$string['order_last'] = 'Latest announcements';
$string['order_random'] = 'Randomly';
$string['sb_announcements:view'] = 'Announcements View';
$string['view'] = 'View';
$string['sb_announcements:manage'] = 'Announcements Manage';
$string['sb_announcements:edit'] = 'Edit Announcements';
$string['sb_announcements:canseeallcourses'] = 'Can see all courses';
$string['sb_announcements:canseeallannouncements'] = 'Can see all Announcements';
$string['sb_announcements:editsystemtype'] = 'Create/Edit system Announcements';
$string['sb_announcements:editcoursetype'] = 'Create/Edit courses Announcements';
$string['sb_announcements:editcohorttype'] = 'Create/Edit cohorts Announcements';
$string['sb_announcements:canseeallcohorts'] = 'Can see all cohorts';
$string['title'] = 'Announcement title';
$string['required_field'] = 'Field is required!';
$string['description'] = 'Announcement text';
$string['announcements_image'] = 'Announcement image';
$string['announcementdate'] = 'Announcement date';
$string['startdate'] = 'Publish start date';
$string['enddate'] = 'Publish end date';
$string['state'] = 'Publish state';
$string['visibility'] = 'Visibility';
$string['create_new'] = 'Сreate New Announcement';
$string['create_new_sm'] = 'Сreate New';
$string['created'] = 'Created by';
$string['actions'] = 'Actions';
$string['showall'] = 'Show all';
$string['nothing'] = 'Nothing to display';
$string['YOUR_URI'] = 'YOUR_URI';
$string['YOUR_URI_TITLE'] = 'YOUR_URI_TITLE';
$string['save'] = 'Save';
$string['create'] = 'Create';
$string['search'] = 'Search';
$string['close'] = 'Close';
$string['addtofeed'] = 'Add to Feed';
$string['body'] = 'Message';
$string['type'] = 'Type';
$string['data'] = 'Data';
$string['announcement_date'] = 'Announcement date';
$string['manage_announcements'] = 'Manage Announcements';
$string['delete_announcement'] = 'Delete Announcement';
$string['delete_announcement_text'] = 'Do you really want to delete announcement \'{$a}\'?';
$string['message'] = 'Message';
$string['date'] = 'Date';
$string['author'] = 'Author';
$string['mark_as_unread'] = 'Mark as unread';
$string['mark_as_read'] = 'Mark as read';
$string['mark_all_as_read'] = 'Mark all as read';
$string['class'] = 'Class';
$string['announcementdeleted'] = 'Announcement deleted';
$string['announcementcreated'] = 'Announcement created';
$string['announcementupdated'] = 'Announcement updated';
$string['announcementclass'] = 'Announcement Class';
$string['selecttype'] = 'Select type';
$string['selectcourse'] = 'Select course';
$string['selectclass'] = 'Select class';
$string['selectrole'] = 'Select role';
$string['selectcohort'] = 'Select cohort';
$string['roles'] = 'Roles';
$string['systemtype'] = 'System';
$string['coursetype'] = 'Course';
$string['cohorttype'] = 'Cohort';
$string['announcementtype'] = 'Announcement type';
$string['cleanclass'] = 'Clean (white)';
$string['infoclass'] = 'Info (blue)';
$string['warningclass'] = 'Warning (yellow)';
$string['dangerclass'] = 'Danger (red)';
$string['successclass'] = 'Success (green)';
$string['course'] = 'Course';
$string['cohort'] = 'Cohort';
$string['visible'] = 'Visible';
$string['sendnotification'] = 'Send notification';
$string['announcementemail'] = '<p>{$a->text}</p>';
$string['messageprovider:announcements_notify'] = 'Announcements notification';
$string['noannouncements'] = 'You have no Announcements';
$string['toggleannouncementsmenu'] = 'Toggle Announcements menu';
$string['announcementswindow'] = 'Toggle Announcements menu';

// Events
$string['event_mail_send'] = 'Announcement mail send';
$string['event_mail_send_description'] = 'Announcement mail send description';
$string['event_mail_send_reminder'] = 'Announcement mail send reminder';
$string['event_mail_send_reminder_description'] = 'Announcement mail send reminder description';
