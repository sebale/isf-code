<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * External functions and service definitions.
 *
 * @package    local_sb_announcements
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$functions = array(
    'local_sb_announcements_get_popup_announcements' => array(
        'classname' => 'local_sb_announcements_external',
        'methodname' => 'get_popup_announcements',
        'classpath' => 'local/sb_announcements/externallib.php',
        'description' => 'Retrieve a list of popup announcements for a user',
        'type' => 'read',
        'ajax' => true,
        'services' => array(MOODLE_OFFICIAL_MOBILE_SERVICE),
    ),
    'local_sb_announcements_get_unread_popup_announcement_count' => array(
        'classname' => 'local_sb_announcements_external',
        'methodname' => 'get_unread_popup_announcement_count',
        'classpath' => 'local/sb_announcements/externallib.php',
        'description' => 'Retrieve the count of unread popup announcements for a given user',
        'type' => 'read',
        'ajax' => true,
        'services' => array(MOODLE_OFFICIAL_MOBILE_SERVICE),
    ),
    'local_sb_announcements_mark_announcement_read' => array(
        'classname' => 'local_sb_announcements_external',
        'methodname' => 'mark_announcement_read',
        'classpath' => 'local/sb_announcements/externallib.php',
        'description' => 'Mark announcement as read',
        'type' => 'write',
        'ajax' => true,
        'services' => array(MOODLE_OFFICIAL_MOBILE_SERVICE),
    ),
    'local_sb_announcements_mark_all_read' => array(
        'classname' => 'local_sb_announcements_external',
        'methodname' => 'mark_all_announcements_read',
        'classpath' => 'local/sb_announcements/externallib.php',
        'description' => 'Mark all announcements as read',
        'type' => 'write',
        'ajax' => true,
        'services' => array(MOODLE_OFFICIAL_MOBILE_SERVICE),
    ),
    'local_sb_announcements_delete_announcement' => array(
        'classname' => 'local_sb_announcements_external',
        'methodname' => 'delete_announcement',
        'classpath' => 'local/sb_announcements/externallib.php',
        'description' => 'Delete announcement',
        'type' => 'write',
        'ajax' => true,
        'services' => array(MOODLE_OFFICIAL_MOBILE_SERVICE),
    ),
);
