<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Announcements version file.
 *
 * @package    local_sb_announcements
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

define('LOCAL_ANNOUNCEMENTS_SYSTEMTYPE', 'system');
define('LOCAL_ANNOUNCEMENTS_COURSETYPE', 'course');
define('LOCAL_ANNOUNCEMENTS_COHORTTYPE', 'cohort');

function local_sb_announcements_cron()
{
    global $CFG, $DB;
    require_once('locallib.php');

    // We are going to measure execution times

    mtrace('Announcements CRON START...');

    $starttime = microtime();
    $count = local_sb_announcements_cleanup_announcements();
    mtrace('    ' . $count . ' announcements removed (took ' . microtime_diff($starttime, microtime()) . ' seconds)');

    $starttime = microtime();
    $count = local_sb_announcements_send_announcements();
    mtrace('    ' . $count . ' send announcements notifications (took ' . microtime_diff($starttime, microtime()) . ' seconds)');

    mtrace('Announcements CRON DONE.');

    // And return $status
    return true;
}

function local_sb_announcements_update($data)
{
    global $DB;

    $announcement = $DB->get_record('local_sb_announcements', array('id' => $data->id));

    if ($announcement->id) {
        $data->timemodified = time();

        if ($data->type == LOCAL_ANNOUNCEMENTS_COURSETYPE && count($data->courses) > 0) {
            $data->data = implode(',', $data->courses);
        } else if ($data->type == LOCAL_ANNOUNCEMENTS_SYSTEMTYPE && count($data->roles) > 0) {
            $data->data = implode(',', $data->roles);
        } else if ($data->type == LOCAL_ANNOUNCEMENTS_COHORTTYPE && count($data->cohorts) > 0) {
            $data->data = implode(',', $data->cohorts);
        }

        $DB->update_record('local_sb_announcements', $data);

        local_sb_announcements_user_update($data);
    }
}

function local_sb_announcements_save($data)
{
    global $DB, $USER;

    $data->userid = $USER->id;
    $data->timecreated = time();

    if ($data->type == LOCAL_ANNOUNCEMENTS_COURSETYPE && count($data->courses) > 0) {
        $data->data = implode(',', $data->courses);
    } else if ($data->type == LOCAL_ANNOUNCEMENTS_SYSTEMTYPE && count($data->roles) > 0) {
        $data->data = implode(',', $data->roles);
    } else if ($data->type == LOCAL_ANNOUNCEMENTS_COHORTTYPE && count($data->cohorts) > 0) {
        $data->data = implode(',', $data->cohorts);
    }

    $data->id = $DB->insert_record('local_sb_announcements', $data);

    local_sb_announcements_user_update($data);

    return $data->id;
}

function local_sb_announcements_user_update($data) {
    global $DB, $CFG;
    require_once('locallib.php');

    $announcement_user = new stdClass();
    $announcement_user->announcementid = $data->id;

    if ($data->type == LOCAL_ANNOUNCEMENTS_COURSETYPE && count($data->courses) > 0) {
        $announcement_user_data = [];

        $users = local_sb_announcements_get_course_users($data->courses);
        if (count($users) > 0) {
            foreach ($users as $user) {
                $announcement_user_data[$user->id][] = $user->id;
            }
        }

        foreach ($announcement_user_data as $userid => $value) {
            $announcement_user->userid = $userid;

            $result = $DB->get_record('local_sb_announcements_user', array('userid' => $announcement_user->userid, 'announcementid' => $announcement_user->announcementid));
            if (!$result) {
                $DB->insert_record('local_sb_announcements_user', $announcement_user);
            } else {
                $announcement_user->id = $result->id;
                $DB->update_record('local_sb_announcements_user', $announcement_user);
            }
        }

    } elseif ($data->type == LOCAL_ANNOUNCEMENTS_SYSTEMTYPE && count($data->roles) > 0) {
        $users = array();

        if (count($data->roles)) {
            list($sql_in, $params) = $DB->get_in_or_equal($data->roles, SQL_PARAMS_NAMED);
            $sql = "SELECT DISTINCT u.id
                            FROM  {role_assignments} ra
                        LEFT JOIN {user} u ON ra.userid = u.id
                            WHERE u.id > 1 AND u.deleted = 0 AND ra.roleid $sql_in";
            $users = $DB->get_records_sql($sql, $params);
        }

        if (count($users)) {
            foreach ($users as $user) {
                $announcement_user->userid = $user->id;
                $result = $DB->get_record('local_sb_announcements_user', array('userid' => $announcement_user->userid, 'announcementid' => $announcement_user->announcementid));
                if (!$result) {
                    $DB->insert_record('local_sb_announcements_user', $announcement_user);
                } else {
                    $announcement_user->id = $result->id;
                    $DB->update_record('local_sb_announcements_user', $announcement_user);
                }
            }
        }
    } elseif ($data->type == LOCAL_ANNOUNCEMENTS_COHORTTYPE && count($data->cohorts) > 0) {
        $users = array();

        if (count($data->cohorts)) {
            list($sql_in, $params) = $DB->get_in_or_equal($data->cohorts, SQL_PARAMS_NAMED);
            $sql = "SELECT DISTINCT u.id
                            FROM  {user} u
                        LEFT JOIN {cohort_members} cm ON cm.userid = u.id
                            WHERE u.id > 1 AND u.deleted = 0 AND cm.cohortid $sql_in";
            $users = $DB->get_records_sql($sql, $params);
        }

        if (count($users)) {
            foreach ($users as $user) {
                $announcement_user->userid = $user->id;
                $result = $DB->get_record('local_sb_announcements_user', array('userid' => $announcement_user->userid, 'announcementid' => $announcement_user->announcementid));
                if (!$result) {
                    $DB->insert_record('local_sb_announcements_user', $announcement_user);
                } else {
                    $announcement_user->id = $result->id;
                    $DB->update_record('local_sb_announcements_user', $announcement_user);
                }
            }
        }
    }
}


function local_sb_announcements_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options=array()) {
    global $CFG;
    require_once($CFG->dirroot . '/repository/lib.php');

    // Leave this line out if you set the itemid to null in make_pluginfile_url (set $itemid to 0 instead).
    $itemid = array_shift($args); // The first item in the $args array.

    // Use the itemid to retrieve any relevant data records and perform any security checks to see if the
    // user really does have access to the file in question.

    // Extract the filename / filepath from the $args array.
    $filename = array_pop($args); // The last item in the $args array.
    if (!$args) {
        $filepath = '/'; // $args is empty => the path is '/'
    } else {
        $filepath = '/'.implode('/', $args).'/'; // $args contains elements of the filepath
    }

    // Retrieve the file from the Files API.
    $fs = get_file_storage();
    $file = $fs->get_file($context->id, 'local_sb_announcements', $filearea, $itemid, $filepath, $filename);
    if (!$file) {
        return false; // The file does not exist.
    }

    send_stored_file($file, 86400, 0, $forcedownload, $options);
}


/**
 * Renders the popup.
 *
 * @param renderer_base $renderer
 * @return string The HTML
 */
function local_sb_announcements_render_navbar_output(\renderer_base $renderer) {
    global $USER, $CFG;

    // Early bail out conditions.
    if (!isloggedin() || isguestuser() || user_not_fully_set_up($USER) ||
        get_user_preferences('auth_forcepasswordchange') ||
        ($CFG->sitepolicy && !$USER->policyagreed && !is_siteadmin())) {
        return '';
    }

    $output = '';

    // Add the notifications popover.
    $enabled = get_config('local_sb_announcements', 'enabled');
    if ($enabled and has_capability('local/sb_announcements:view', context_system::instance())) {
        $context = [
            'userid' => $USER->id,
            'urls' => [
                'seeall' => (new moodle_url('/local/sb_announcements/index.php'))->out()
            ],
        ];

        if (has_capability('local/sb_announcements:manage', context_system::instance())) {
            $context['urls']['create'] = (new moodle_url('/local/sb_announcements/announcements.php'))->out();
        }

        $output .= $renderer->render_from_template('local_sb_announcements/announcement_popover', $context);
    }
    return $output;
}

