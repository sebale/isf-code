<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Announcements version file.
 *
 * @package    local_sb_announcements
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require_once('../../config.php');
require_once('classes/tables/announcements_table.php');
require_once('lib.php');
require_once('locallib.php');

global $PAGE, $OUTPUT;

$search = optional_param('search', '', PARAM_ALPHANUM);
$id = optional_param('id', 0, PARAM_INT);

$systemcontext = context_system::instance();
require_login();
require_capability('local/sb_announcements:manage', $systemcontext);
$title = get_string('announcementsmanage', 'local_sb_announcements');

$PAGE->set_url(new moodle_url("/local/sb_announcements/announcements.php", array()));
$PAGE->set_pagelayout('standard');
$PAGE->set_context(context_system::instance());
$PAGE->requires->jquery();
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

$table = new announcements_table('announcements_table', $search);
$table->is_collapsible = false;
$table->no_sorting('actions');
$table->column_class('actions', 'actions');

// load js
$PAGE->requires->js_call_amd('local_sb_announcements/announcements', 'processbgcolor');

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo html_writer::start_tag("form", array("action" => $PAGE->url, 'class' => 'announcements-search-form'));
echo html_writer::start_tag("label",  array('class'=>'form-inline'));
echo html_writer::empty_tag('input', array('type' => 'text', 'class'=>'form-control', 'name' => 'search', 'placeholder' => get_string('search', 'local_sb_announcements') . ' ...', 'value' => $search));
echo html_writer::end_tag("label");
echo html_writer::link(new moodle_url($CFG->wwwroot . '/local/sb_announcements/edit.php'), html_writer::tag('i', '', array('class' => 'fa fa-plus')) . get_string('create_new', 'local_sb_announcements'), array('class' => 'btn btn-warning'));
echo html_writer::end_tag("form");

$table->out(20, true);

echo $OUTPUT->footer();

