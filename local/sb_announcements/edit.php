<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Announcements version file.
 *
 * @package    local_sb_announcements
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */
require('../../config.php');
require_once('lib.php');
require_once('locallib.php');
require_once('classes/forms/edit_form.php');

global $PAGE, $OUTPUT, $DB, $USER;

require_login();
$systemcontext = context_system::instance();
require_capability('local/sb_announcements:edit', $systemcontext);

$id = optional_param('id', 0, PARAM_INT); // Announcement id.
$confirm = optional_param('confirm', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_RAW); // Announcement action.

if ($action == 'delete') {
    $announcement = $DB->get_record('local_sb_announcements', array('id' => $id));
    if ($confirm and confirm_sesskey() && $announcement->id) {
        $DB->delete_records('local_sb_announcements_user', array('announcementid' => $announcement->id));
        $DB->delete_records('local_sb_announcements', array('id' => $announcement->id));
        redirect(new moodle_url('/local/sb_announcements/announcements.php'), get_string('announcementdeleted', 'local_sb_announcements'));
    }

    $strheading = get_string('delete_announcement', 'local_sb_announcements');
    $PAGE->set_url('/local/sb_announcements/edit.php', array('id' => $id));
    $PAGE->set_context($systemcontext);
    $PAGE->navbar->add(get_string('announcementsmanage', 'local_sb_announcements'), new moodle_url("/local/sb_announcements/announcements.php"));
    $PAGE->navbar->add($strheading);
    $PAGE->set_title($strheading);
    $PAGE->set_heading($strheading);

    echo $OUTPUT->header();
    echo $OUTPUT->heading($strheading);
    $yesurl = new moodle_url('/local/sb_announcements/edit.php', array('id' => $announcement->id, 'action' => 'delete', 'confirm' => 1, 'sesskey' => sesskey()));
    $message = get_string('delete_announcement_text', 'local_sb_announcements', format_string($announcement->title));
    echo $OUTPUT->confirm($message, $yesurl, new moodle_url("/local/sb_announcements/announcements.php"));
    echo $OUTPUT->footer();
    die;

} elseif (($action == 'hide' || $action == 'show') and $id) {
    $announcement = $DB->get_record('local_sb_announcements', array('id' => $id));
    if ($announcement) {
        $announcement->state = ($action == 'show') ? 1 : 0;
        $DB->update_record('local_sb_announcements', $announcement);
    }

    redirect(new moodle_url("/local/sb_announcements/announcements.php"), get_string('announcementupdated', 'local_sb_announcements'));
}

// Prepare course and the editor.
$editoroptions = array('maxfiles' => 1, 'maxbytes' => $CFG->maxbytes, 'trusttext' => false, 'noclean' => true, 'context' => $systemcontext, 'subdirs' => 0);

if ($id > 0) {
    $announcement = $DB->get_record('local_sb_announcements', array('id' => $id));
    $announcement->bodyformat = 1;
} else {
    $announcement = new stdClass();
    $announcement->id = null;
}

$announcement = file_prepare_standard_editor($announcement, 'body', $editoroptions, $systemcontext, 'local_sb_announcements', 'bodyfile', $announcement->id);

$args = array(
    'id' => $id,
    'announcement' => $announcement,
    'editoroptions' => $editoroptions
);

$title = ($id > 0) ? get_string('announcementsedit', 'local_sb_announcements') : get_string('announcementscreate', 'local_sb_announcements');

$PAGE->set_context($systemcontext);
$PAGE->set_pagelayout('admin');
$pageparams = array('id' => $id);
$PAGE->set_url('/local/sb_announcements/edit.php', $pageparams);
$PAGE->navbar->add(get_string('announcementsmanage', 'local_sb_announcements'), new moodle_url("/local/sb_announcements/announcements.php"));
$PAGE->navbar->add($title);

// load js
$PAGE->requires->js_call_amd('local_sb_announcements/announcements', 'editannouncement', array('id'=>(isset($announcement->id)) ? $announcement->id : 0));

$editform = new edit_form(null, $args);

if ($editform->is_cancelled()) {
    // The form has been cancelled, take them back to what ever the return to is.
    redirect(new moodle_url('/local/sb_announcements/announcements.php'));
} else if ($data = $editform->get_data()) {
    // save and relink embedded images and save attachments
    $data = file_postupdate_standard_editor($data, 'body', $editoroptions, $systemcontext, 'local_sb_announcements', 'bodyfile', $announcement->id);

    if ($data->id > 0) {
        local_sb_announcements_update($data);
        $msg = get_string('announcementupdated', 'local_sb_announcements');
    } else {
        $data->id = local_sb_announcements_save($data);
        $msg = get_string('announcementcreated', 'local_sb_announcements');
    }

    if ($data->sendnotification) {
        local_sb_announcements_send_mail($data);
    }

    redirect(new moodle_url('/local/sb_announcements/announcements.php'), $msg);
}

$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$editform->display();

echo $OUTPUT->footer(); ?>
