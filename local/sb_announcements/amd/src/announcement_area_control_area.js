// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Controls the announcement area on the announcement page.
 *
 * @module     local_sb_announcements/announcement_area_control_area
 * @class      announcement_area_control_area
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
define(['jquery', 'core/templates', 'core/notification', 'core/custom_interaction_events',
        'local_sb_announcements/announcement_repository', 'local_sb_announcements/announcement_area_events'],
    function($, Templates, DebugNotification, CustomEvents, AnnouncementRepo, AnnouncementAreaEvents) {

    var SELECTORS = {
        CONTAINER: '[data-region="announcements-area"]',
        CONTENT: '[data-region="content"]',
        ANNOUNCEMENT: '[data-region="announcement-content-item-container"]',
        CAN_RECEIVE_FOCUS: 'input:not([type="hidden"]), a[href], button, textarea, select, [tabindex]',
    };

    var TEMPLATES = {
        ANNOUNCEMENT: 'local_sb_announcements/announcement_content_item',
    };

    /**
     * Constructor for ControlArea
     *
     * @param {object} root The root element for the content area
     * @param {int} userId The user id of the current user
     */
    var ControlArea = function(root, userId) {
        this.root = $(root);
        this.container = this.root.closest(SELECTORS.CONTAINER);
        this.userId = userId;
        this.content = this.root.find(SELECTORS.CONTENT);
        this.offset = 0;
        this.limit = 20;
        this.initialLoad = false;
        this.isLoading = false;
        this.loadedAll = false;
        this.announcements = {};

        this.registerEventListeners();
    };

    /**
     * Get the root element.
     *
     * @method getRoot
     * @return {object} jQuery element
     */
    ControlArea.prototype.getRoot = function() {
        return this.root;
    };

    /**
     * Get the container element (which the control area is within).
     *
     * @method getContainer
     * @return {object} jQuery element
     */
    ControlArea.prototype.getContainer = function() {
        return this.container;
    };

    /**
     * Get the user id.
     *
     * @method getUserId
     * @return {int}
     */
    ControlArea.prototype.getUserId = function() {
        return this.userId;
    };

    /**
     * Get the control area content element.
     *
     * @method getContent
     * @return {object} jQuery element
     */
    ControlArea.prototype.getContent = function() {
        return this.content;
    };

    /**
     * Get the offset value for paginated loading of the
     * announcements.
     *
     * @method getOffset
     * @return {int}
     */
    ControlArea.prototype.getOffset = function() {
        return this.offset;
    };

    /**
     * Get the limit value for the paginated loading of the
     * announcements.
     *
     * @method getLimit
     * @return {int}
     */
    ControlArea.prototype.getLimit = function() {
        return this.limit;
    };

    /**
     * Set the offset value for the paginated loading of the
     * announcements.
     *
     * @method setOffset
     * @param {int} value The new offset value
     */
    ControlArea.prototype.setOffset = function(value) {
        this.offset = value;
    };

    /**
     * Set the limit value for the paginated loading of the
     * announcements.
     *
     * @method setLimit
     * @param {int} value The new limit value
     */
    ControlArea.prototype.setLimit = function(value) {
        this.limit = value;
    };

    /**
     * Increment the offset by the limit amount.
     *
     * @method incrementOffset
     */
    ControlArea.prototype.incrementOffset = function() {
        this.offset += this.limit;
    };

    /**
     * Flag the control area as loading.
     *
     * @method startLoading
     */
    ControlArea.prototype.startLoading = function() {
        this.isLoading = true;
        this.getRoot().addClass('loading');
    };

    /**
     * Remove the loading flag from the control area.
     *
     * @method stopLoading
     */
    ControlArea.prototype.stopLoading = function() {
        this.isLoading = false;
        this.getRoot().removeClass('loading');
    };

    /**
     * Check if the first load of announcements has been triggered.
     *
     * @method hasDoneInitialLoad
     * @return {bool} true if first announcement loaded, false otherwise
     */
    ControlArea.prototype.hasDoneInitialLoad = function() {
        return this.initialLoad;
    };

    /**
     * Check if all of the announcements have been loaded.
     *
     * @method hasLoadedAllContent
     * @return {bool}
     */
    ControlArea.prototype.hasLoadedAllContent = function() {
        return this.loadedAll;
    };

    /**
     * Set the state of the loaded all content property.
     *
     * @method setLoadedAllContent
     * @param {bool} val True if all content is loaded, false otherwise
     */
    ControlArea.prototype.setLoadedAllContent = function(val) {
        this.loadedAll = val;
    };

    /**
     * Save a announcement in the cache.
     *
     * @method setCacheAnnouncement
     * @param {object} announcement A announcement returned by a webservice
     */
    ControlArea.prototype.setCacheAnnouncement = function(announcement) {
        this.announcements[announcement.id] = announcement;
    };

    /**
     * Retrieve a announcement from the cache.
     *
     * @method getCacheAnnouncement
     * @param {int} id The id for the announcement you wish to retrieve
     * @return {object} A announcement (as returned by a webservice)
     */
    ControlArea.prototype.getCacheAnnouncement = function(id) {
        return this.announcements[id];
    };

    /**
     * Find the announcement element in the control area for the given id.
     *
     * @method getAnnouncementElement
     * @param {int} id The announcement id
     * @return {(object|null)} jQuery element or null
     */
    ControlArea.prototype.getAnnouncementElement = function(id) {
        var element = this.getRoot().find(SELECTORS.ANNOUNCEMENT + '[data-id="' + id + '"]');
        return element.length == 1 ? element : null;
    };

    /**
     * Scroll the announcement element into view within the control area, if it
     * isn't already visible.
     *
     * @method scrollAnnouncementIntoView
     * @param {object} announcementElement The jQuery announcement element
     */
    ControlArea.prototype.scrollAnnouncementIntoView = function(announcementElement) {
        var position = announcementElement.position();
        var container = this.getRoot();
        var relativeTop = position.top - container.scrollTop();

        // If the element isn't in the view window.
        if (relativeTop > container.innerHeight()) {
            var height = announcementElement.outerHeight();
            // offset enough to make sure the announcement will be in view.
            height = height * 4;
            var scrollTo = position.top - height;
            container.scrollTop(scrollTo);
        }
    };

    /**
     * Show the full announcement for the given announcement element. The announcement
     * context is retrieved from the cache and send as data with an event to be
     * rendered in the content area.
     *
     * @method showAnnouncement
     * @param {(int|object)} announcementElement The announcement id or jQuery announcement element
     */
    ControlArea.prototype.showAnnouncement = function(announcementElement) {
        if (typeof announcementElement !== 'object') {
            // Assume it's an ID if it's not an object.
            announcementElement = this.getAnnouncementElement(announcementElement);
        }

        if (announcementElement && announcementElement.length) {
            this.getRoot().find(SELECTORS.ANNOUNCEMENT).removeClass('selected');
            announcementElement.addClass('selected').find(SELECTORS.CAN_RECEIVE_FOCUS).focus();
            var announcementId = announcementElement.attr('data-id');
            var announcement = this.getCacheAnnouncement(announcementId);
            this.scrollAnnouncementIntoView(announcementElement);
            // Create a new version of the announcement to send with the announcement so
            // this copy isn't modified.
            this.getContainer().trigger(AnnouncementAreaEvents.showAnnouncement, [$.extend({}, announcement)]);
        }
    };

    /**
     * Send a request to mark the announcement as read in the server and remove the unread
     * status from the element.
     *
     * @method markAnnouncementAsRead
     * @param {object} announcementElement The jQuery announcement element
     * @return {object} jQuery promise
     */
    ControlArea.prototype.markAnnouncementAsRead = function(announcementElement) {
        return AnnouncementRepo.markAsRead(announcementElement.attr('data-id')).done(function() {
            announcementElement.removeClass('unread');
        });
    };

    /**
     * Render the announcement data with the appropriate template and add it to the DOM.
     *
     * @method renderAnnouncements
     * @param {array} announcements Array of announcement data
     * @return {object} jQuery promise that is resolved when all announcements have been
     *                  rendered and added to the DOM
     */
    ControlArea.prototype.renderAnnouncements = function(announcements) {
        var promises = [];
        var allhtml = [];
        var alljs = [];
        var container = this.getContent();

        if (announcements.length) {
            $.each(announcements, function(index, announcement) {
                // Need to remove the contexturl so the item isn't rendered
                // as a link.
                var contextUrl = announcement.contexturl;
                delete announcement.contexturl;

                var promise = Templates.render(TEMPLATES.ANNOUNCEMENT, announcement);

                promises.push(promise);
                promise.then(function(html, js) {
                    allhtml[index] = html;
                    alljs[index] = js;
                    // Restore it for the cache.
                    announcement.contexturl = contextUrl;
                    this.setCacheAnnouncement(announcement);
                }.bind(this))
                .fail(DebugNotification.exception);
            }.bind(this));
        }

        return $.when.apply($.when, promises).then(function() {
            if (announcements.length) {
                $.each(announcements, function(index) {
                    container.append(allhtml[index]);
                    Templates.runTemplateJS(alljs[index]);
                });
            }
        });
    };

    /**
     * Load announcements from the server and render them.
     *
     * @method loadMoreAnnouncements
     * @return {object} jQuery promise
     */
    ControlArea.prototype.loadMoreAnnouncements = function() {
        if (this.isLoading || this.hasLoadedAllContent()) {
            return $.Deferred().resolve();
        }

        this.startLoading();
        var request = {
            limit: this.getLimit(),
            offset: this.getOffset(),
            useridto: this.getUserId(),
        };

        if (!this.initialLoad) {
            // If this is the first load we may have been given a non-zero offset,
            // in which case we need to load all announcements preceeding that offset
            // to make sure the full list is rendered.
            request.limit = this.getOffset() + this.getLimit();
            request.offset = 0;
        }

        var promise = AnnouncementRepo.query(request).then(function(result) {
            var announcements = result.announcements;
            this.unreadCount = result.unreadcount;
            this.setLoadedAllContent(!announcements.length || announcements.length < this.getLimit());
            this.initialLoad = true;

            if (announcements.length) {
                this.incrementOffset();
                return this.renderAnnouncements(announcements);
            }

            return false;
        }.bind(this))
        .always(function() {
            this.stopLoading();
        }.bind(this));

        return promise;
    };

    /**
     * Create the event listeners for the control area.
     *
     * @method registerEventListeners
     */
    ControlArea.prototype.registerEventListeners = function() {
        CustomEvents.define(this.getRoot(), [
            CustomEvents.events.activate,
            CustomEvents.events.scrollBottom,
            CustomEvents.events.scrollLock,
            CustomEvents.events.up,
            CustomEvents.events.down,
        ]);

        this.getRoot().on(CustomEvents.events.scrollBottom, function() {
            this.loadMoreAnnouncements();
        }.bind(this));

        this.getRoot().on(CustomEvents.events.activate, SELECTORS.ANNOUNCEMENT, function(e) {
            var announcementElement = $(e.target).closest(SELECTORS.ANNOUNCEMENT);
            this.showAnnouncement(announcementElement);
        }.bind(this));

        // Show the previous announcement in the list.
        this.getRoot().on(CustomEvents.events.up, SELECTORS.ANNOUNCEMENT, function(e, data) {
            var announcementElement = $(e.target).closest(SELECTORS.ANNOUNCEMENT);
            this.showAnnouncement(announcementElement.prev());

            data.originalEvent.preventDefault();
        }.bind(this));

        // Show the next announcement in the list.
        this.getRoot().on(CustomEvents.events.down, SELECTORS.ANNOUNCEMENT, function(e, data) {
            var announcementElement = $(e.target).closest(SELECTORS.ANNOUNCEMENT);
            this.showAnnouncement(announcementElement.next());

            data.originalEvent.preventDefault();
        }.bind(this));

        this.getContainer().on(AnnouncementAreaEvents.announcementShown, function(e, announcement) {
            if (!announcement.read) {
                var element = this.getAnnouncementElement(announcement.id);

                if (element) {
                    this.markAnnouncementAsRead(element);
                }

                var cachedAnnouncement = this.getCacheAnnouncement(announcement.id);

                if (cachedAnnouncement) {
                    cachedAnnouncement.read = true;
                }
            }
        }.bind(this));

        // this.getContainer().on(AnnouncementAreaEvents.showAnnouncement, function(e, announcement) {
        //     console.log('show trigger');
        //     console.log(e);
        //     console.log(announcement);
        // }.bind(this));
    };

    return ControlArea;
});
