// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is an empty module, that is required before all other modules.
 * Because every module is returned from a request for any other module, this
 * forces the loading of all modules with a single request.
 *
 * This function also sets up the listeners for ajax requests so we can tell
 * if any requests are still in progress.
 *
 * @package    local_sb_announcements
 * @module     local_sb_announcements/announcements
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define(['jquery', 'core/ajax', 'core/log'], function($, ajax, log) {

    var Announcements = {

        init: function() {


        },

        // announcement edit
        editannouncement: function(id) {
            var type = $('#id_type').val();
            Announcements.editannouncement_process_data(type, id);

            $('#id_type').change(function(){
                var type = $('#id_type').val();
                Announcements.editannouncement_process_data(type, id);
            });
        },

        editannouncement_process_data: function(type, id){
            $('#id_courses').parents('.fitem').hide();
            $('#id_cohorts').parents('.fitem').hide();
            $('#id_roles').parents('.fitem').hide();

            if (id == 0){
                if (type == 'course'){
                    $('#id_courses').parents('.fitem').show();
                } else if (type == 'cohort') {
                    $('#id_cohorts').parents('.fitem').show();
                } else if (type == 'system') {
                    $('#id_roles').parents('.fitem').show();
                }
            }
        },

        processbgcolor: function(){
            $('.raw-color').each(function (key, value) {
                $(this).closest('tr').addClass('bg-light-' + $(value).text());
            })
        }
    };

    /**
    * @alias module:local_sb_announcements/announcements
    */
    return Announcements;

});

