// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Retrieves announcements from the server.
 *
 * @module     local_sb_announcements/announcement_repository
 * @class      announcement_repository
 * @package    local_sb_announcements
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
define(['core/ajax', 'core/notification'], function(Ajax, Notification) {
    /**
     * Retrieve a list of announcements from the server.
     *
     * @param {object} args The request arguments
     * @return {object} jQuery promise
     */
    var query = function(args) {
        if (typeof args.limit === 'undefined') {
            args.limit = 20;
        }

        if (typeof args.offset === 'undefined') {
            args.offset = 0;
        }

        var request = {
            methodname: 'local_sb_announcements_get_popup_announcements',
            args: args
        };

        var promise = Ajax.call([request])[0];

        promise.fail(Notification.exception);

        return promise;
    };

    /**
     * Get the number of unread announcements from the server.
     *
     * @param {object} args The request arguments
     * @return {object} jQuery promise
     */
    var countUnread = function(args) {
        var request = {
            methodname: 'local_sb_announcements_get_unread_popup_announcement_count',
            args: args
        };

        var promise = Ajax.call([request])[0];

        promise.fail(Notification.exception);

        return promise;
    };

    /**
     * Mark all announcements for the given user as read.
     *
     * @param {object} args The request arguments:
     * @return {object} jQuery promise
     */
    var markAllAsRead = function(args) {
        var request = {
            methodname: 'local_sb_announcements_mark_all_read',
            args: args
        };

        var promise = Ajax.call([request])[0];

        promise.fail(Notification.exception);

        return promise;
    };

    /**
     * Mark a specific announcement as read.
     *
     * @param {int} id The announcement id
     * @param {int} timeread The read timestamp (optional)
     * @return {object} jQuery promise
     */
    var markAsRead = function(id, timeread) {
        var args = {
            messageid: id,
        };

        if (timeread) {
            args.timeread = timeread;
        }

        var request = {
            methodname: 'local_sb_announcements_mark_announcement_read',
            args: args
        };

        var promise = Ajax.call([request])[0];

        promise.fail(Notification.exception);

        return promise;
    };


    /**
     * Delete announcement.
     *
     * @param {int} id The announcement id
     * @return {object} jQuery promise
     */
    var deleteItem = function(id) {
        var args = {
            announcementid: id
        };

        var request = {
            methodname: 'local_sb_announcements_delete_announcement',
            args: args
        };

        var promise = Ajax.call([request])[0];

        promise.fail(Notification.exception);

        return promise;
    };

    return {
        query: query,
        countUnread: countUnread,
        markAllAsRead: markAllAsRead,
        markAsRead: markAsRead,
        deleteItem: deleteItem
    };
});
