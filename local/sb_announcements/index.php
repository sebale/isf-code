<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Announcements version file.
 *
 * @package    local_sb_announcements
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require_once('../../config.php');
require_once('lib.php');
require_once('locallib.php');

$systemcontext = context_system::instance();
require_login();
require_capability('local/sb_announcements:view', $systemcontext);

$announcementid = optional_param('announcementid', 0, PARAM_INT);
$offset = optional_param('offset', 0, PARAM_INT);
$limit = optional_param('limit', 0, PARAM_INT);
$userid = $USER->id;

$url = new moodle_url('/local/sb_announcements/index.php', array('id'=>$announcementid));
$PAGE->set_url($url);

$personalcontext = context_user::instance($USER->id);
$PAGE->set_context($personalcontext);
$PAGE->set_pagelayout('admin');

$title = get_string('announcements', 'local_sb_announcements');
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

// Grab the renderer.
$renderer = $PAGE->get_renderer('core', 'message');
$context = [
    'announcementid' => $announcementid,
    'userid' => $userid,
    'limit' => $limit,
    'offset' => $offset,
];

echo $OUTPUT->header();
echo $OUTPUT->heading($title);
echo $renderer->render_from_template('local_sb_announcements/announcement_area', $context);
echo $OUTPUT->footer();
