<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * External message popup API
 *
 * @module     local_sb_announcements/announcement_popover_controller
 * @package    local_sb_announcements
 * @category   external
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once("$CFG->libdir/externallib.php");
require_once($CFG->dirroot . "/local/sb_announcements/lib.php");

/**
 * Message external functions
 *
 * @module     local_sb_announcements/announcement_popover_controller
 * @package    local_sb_announcements
 * @category   external
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class local_sb_announcements_external extends external_api {

    /**
     * Get popup announcements parameters description.
     *
     * @return external_function_parameters
     * @since 3.2
     */
    public static function get_popup_announcements_parameters() {
        return new external_function_parameters(
            array(
                'useridto' => new external_value(PARAM_INT, 'the user id who received the message, 0 for current user'),
                'newestfirst' => new external_value(
                    PARAM_BOOL, 'true for ordering by newest first, false for oldest first',
                    VALUE_DEFAULT, true),
                'limit' => new external_value(PARAM_INT, 'the number of results to return', VALUE_DEFAULT, 0),
                'offset' => new external_value(PARAM_INT, 'offset the result set by a given amount', VALUE_DEFAULT, 0)
            )
        );
    }

    /**
     * Get announcements function.
     *
     * @since  3.2
     * @throws invalid_parameter_exception
     * @throws moodle_exception
     * @param  int      $useridto           the user id who received the message
     * @param  bool     $newestfirst        true for ordering by newest first, false for oldest first
     * @param  int      $limit              the number of results to return
     * @param  int      $offset             offset the result set by a given amount
     * @return external_description
     */
    public static function get_popup_announcements($useridto, $newestfirst, $limit, $offset) {
        global $USER, $PAGE;

        $params = self::validate_parameters(
            self::get_popup_announcements_parameters(),
            array(
                'useridto' => $useridto,
                'newestfirst' => $newestfirst,
                'limit' => $limit,
                'offset' => $offset,
            )
        );

        $context = context_system::instance();
        self::validate_context($context);

        $useridto = $params['useridto'];
        $newestfirst = $params['newestfirst'];
        $limit = $params['limit'];
        $offset = $params['offset'];
        $issuperuser = has_capability('moodle/site:readallmessages', $context);
        $renderer = $PAGE->get_renderer('core_message');

        if (empty($useridto)) {
            $useridto = $USER->id;
        }

        // Check if the current user is the sender/receiver or just a privileged user.
        if ($useridto != $USER->id and !$issuperuser) {
            throw new moodle_exception('accessdenied', 'admin');
        }

        if (!empty($useridto)) {
            if (!core_user::is_real_user($useridto)) {
                throw new moodle_exception('invaliduser');
            }
        }

        $sort = $newestfirst ? 'DESC' : 'ASC';
        $announcements = \local_sb_announcements\api::get_popup_announcements($useridto, $sort, $limit, $offset);
        $announcementscontexts = [];

        if ($announcements) {
            foreach ($announcements as $announcement) {

                $announcementoutput = new \local_sb_announcements\output\popup_announcements($announcement);

                $announcementcontext = $announcementoutput->export_for_template($renderer);
                $announcementscontexts[] = $announcementcontext;
            }
        }

        return array(
            'announcements' => $announcementscontexts,
            'unreadcount' => \local_sb_announcements\api::count_unread_popup_announcements($useridto)
        );
    }

    /**
     * Get announcements return description.
     *
     * @return external_single_structure
     * @since 3.2
     */
    public static function get_popup_announcements_returns() {
        return new external_single_structure(
            array(
                'announcements' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'id' => new external_value(PARAM_INT, 'Announcement id (this is not guaranteed to be unique within this result set)'),
                            'useridfrom' => new external_value(PARAM_INT, 'User from id'),
                            'useridto' => new external_value(PARAM_INT, 'User to id'),
                            'title' => new external_value(PARAM_TEXT, 'The announcement title'),
                            'shortenedtitle' => new external_value(PARAM_TEXT, 'The announcement title shortened
                                with ellipsis'),
                            'text' => new external_value(PARAM_RAW, 'The announcement body formated'),
                            'body' => new external_value(PARAM_RAW, 'The announcement body'),
                            'timecreated' => new external_value(PARAM_INT, 'Time created'),
                            'startdatepretty' => new external_value(PARAM_TEXT, 'Time started in a pretty format'),
                            'read' => new external_value(PARAM_BOOL, 'announcement read status'),
                            'deleted' => new external_value(PARAM_BOOL, 'announcement deletion status'),
                            'class' => new external_value(PARAM_RAW, 'announcement class color'),
                            'iconurl' => new external_value(PARAM_URL, 'URL for announcement icon'),
                        ), 'message'
                    )
                ),
                'unreadcount' => new external_value(PARAM_INT, 'the number of unread message for the given user'),
            )
        );
    }

    /**
     * Get unread announcement count parameters description.
     *
     * @return external_function_parameters
     * @since 3.2
     */
    public static function get_unread_popup_announcement_count_parameters() {
        return new external_function_parameters(
            array(
                'useridto' => new external_value(PARAM_INT, 'the user id who received the message, 0 for any user', VALUE_REQUIRED),
            )
        );
    }

    /**
     * Get unread announcement count function.
     *
     * @since  3.2
     * @throws invalid_parameter_exception
     * @throws moodle_exception
     * @param  int      $useridto       the user id who received the message
     * @return external_description
     */
    public static function get_unread_popup_announcement_count($useridto) {
        global $USER;

        $params = self::validate_parameters(
            self::get_unread_popup_announcement_count_parameters(),
            array('useridto' => $useridto)
        );

        $context = context_system::instance();
        self::validate_context($context);

        $useridto = $params['useridto'];

        if (!empty($useridto)) {
            if (core_user::is_real_user($useridto)) {
                $userto = core_user::get_user($useridto, '*', MUST_EXIST);
            } else {
                throw new moodle_exception('invaliduser');
            }
        }

        // Check if the current user is the sender/receiver or just a privileged user.
        if ($useridto != $USER->id and !has_capability('moodle/site:readallmessages', $context)) {
            throw new moodle_exception('accessdenied', 'admin');
        }

        return \local_sb_announcements\api::count_unread_popup_announcements($useridto);
    }

    /**
     * Get unread popup announcement count return description.
     *
     * @return external_single_structure
     * @since 3.2
     */
    public static function get_unread_popup_announcement_count_returns() {
        return new external_value(PARAM_INT, 'The count of unread popup announcements');
    }

    /**
     * Mark read announcement description.
     *
     * @return external_function_parameters
     * @since 3.2
     */
    public static function mark_announcement_read_parameters() {
        return new external_function_parameters(
            array(
                'messageid' => new external_value(PARAM_INT, 'the message id', VALUE_REQUIRED),
            )
        );
    }

    /**
     * Mark read announcement function.
     *
     * @since  3.2
     * @throws invalid_parameter_exception
     * @throws moodle_exception
     * @param  int      $messageid       the message id
     * @return external_description
     */
    public static function mark_announcement_read($messageid) {
        global $USER;

        $params = self::validate_parameters(
            self::mark_announcement_read_parameters(),
            array('messageid' => $messageid)
        );
        $status = \local_sb_announcements\api::set_read_announcements($messageid);
        return array(
          'status' => $status
        );
    }

    /**
     * Get unread popup announcement count return description.
     *
     * @return external_single_structure
     * @since 3.2
     */
    public static function mark_announcement_read_returns() {
      return new external_function_parameters(
          array(
              'status' => new external_value(PARAM_BOOL, 'action status', VALUE_REQUIRED),
          )
      );
    }

    /**
     * Mark read all announcements description.
     *
     * @return external_function_parameters
     * @since 3.2
     */
    public static function mark_all_announcements_read_parameters() {
        return new external_function_parameters(
            array(
                'useridto' => new external_value(PARAM_INT, 'user id', VALUE_OPTIONAL),
            )
        );
    }

    /**
     * Mark read all announcements function.
     *
     * @since  3.2
     * @throws invalid_parameter_exception
     * @throws moodle_exception
     * @param  int      $messageid       the message id
     * @return external_description
     */
    public static function mark_all_announcements_read($useridto) {
        global $USER;

        $params = self::validate_parameters(
            self::mark_all_announcements_read_parameters(),
            array('useridto' => $useridto)
        );
        $status = \local_sb_announcements\api::set_read_all_announcements();

        return array(
          'status' => $status
        );
    }

    /**
     * Get unread popup announcement count return description.
     *
     * @return external_single_structure
     * @since 3.2
     */
    public static function mark_all_announcements_read_returns() {
      return new external_function_parameters(
          array(
              'status' => new external_value(PARAM_BOOL, 'action status', VALUE_REQUIRED),
          )
      );
    }


    /**
     * Delete announcement description.
     *
     * @return external_function_parameters
     * @since 3.2
     */
    public static function delete_announcement_parameters() {
        return new external_function_parameters(
            array(
                'announcementid' => new external_value(PARAM_INT, 'the announcement id', VALUE_REQUIRED),
            )
        );
    }

    /**
     * delete announcement function.
     *
     * @since  3.2
     * @throws invalid_parameter_exception
     * @throws moodle_exception
     * @param  int      $announcementid       the announcement id
     * @return external_description
     */
    public static function delete_announcement($announcementid) {
        $params = self::validate_parameters(
            self::delete_announcement_parameters(),
            array('announcementid' => $announcementid)
        );
        $status = \local_sb_announcements\api::delete_announcement($announcementid);

        return array(
          'status' => $status
        );
    }

    /**
     * delete announcement return description.
     *
     * @return external_single_structure
     * @since 3.2
     */
    public static function delete_announcement_returns() {
      return new external_function_parameters(
          array(
              'status' => new external_value(PARAM_BOOL, 'action status', VALUE_REQUIRED),
          )
      );
    }
}
