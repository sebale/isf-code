<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Kaltura version file.
 *
 * @package    local_kaltura_announcements
 * @author     KALTURA
 * @copyright  2016 KALTURA, kaltura.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
$ajaxRequest = true;
require_once(dirname(__FILE__).'/lib.php');

$action = optional_param('action', '', PARAM_RAW);
$id     = optional_param('id', 0, PARAM_INT);

if ($action == 'close' and $id > 0){
    
    $ann = new stdClass();
    $ann->instanceid = $id;
    $ann->userid = $USER->id;
    $ann->feed = 0;
    $ann->timecreated = time();
    
    $ann->id = $DB->insert_record('local_kaltura_ann_view', $ann);
    echo $ann->id;
    
} elseif ($action == 'feed' and $id > 0){
    
    $ann = new stdClass();
    $ann->instanceid = $id;
    $ann->userid = $USER->id;
    $ann->feed = 1;
    $ann->timecreated = time();
    
    $ann->id = $DB->insert_record('local_kaltura_ann_view', $ann);
    
    echo $ann->id;
}

