<?php

defined('MOODLE_INTERNAL') || die;

$plugin->version   = 2017041903;
$plugin->release   = '1.0.0';
$plugin->maturity  = MATURITY_STABLE;
$plugin->requires  = 2014051200;
$plugin->component = 'local_agenda';
