$(document).ready(function() {
    $('.agenda-buttons ul li[class*="tab-"]').on('click', function (e) {
        $('.agenda-buttons ul li[class*="tab-"]').removeClass('active');

        $('.mform fieldset').attr('class', 'clearfix collapsible collapsed');

        $('.mform fieldset[id="id_' + $(this).attr('class') + '"]').toggleClass('collapsed');
        $(this).toggleClass('active');
        $('input[name="type"]').val($(this).attr('data-type'));

        $('.mform fieldset[id="id_tab-settings"]').toggleClass('collapsed');
        $('.mform fieldset[id="id_tab-settings"]').toggleClass('hidden');
    });

    $('.mform fieldset[id="id_tab-settings"]').toggleClass('collapsed');
    $('.mform fieldset[id="id_tab-settings"]').toggleClass('hidden');

    if($('#id_allow_post_with_delay:checked').length == 0) {
        $('div[data-fieldtype="date_time_selector"]').parent().css('display', 'none');
    }

    $('#id_allow_post_with_delay').on('change', function () {
        if($('#id_allow_post_with_delay:checked').length == 0) {
            $('div[data-fieldtype="date_time_selector"]').parent().css('display', 'none');
        } else {
            $('div[data-fieldtype="date_time_selector"]').parent().css('display', 'block');
        }
    });

    $('#agenda_add_more_options').on('click', function (e) {
        e.preventDefault();
        var id = $('input[name*="option_"]').length + 1;
        $('fieldset[id="id_tab-poll"] #agenda_add_more_options').before('<div class="form-group row fitem"><div class="col-md-3"></div><div class="col-md-9 form-inline felement" data-fieldtype="text"><input type="text" class="form-control " name="option_' + id + '" value="" size="" placeholder="Option"></div></div>');
    });

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var calendar = $("#course_calendar").fullCalendar({
        header: {
            left: "title",
            center: "",
            right: "prev,next"
        },
        dayClick: function(date, jsEvent, view) {
            /*jQuery('.fc-day').removeClass('active');
            jQuery(this).addClass('active');
            var day = jQuery(this).attr('data-date');
            jQuery(".course-calendar-events").html("<div class='loading'><i class='fa fa-spin fa-spinner'></div>").load("<?php echo $CFG->wwwroot; ?>/course/format/converge/ajax.php?action=get_day_events&id=<?php echo $course->id; ?>&date="+day);*/
        },
        editable: false,
        disableDragging: true,
        contentHeight: 50,
        handleWindowResize: true,
        events: null
    });
});