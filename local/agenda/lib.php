<?php

class LocalAgenda {

    static $AGENDA_TYPE_POST = 0;
    static $AGENDA_TYPE_QUESTION = 1;
    static $AGENDA_TYPE_LINK = 2;
    static $AGENDA_TYPE_FILES = 3;
    static $AGENDA_TYPE_POLL = 4;

    public static function getTypeList() {
        return array(
            self::$AGENDA_TYPE_POST => get_string('post', 'local_agenda'),
            self::$AGENDA_TYPE_QUESTION => get_string('question', 'local_agenda'),
            self::$AGENDA_TYPE_LINK => get_string('link', 'local_agenda'),
            self::$AGENDA_TYPE_FILES => get_string('files', 'local_agenda'),
            self::$AGENDA_TYPE_POLL => get_string('poll', 'local_agenda')
        );
    }
}


function local_agenda_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options=array()) {
    global $CFG;
    require_once($CFG->dirroot . '/repository/lib.php');

    // Leave this line out if you set the itemid to null in make_pluginfile_url (set $itemid to 0 instead).
    $itemid = array_shift($args); // The first item in the $args array.

    // Use the itemid to retrieve any relevant data records and perform any security checks to see if the
    // user really does have access to the file in question.

    // Extract the filename / filepath from the $args array.
    $filename = array_pop($args); // The last item in the $args array.
    if (!$args) {
        $filepath = '/'; // $args is empty => the path is '/'
    } else {
        $filepath = '/'.implode('/', $args).'/'; // $args contains elements of the filepath
    }

    // Retrieve the file from the Files API.
    $fs = get_file_storage();
    $file = $fs->get_file($context->id, 'local_agenda', $filearea, $itemid, $filepath, $filename);
    if (!$file) {
        return false; // The file does not exist.
    }

    send_stored_file($file, 86400, 0, $forcedownload, $options);
}
