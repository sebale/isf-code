<?php

define('AJAX_SCRIPT', true);

require_once('../../config.php');

$id         = optional_param('id', 0, PARAM_INT);
$courseid   = optional_param('courseid', 0, PARAM_INT);
$userid     = optional_param('userid', 0, PARAM_INT);
$state      = optional_param('state', 0, PARAM_INT);
$type       = optional_param('type', '', PARAM_RAW);
$action     = optional_param('action', '', PARAM_RAW);
$grade      = optional_param('grade', '', PARAM_RAW);
$form       = (object)optional_param_array('form', array(), PARAM_RAW);
$courseid   = optional_param('courseid', 0, PARAM_INT);

if (empty($id) and empty($courseid)){
    die('ERROR');
}

if ($id > 0){
    $agenda = $DB->get_record('local_agenda_posts', array('id'=>$id));
}

if ($courseid > 0){
    $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
} elseif (isset($agenda->courseid)) {
    $course = $DB->get_record('course', array('id' => $agenda->courseid), '*', MUST_EXIST);
} else {
    die('Wrong param');
}

$context = context_course::instance($course->id, MUST_EXIST);
$PAGE->set_url(new moodle_url('/local/agenda/ajax.php', array('id' => $id, 'courseid'=> $courseid)));
$PAGE->set_context($context);

if ($action == 'agenda_hide') {

    $agenda->visible = 0;
    $DB->update_record('local_agenda_posts', $agenda);
    echo '1';

} elseif ($action == 'agenda_show') {

    $agenda->visible = 1;
    $DB->update_record('local_agenda_posts', $agenda);
    echo '1';

} elseif ($action == 'agenda_delete') {

    require_once($CFG->dirroot . '/repository/lib.php');

    $course = $DB->get_record('course', array('id'=>$agenda->courseid));
    $context = context_course::instance($course->id, MUST_EXIST);
    $fs = get_file_storage();
    $fs->delete_area_files($context->id, 'local_agenda', 'files', $agenda->id);
    $DB->delete_records('local_agenda_posts', array('id'=>$agenda->id));
    echo '1';

} elseif ($action == 'agenda_like') {

    $result = array();
    if ($agenda->id){
        $agenda_likes = $DB->get_record('local_agenda_likes', array('instanceid'=>$agenda->id, 'userid'=>$USER->id));
        if (isset($agenda_likes->id)){
            $DB->delete_records('local_agenda_likes', array('instanceid'=>$agenda->id, 'userid'=>$USER->id));
            $result['type'] = 'dislike';
        } else {
            $new_like = new stdClass();
            $new_like->courseid = $agenda->courseid;
            $new_like->instanceid = $agenda->id;
            $new_like->userid = $USER->id;
            $new_like->timecreated = time();
            $new_like->id = $DB->insert_record('local_agenda_likes', $new_like);
            $result['type'] = 'like';
        }
        $likes_count = $DB->get_record_sql("SELECT COUNT(id) as count FROM {local_agenda_likes} WHERE instanceid = $agenda->id");
        $result['count'] = $likes_count->count;
    }
    echo json_encode($result);

} elseif ($action == 'agenda_comment') {

    $text     = optional_param('text', '', PARAM_RAW);
    $html = '';
    if ($agenda->id and !empty($text)){

        $comment = new stdClass();
        $comment->instanceid = $agenda->id;
        $comment->courseid = $agenda->courseid;
        $comment->userid = $USER->id;
        $comment->text = $text;
        $comment->timecreated = time();
        $comment->id = $DB->insert_record('local_agenda_comments', $comment);

        if ($comment->id){
            $html .= '<li class="comment-item" comment-id="'.$comment->id.'">';
                $html .= '<div class="comments-form-picture">'.$OUTPUT->user_picture($USER, array('size'=>30)).'</div>';
                $html .= '<div class="comment-body">';
                    $html .= '<a href="'.$CFG->wwwroot.'/user/profile.php?id='.$USER->id.'">';
                        $html .= fullname($USER);
                    $html .= '</a> ';
                    $html .= $comment->text;
                    $html .= '<span class="comment-actions"><a href="javascript:void(0);" onclick="agendaDeleteComment('.$comment->id.', '.$agenda->id.');"><i class="fa fa-trash"></i> '.get_string('delete', 'local_agenda').'</a><span class="comment-date">'.userdate($comment->timecreated, '%B %d, %Y %I:%M %p').'</span></div>';
                $html .= '</div>';
            $html .= '</li>';
        }

        $result['html'] = $html;
        echo json_encode($result);
    }
} elseif ($action == 'agenda_delete_comment') {

    $commentid   = optional_param('commentid', 0, PARAM_INT);
    $result = array();

    if ($commentid){
        $comment = $DB->get_record('local_agenda_comments', array('id'=>$commentid));
        if ($comment->id){
            $DB->delete_records('local_agenda_comments', array('id'=>$comment->id));
            $comments_count = $DB->get_record_sql("SELECT COUNT(id) as count FROM {local_agenda_comments} WHERE instanceid = $comment->instanceid");
            $result['count'] = $comments_count->count;
        }
    }
    echo json_encode($result);

} elseif ($action == 'agenda_load_more') {

    require_once($CFG->dirroot . '/repository/lib.php');
    require_once($CFG->dirroot . '/local/agenda/renderer.php');

    $start     = optional_param('start', 0, PARAM_INT);

    $renderer = $PAGE->get_renderer('local_agenda');
    echo $renderer->print_agenda_posts($course->id, $start);

} elseif ($action == "agenda_poll_save"){

    $optionid = optional_param('optionid', 0, PARAM_INT);

    $new_poll = new stdClass();
    $new_poll->courseid = $id;
    $new_poll->instanceid = $agenda->id;
    $new_poll->optionid = $optionid;
    $new_poll->userid = $USER->id;
    $new_poll->timecreated = time();

    if (!$DB->get_record('local_agenda_poll_values', array('instanceid'=>$agenda->id, 'userid'=>$USER->id))){
        $new_poll->id = $DB->insert_record('local_agenda_poll_values', $new_poll);
    }

    $options = $DB->get_records_sql("SELECT cao.*, oc.option_count, ac.agenda_count  FROM {local_agenda_options} cao
                                            LEFT JOIN (SELECT optionid, COUNT(id) as option_count FROM {local_agenda_poll_values} GROUP BY optionid) oc ON oc.optionid = cao.id
                                            LEFT JOIN (SELECT instanceid, COUNT(id) as agenda_count FROM {local_agenda_poll_values} GROUP BY instanceid) ac ON ac.instanceid = cao.instanceid
                                        WHERE cao.instanceid = $agenda->id ORDER BY cao.id ASC");
    $pollscount = $DB->get_record_sql("SELECT COUNT(id) as agenda_count FROM {local_agenda_poll_values}
                                            WHERE instanceid = $agenda->id GROUP BY instanceid");
    $agenda_options_colors = array('#2EC6C8', '#B5A1DD', '#5AB0EE', '#F4984E', '#D77A80', '#DB4B39', '#2A394F', '#8CC63E', '#58585B', '#5B9BB4');

    echo '<div class="post-poll">'.((!empty($agenda->poll)) ? $agenda->poll : '').'<span><i class="fa fa-user"></i> '.((isset($pollscount->agenda_count) and $pollscount->agenda_count > 0) ? number_format($pollscount->agenda_count) : 0).'</span></div>';

    if(count($options) > 0){
        echo '<table class="poll-list">'; $c_f = 0;
                foreach ($options as $option){
                    echo '<tr>';
                        echo '<td class="cpo_1">'.$option->name.'</td>';
                        echo '<td class="cpo_2">';
                            $option_percents = ($option->option_count > 0) ? round(($option->option_count / $option->agenda_count) * 100) : 0;
                            echo '<div class="poll-result-box"><div class="poll-result-inner" style="width:'.$option_percents.'%; background-color:'.$agenda_options_colors[$c_f].'"></div></div><span class="pol-result-num">'.$option_percents.'%</span>';
                        echo '</td>';
                        echo '<td class="cpo_3">'.(($option->option_count) ? $option->option_count : 0).'</td>';
                    echo '</tr>';
                    $c_f = ($c_f <= 9) ? $c_f+1 : $c_f = 0;
                }
        echo '</table>';
    }
    exit;

} elseif ($action == "agenda_poll_show"){

    $options = $DB->get_records_sql("SELECT cao.*, oc.option_count, ac.agenda_count  FROM {local_agenda_options} cao
                                            LEFT JOIN (SELECT optionid, COUNT(id) as option_count FROM {local_agenda_poll_values} GROUP BY optionid) oc ON oc.optionid = cao.id
                                            LEFT JOIN (SELECT instanceid, COUNT(id) as agenda_count FROM {local_agenda_poll_values} GROUP BY instanceid) ac ON ac.instanceid = cao.instanceid
                                        WHERE cao.instanceid = $agenda->id ORDER BY cao.id ASC");
    $pollscount = $DB->get_record_sql("SELECT COUNT(id) as agenda_count FROM {local_agenda_poll_values}
                                            WHERE instanceid = $agenda->id GROUP BY instanceid");
    $agenda_options_colors = array('#2EC6C8', '#B5A1DD', '#5AB0EE', '#F4984E', '#D77A80', '#DB4B39', '#2A394F', '#8CC63E', '#58585B', '#5B9BB4');

    echo '<div class="post-poll">'.((!empty($agenda->poll)) ? $agenda->poll : '').'<span><i class="fa fa-user"></i> '.((isset($pollscount->agenda_count) and $pollscount->agenda_count > 0) ? number_format($pollscount->agenda_count) : 0).'</span></div>';

    if(count($options) > 0){
        echo '<table class="poll-list">'; $c_f = 0;
                foreach ($options as $option){
                    echo '<tr>';
                        echo '<td class="cpo_1">'.$option->name.'</td>';
                        echo '<td class="cpo_2">';
                            $option_percents = ($option->option_count > 0) ? round(($option->option_count / $option->agenda_count) * 100) : 0;
                            echo '<div class="poll-result-box"><div class="poll-result-inner" style="width:'.$option_percents.'%; background-color:'.$agenda_options_colors[$c_f].'"></div></div><span class="pol-result-num">'.$option_percents.'%</span>';
                        echo '</td>';
                        echo '<td class="cpo_3">'.(($option->option_count) ? $option->option_count : 0).'</td>';
                    echo '</tr>';
                    $c_f = ($c_f <= 9) ? $c_f+1 : $c_f = 0;
                }
        echo '</table>';
    }

} elseif ($action == 'get_day_events') {

    require_once($CFG->dirroot.'/calendar/lib.php');
    $date        = optional_param('date', '', PARAM_RAW);
    $output = ''; $events = array();
    if (!empty($date)){
        $date = (!empty($date)) ? strtotime($date) : time();

        $utime = strtotime(userdate($date));
        if ($date < $utime){
            $datetrashold = $utime-$date;
        } elseif ($date > $utimestart){
            $datetrashold = $date-$utime;
        } else {
            $datetrashold = 0;
        }

        $starttime = $date-$datetrashold;
        $endtime = $date+86399+$datetrashold;

        list($courses, $group, $user) = calendar_set_filters(array($course->id));
        $events_db = calendar_get_events($starttime, $endtime, $user, $group, $course->id, true, true);
        $starttime = $date;
        $endtime = $date+86399;
        if (count($events_db) > 0){
            foreach ($events_db as $event){
                if ($event->eventtype == 'due') $event->timestart = strtotime(userdate($event->timestart));
                if (($event->timestart >= $starttime and $event->timestart <= $endtime) or
                ($event->timeduration > 0 and (($starttime >= $event->timestart and $starttime <= ($event->timestart+$event->timeduration)) or ($endtime >= $event->timestart and $endtime <= ($event->timestart+$event->timeduration))))){
                    $events[$event->id] = $event;
                }
            }
        }
        $output .= '<div class="day-title">'.((date('Y-m-d', $starttime) == date('Y-m-d')) ? get_string('today', 'local_agenda').' ' : '').date('F d Y', $utime).'</div>';
        if (count($events) > 0){
            $output .= '<ul>';
                foreach($events as $event){
                    $event = calendar_add_event_metadata($event);
                    $output .= '<li>';
                        $output .= '<div class="event-time-box">';
                            $output .= '<span>'.date('h:i a', strtotime(userdate($event->timestart))).'</span>';
                            $output .= date('m.d', strtotime(userdate($event->timestart))).'<br />'.date('Y', strtotime(userdate($event->timestart)));
                        $output .= '</div>';
                        $output .= '<div class="event-body">';
                            $output .= '<div class="title">'.(($event->referer) ? $event->referer : $event->name).'</div>';
                            $output .= $event->description;
                            if (strlen(strip_tags($event->description)) > 110){
                                $output .= '<i class="fa fa-chevron-up"></i>';
                                $output .= '<i class="fa fa-chevron-down"></i>';
                            }
                        $output .= '</div>';
                    $output .= '</li>';
                }
            $output .= '</ul>';
            $output .= '<script>jQuery(".event-body .fa").click(function(e){ jQuery(this).parent().toggleClass("open");});</script>';
        } else {
            $output .= '<div class="alert alert-success">'.get_string('noevents', 'local_agenda').'</div>';
        }
    }
    echo $output;

} elseif ($action == 'agenda_copy') {

    require_once($CFG->dirroot . '/repository/lib.php');
    $courseid        = optional_param('cid', 0, PARAM_INT);
    if ($courseid > 0){
        $course = $DB->get_record('course', array('id'=>$courseid));
        if ($course->id){
            $new_agenda = new stdClass();
            $new_agenda->courseid = $course->id;
            $new_agenda->userid = $agenda->userid;
            $new_agenda->title = $agenda->title;
            $new_agenda->post = $agenda->post;
            $new_agenda->question = $agenda->question;
            $new_agenda->link = $agenda->link;
            $new_agenda->link_title = $agenda->link_title;
            $new_agenda->link_info = $agenda->link_info;
            $new_agenda->files = $agenda->files;
            $new_agenda->poll = $agenda->poll;
            $new_agenda->allow_likes = $agenda->allow_likes;
            $new_agenda->allow_comments = $agenda->allow_comments;
            $new_agenda->timeopen = $agenda->timeopen;
            $new_agenda->visible = $agenda->visible;
            $new_agenda->timecreated = time();
            $new_agenda->id = $DB->insert_record('local_agenda_posts', $new_agenda);

            $coursecontext = context_course::instance($agenda->courseid, MUST_EXIST);
            $context = context_course::instance($course->id, MUST_EXIST);
            $fs = get_file_storage();

            $newfiles = array('contextid' => $context->id, 'component' => 'local_agenda', 'filearea' => 'postfile', 'itemid' => $new_agenda->id, 'filepath' => '/', 'timecreated' => time(), 'timemodified' => time());
            $files = $fs->get_area_files($coursecontext->id, 'local_agenda', 'postfile', $agenda->id);
            if (count($files) > 0){
                foreach ($files as $file) {
                    if ($file->get_filename() == '.') continue;
                    $fs->create_file_from_storedfile($newfiles, $file);
                }
            }

            $newfiles = array('contextid' => $context->id, 'component' => 'local_agenda', 'filearea' => 'questionfile', 'itemid' => $new_agenda->id, 'filepath' => '/', 'timecreated' => time(), 'timemodified' => time());
            $files = $fs->get_area_files($coursecontext->id, 'local_agenda', 'questionfile', $agenda->id);
            if (count($files) > 0){
                foreach ($files as $file) {
                    if ($file->get_filename() == '.') continue;
                    $fs->create_file_from_storedfile($newfiles, $file);
                }
            }

            $newfiles = array('contextid' => $context->id, 'component' => 'local_agenda', 'filearea' => 'pollfile', 'itemid' => $new_agenda->id, 'filepath' => '/', 'timecreated' => time(), 'timemodified' => time());
            $files = $fs->get_area_files($coursecontext->id, 'local_agenda', 'pollfile', $agenda->id);
            if (count($files) > 0){
                foreach ($files as $file) {
                    if ($file->get_filename() == '.') continue;
                    $fs->create_file_from_storedfile($newfiles, $file);
                }
            }

            $newfiles = array('contextid' => $context->id, 'component' => 'local_agenda', 'filearea' => 'files', 'itemid' => $new_agenda->id, 'filepath' => '/', 'timecreated' => time(), 'timemodified' => time());
            $files = $fs->get_area_files($coursecontext->id, 'local_agenda', 'files', $agenda->id);
            if (count($files) > 0){
                foreach ($files as $file) {
                    if ($file->get_filename() == '.') continue;
                    $fs->create_file_from_storedfile($newfiles, $file);
                }
            }

            // copy polls
            $polls = $DB->get_records('local_agenda_options', array('instanceid'=>$agenda->id));
            if (count($polls)) {
                foreach ($polls as $poll) {
                    unset($poll->id);
                    $poll->courseid = $new_agenda->courseid;
                    $poll->instanceid = $new_agenda->id;
                    $DB->insert_record('local_agenda_options', $poll);
                }
            }
        }
    }
    echo '1';
} elseif ($action == 'get_events') {
    require_once($CFG->dirroot.'/local/agenda/locallib.php');

    echo json_encode(local_agenda_get_events($course->id));
    exit;
}

exit;
