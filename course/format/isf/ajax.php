<?php

define('AJAX_SCRIPT', true);
require_once('../../../config.php');
require_once($CFG->dirroot . '/course/format/lib.php');
require_once('locallib.php');

$courseid    = optional_param('courseid', 0, PARAM_INT);
$userid      = optional_param('userid', 0, PARAM_INT);
$action      = optional_param('action', '', PARAM_RAW);

if ($courseid > 0){
    // make sure all sections are created
    $course = course_get_format($courseid)->get_course();
    course_create_sections_if_missing($course, range(0, $course->numsections));
    context_helper::preload_course($course->id);
    $context = context_course::instance($course->id, MUST_EXIST);
    $PAGE->set_context($context);
}

$Course = new isfCourse($course);
$params = array();

if (method_exists($Course, $action)){
    $Course->{$action}($params);
}


exit;
