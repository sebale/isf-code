<?php

/**
 * Defines restore_format_isf_plugin class
 *
 * @package     format_isf
 * @category    backup
 * @copyright  2017 ISF
 */

defined('MOODLE_INTERNAL') || die();
require_once($CFG->dirroot . '/course/format/isf/lib.php');

/**
 * resource restore task that provides all the settings and steps to perform one
 * complete restore of the activity
 */
class restore_format_isf_plugin extends restore_format_plugin {

    /**
     * Returns the paths to be handled by the plugin at course level
     */
    protected function define_course_plugin_structure() {
        $paths = array();

        // Add own format stuff.
        $elename = 'isf'; // This defines the postfix of 'process_*' below.

        /*
         * This is defines the nested tag within 'plugin_format_isf_course' to allow '/course/plugin_format_isf_course' in
         * the path therefore as a path structure representing the levels in section.xml in the backup file.
         */
        $elepath = $this->get_pathfor('/');
        $paths[] = new restore_path_element($elename, $elepath);

        $users = $this->get_setting_value('users');
        if ($users){
            $paths[] = new restore_path_element('local_agenda_posts', $this->get_pathfor('/agendas/local_agenda_posts'));
            $paths[] = new restore_path_element('local_agenda_comments', $this->get_pathfor('/agendas/local_agenda_posts/local_agenda_comments'));
            $paths[] = new restore_path_element('local_agenda_likes', $this->get_pathfor('/agendas/local_agenda_posts/local_agenda_likes'));
            $paths[] = new restore_path_element('local_agenda_options', $this->get_pathfor('/agendas/local_agenda_posts/local_agenda_options'));
            $paths[] = new restore_path_element('local_agenda_poll_values', $this->get_pathfor('/agendas/local_agenda_posts/local_agenda_poll_values'));
        }

        return $paths;
    }

    public function process_isf($data) {
        global $DB;
        $data = (object) $data;
        /* We only process this information if the course we are restoring to
          has 'isf' format (target format can change depending of restore options). */
        $format = $DB->get_field('course', 'format', array('id' => $this->task->get_courseid()));
        if ($format != 'isf') {
            return;
        }
    }

    public function process_local_agenda_posts($data) {
        global $DB;
        // Get data record ready to insert in database
        $data = (object)$data;
        $oldid = $data->id;
        $data->courseid = $this->task->get_courseid();
        if ($data->userid > 0) {
            $data->userid = $this->get_mappingid('user', $data->userid);
        }
        // See if there is an existing record for this course
        $newitemid = $DB->insert_record('local_agenda_posts', $data);

        $this->set_mapping('local_agenda_posts', $oldid, $newitemid);
        $this->add_related_files('local_agenda', 'files', 'local_agenda_posts', null, $oldid);
        $this->add_related_files('local_agenda', 'postfile', 'local_agenda_posts', null, $oldid);
        $this->add_related_files('local_agenda', 'questionfile', 'local_agenda_posts', null, $oldid);
        $this->add_related_files('local_agenda', 'poll', 'local_agenda_posts', null, $oldid);
    }

    public function process_local_agenda_comments($data) {
        global $DB;
        // Get data record ready to insert in database
        $data = (object)$data;
        $oldid = $data->id;
        $data->courseid = $this->task->get_courseid();
        $data->instanceid = $this->get_new_parentid('local_agenda_posts');
        $data->userid = $this->get_mappingid('user', $data->userid);
        $data->timecreated = $this->apply_date_offset($data->timecreated);
        $newitemid = $DB->insert_record('local_agenda_comments', $data);
    }

    public function process_local_agenda_likes($data) {
        global $DB;
        // Get data record ready to insert in database
        $data = (object)$data;
        $oldid = $data->id;
        $data->courseid = $this->task->get_courseid();
        $data->instanceid = $this->get_new_parentid('local_agenda_posts');
        $data->userid = $this->get_mappingid('user', $data->userid);
        $data->timecreated = $this->apply_date_offset($data->timecreated);
        $newitemid = $DB->insert_record('local_agenda_likes', $data);
    }

    public function process_local_agenda_options($data) {
        global $DB;
        // Get data record ready to insert in database
        $data = (object)$data;
        $oldid = $data->id;
        $data->courseid = $this->task->get_courseid();
        $data->instanceid = $this->get_new_parentid('local_agenda_posts');
        $data->name = $data->name;
        $newitemid = $DB->insert_record('local_agenda_options', $data);
        $this->set_mapping('local_agenda_options', $oldid, $newitemid);
    }

    public function process_local_agenda_poll_values($data) {
        global $DB;
        // Get data record ready to insert in database
        $data = (object)$data;
        $oldid = $data->id;
        $data->courseid = $this->task->get_courseid();
        $data->instanceid = $this->get_new_parentid('local_agenda_posts');
        $data->optionid = $this->get_mappingid('local_agenda_options', $data->optionid);
        $data->userid = $this->get_mappingid('user', $data->userid);
        $data->timecreated = $this->apply_date_offset($data->timecreated);
        $newitemid = $DB->insert_record('local_agenda_agenda_poll_values', $data);
    }

    /**
     * Returns the paths to be handled by the plugin at section level
     */
    protected function define_module_plugin_structure() {
        $paths = array();

        $elepath = $this->get_pathfor('/activityimages/activityimage');
        $paths[] = new restore_path_element('activityimage', $elepath);

        return $paths;
    }

    public function process_activityimage($data) {
        global $DB;
        $data = (object) $data;

        if (empty($data->id)){
            return;
        }

        $courseid = $this->task->get_courseid();
        $newitemid = $this->task->get_moduleid();
        $this->set_mapping('activityimage', $data->id, $newitemid, true, null);
        $this->process_activity_images($data->id, $newitemid);
    }

    protected function after_restore_course() {
        global $DB;

        // process format images
        $context = context_course::instance($this->task->get_courseid());

        // course images
        $this->add_related_files('format_isf', 'thumbnail', null);
        $this->update_restored_file('format_isf', 'thumbnail', $context->id, $this->task->get_old_courseid(), $this->task->get_courseid());
        $this->add_related_files('format_isf', 'wallimage', null);
        $this->update_restored_file('format_isf', 'wallimage', $context->id, $this->task->get_old_courseid(), $this->task->get_courseid());
    }


    protected function update_restored_file($component, $filearea, $contextid, $olditemid, $newitemid) {
        global $DB;

        $fs = get_file_storage();
        $files = $DB->get_records_sql("SELECT *
                      FROM {files}
                     WHERE contextid = ?
                       AND component = ?
                       AND filearea  = ?
                       AND itemid  = ?", array($contextid, $component, $filearea, $olditemid));

        if (count($files)) {
            foreach ($files as $file) {
                if ($file->filepath == '/' && $file->filename == '.') {
                    continue;
                }

                $file_record = array(
                    'contextid'   => $contextid,
                    'component'   => $component,
                    'filearea'    => $filearea,
                    'itemid'      => $newitemid,
                    'filepath'    => $file->filepath,
                    'filename'    => $file->filename,
                    'timecreated' => $file->timecreated,
                    'timemodified'=> $file->timemodified,
                    'userid'      => $file->userid,
                    'source'      => $file->source,
                    'author'      => $file->author,
                    'license'     => $file->license,
                    'sortorder'   => $file->sortorder
                );

                $fs->create_file_from_storedfile($file_record, $file->id);
            }
            $fs->delete_area_files($contextid, $component, $filearea, $olditemid);
        }
    }

    protected function process_activity_images($oldid, $newitemid) {
        global $CFG, $USER, $DB;
        // activity images
        $component = 'format_isf'; $filearea = 'activityimage';
        $context = context_module::instance($newitemid);
        $newcontextid = $context->id;
        $restoreid = $this->get_restoreid();

        $basepath = $this->task->get_basepath();
        $backupinfo = backup_general_helper::get_backup_information(basename($basepath));
        $includesfiles = $backupinfo->include_files;

        $sql = "SELECT f.id AS bftid, f.contextid, f.component, f.filearea, f.itemid, f.info
                      FROM {backup_files_temp} f
                     WHERE f.backupid = ?
                       AND f.component = ?
                       AND f.filearea = ?
                       AND f.itemid = ?";
        $params = array($restoreid, $component, $filearea, $oldid);

        $fs = get_file_storage();         // Get moodle file storage
        $basepath .= '/files/';// Get backup file pool base

        $rs = $DB->get_records_sql($sql, $params);
        foreach ($rs as $rec) {
            $rec->newitemid = $newitemid;
            $file = (object)backup_controller_dbops::decode_backup_temp_info($rec->info);

            // ignore root dirs (they are created automatically)
            if ($file->filepath == '/' && $file->filename == '.') {
                continue;
            }

            // set the best possible user
            $mappeduser = restore_dbops::get_backup_ids_record($restoreid, 'user', $file->userid);
            $mappeduserid = !empty($mappeduser) ? $mappeduser->newitemid : $USER->id;

            // dir found (and not root one), let's create it
            if ($file->filename == '.') {
                $fs->create_directory($newcontextid, $component, $filearea, $newitemid, $file->filepath, $mappeduserid);
                continue;
            }

            // The file record to restore.
            $file_record = array(
                'contextid'   => $newcontextid,
                'component'   => $component,
                'filearea'    => $filearea,
                'itemid'      => $newitemid,
                'filepath'    => $file->filepath,
                'filename'    => $file->filename,
                'timecreated' => $file->timecreated,
                'timemodified'=> $file->timemodified,
                'userid'      => $mappeduserid,
                'source'      => $file->source,
                'author'      => $file->author,
                'license'     => $file->license,
                'sortorder'   => $file->sortorder
            );

            if (empty($file->repositoryid)) {
                // If contenthash is empty then gracefully skip adding file.
                if (empty($file->contenthash)) {
                    continue;
                }
                // this is a regular file, it must be present in the backup pool
                $backuppath = $basepath . backup_file_manager::get_backup_content_file_location($file->contenthash);

                // Some file types do not include the files as they should already be
                // present. We still need to create entries into the files table.
                if ($includesfiles) {
                    // The file is not found in the backup.
                    if (!file_exists($backuppath)) {
                        continue;
                    }
                    // create the file in the filepool if it does not exist yet
                    if (!$fs->file_exists($newcontextid, $component, $filearea, $newitemid, $file->filepath, $file->filename)) {

                        // If no license found, use default.
                        if ($file->license == null){
                            $file->license = $CFG->sitedefaultlicense;
                        }

                        $fs->create_file_from_pathname($file_record, $backuppath);
                    }
                } else {
                    // This backup does not include the files - they should be available in moodle filestorage already.

                    // Create the file in the filepool if it does not exist yet.
                    if (!$fs->file_exists($newcontextid, $component, $filearea, $newitemid, $file->filepath, $file->filename)) {

                        // Even if a file has been deleted since the backup was made, the file metadata will remain in the
                        // files table, and the file will not be moved to the trashdir.
                        // Files are not cleared from the files table by cron until several days after deletion.
                        if ($foundfiles = $DB->get_records('files', array('contenthash' => $file->contenthash), '', '*', 0, 1)) {
                            // Only grab one of the foundfiles - the file content should be the same for all entries.
                            $foundfile = reset($foundfiles);
                            $fs->create_file_from_storedfile($file_record, $foundfile->id);
                        } else {
                            // A matching existing file record was not found in the database.
                            $results[] = self::get_missing_file_result($file);
                            continue;
                        }
                    }
                }

                // store the the new contextid and the new itemid in case we need to remap
                // references to this file later
                $DB->update_record('backup_files_temp', array(
                    'id' => $rec->bftid,
                    'newcontextid' => $newcontextid,
                    'newitemid' => $newitemid), true);

            } else {
                // this is an alias - we can't create it yet so we stash it in a temp
                // table and will let the final task to deal with it
                if (!$fs->file_exists($newcontextid, $component, $filearea, $newitemid, $file->filepath, $file->filename)) {
                    $info = new stdClass();
                    // oldfile holds the raw information stored in MBZ (including reference-related info)
                    $info->oldfile = $file;
                    // newfile holds the info for the new file_record with the context, user and itemid mapped
                    $info->newfile = (object) $file_record;

                    restore_dbops::set_backup_ids_record($restoreid, 'file_aliases_queue', $file->id, 0, null, $info);
                }
            }
        }
    }

}
