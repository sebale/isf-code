var QueryString = function () {
    // This function is anonymous, is executed immediately and
    // the return value is assigned to QueryString!
    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        // If first entry with this name
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
            // If second entry with this name
        } else if (typeof query_string[pair[0]] === "string") {
            var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
            query_string[pair[0]] = arr;
            // If third or later entry with this name
        } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
        }
    }
    return query_string;
}();

function load_tab(tab, courseid) {
    $.ajax({
        url: '/course/format/isf/ajax_items/ajax_' + tab + '.php',
        method: 'post',
        //dataType: 'html',
        data: {courseid: courseid},
        beforeSend: function(xhr) {
            $(".format-isf .course-view-tabs .tab-content").prepend('<div class="loader"><p class="center"><i class="fa fa-spin fa-3x fa-spinner"></i></p></div>');
        }
    }).done(function(data) {
        $(".format-isf .course-view-tabs .tab-content .loader").remove();
        $("#" + tab).html(data);
    });
}

$(function() {
    $('.format-isf .course-view-tabs .nav-pills .nav-link').bind('click', function (event) {
        load_tab($(this).attr('href').substr(1), QueryString.id);
    });

    load_tab($('.format-isf .course-view-tabs .nav-pills .nav-link.active').attr('href').substr(1), QueryString.id);

    $('#show-img').on('click', function (event) {
        event.preventDefault();
        $('#show-img').addClass('hidden');
        $('.format-isf .course-img .image').toggleClass('hidden');
    });

    $('#close-img').on('click', function (event) {
        event.preventDefault();
        $('#show-img').removeClass('hidden');
        $('.format-isf .course-img .image').toggleClass('hidden');
    });
});