<?php
namespace format_isf;

/**
 * ISF Activity Duplication
 * View the associated README.md file for more information.
 *
 * Hook functions for changes in core code.
 *
 * @package    local_isfdup
 * @author     Shane Elliott (@link shane@pukunui.com)
 * @copyright  2017 Pukunui (@link pukunui.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use html_writer;
use moodle_url;
use lang_string;
use context_module;

require_once($CFG->dirroot.'/course/format/isf/lib.php');
require_once($CFG->dirroot.'/course/format/isf/locallib.php');

/**
 * Hook functions.
 *
 * @package    format_isf
 * @author     ISF
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class hooks {

    public static function duplicate_activityimage($cm, $newcm) {
        global $DB, $CFG;

        $cmcontext = context_module::instance($cm->id);
        $newcmcontext = context_module::instance($newcm->id);

        $fs = get_file_storage(); $file = null;
        $imgfiles = $fs->get_area_files($cmcontext->id, 'format_isf', 'activityimage', $cm->id);
        foreach ($imgfiles as $imgfile) {
            $filename = $imgfile->get_filename();
            $filetype = $imgfile->get_mimetype();
            if ($filename == '.' or !$filetype) continue;
            $file = $imgfile;
        }

        if ($file and $imagefile = $file->copy_content_to_temp()) {
            $fs->delete_area_files($newcmcontext->id, 'format_isf', 'activityimage', $newcm->id);
            $newimageid = format_isf_process_icon($newcmcontext, 'format_isf', 'activityimage', $newcm->id, $imagefile, true, $filename);
            @unlink($imagefile);
        }
    }

}
