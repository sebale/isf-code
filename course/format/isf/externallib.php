<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * External message popup API
 *
 * @package    format_isf
 * @category   external
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once("$CFG->libdir/externallib.php");

/**
 * Message external functions
 *
 * @package    format_isf
 * @category   external
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class format_isf_external extends external_api {

    /**
     * Get popup announcements parameters description.
     *
     * @return external_function_parameters
     * @since 3.2
     */
    public static function save_user_preferences_parameters() {
        return new external_function_parameters(
            array(
                'name' => new external_value(PARAM_NOTAGS, 'preference name'),
                'value' => new external_value(PARAM_NOTAGS, 'preference value', VALUE_DEFAULT, ''),
                'instanceid' => new external_value(PARAM_NOTAGS, 'instance id', VALUE_DEFAULT, ''),
                'courseid' => new external_value(PARAM_INT, 'courseid', VALUE_DEFAULT, 0)
            )
        );
    }

    /**
     * Save preferences function.
     *
     * @since  3.2
     * @throws invalid_parameter_exception
     * @throws moodle_exception
     * @param  char     $name               preference name
     * @param  char     $value              preference value
     * @param  courseid $courseid           course id
     * @return external_description
     */
    public static function save_user_preferences($name, $value, $instanceid, $courseid) {
        global $USER, $PAGE, $DB;

        $params = self::validate_parameters(
            self::save_user_preferences_parameters(),
            array(
                'name' => $name,
                'value' => $value,
                'instanceid' => $instanceid,
                'courseid' => $courseid
            )
        );

        if ($courseid > 0) {
            $context = context_course::instance($courseid);
        } else {
            $context = context_system::instance();
        }
        self::validate_context($context);

        $name = $params['name'];
        $value = $params['value'];
        $section = $params['instanceid'];
        $courseid = $params['courseid'];

        if ($name == 'toggled_all_course_sections') {
            if ($value == 1){
                $coursesections = $DB->get_records('course_sections', array('course'=>$courseid));
                $sections = array();
                if (count($coursesections)){
                    foreach ($coursesections as $section){
                        $sections[] = $section->id;
                    }
                    set_user_preference('toggled_sections_'.$courseid, implode(',', $sections));
                    set_user_preference('toggledall_sections_'.$courseid, '1');
                }
            } else {
                set_user_preference('toggled_sections_'.$courseid, '0');
                set_user_preference('toggledall_sections_'.$courseid, '0');
            }
        } elseif ($name == 'toggled_course_sections') {
            $toggled = get_user_preferences('toggled_sections_'.$courseid, '0');
            set_user_preference('toggledall_sections_'.$courseid, '0');

            if ($toggled == '0') {
                if ($value == '1'){
                    set_user_preference('toggled_sections_'.$courseid, $instanceid);
                }
            } else {
                $sections = explode(',', $toggled);
                if ($value == '1' and !in_array($instanceid, $sections)){
                    $sections[] = $instanceid;
                } else if ($value == '0' and in_array($instanceid, $sections)) {
                    $key = array_search ($instanceid, $sections);
                    unset($sections[$key]);
                }

                if (count($sections)){
                    set_user_preference('toggled_sections_'.$courseid, implode(',', $sections));
                } else {
                    set_user_preference('toggled_sections_'.$courseid, '0');
                }
            }
        } elseif ($name == 'fav_course_modules') {
            $fav = get_user_preferences('fav_modules_'.$courseid, '0');

            if ($fav == '0') {
                if ($value == '1'){
                    set_user_preference('fav_modules_'.$courseid, $instanceid);
                }
            } else {
                $modules = explode(',', $fav);
                if ($value == '1' and !in_array($instanceid, $modules)){
                    $modules[] = $instanceid;
                } else if ($value == '0' and in_array($instanceid, $modules)) {
                    $key = array_search ($instanceid, $modules);
                    unset($modules[$key]);
                }

                if (count($modules)){
                    set_user_preference('fav_modules_'.$courseid, implode(',', $modules));
                } else {
                    set_user_preference('fav_modules_'.$courseid, '0');
                }
            }
        } else {
            set_user_preference($name.'_'.$courseid, $value);
        }

        return array(
            'status' => 1,
        );
    }

    /**
    * Returns description of method result value
    * @return external_description
    */
    public static function save_user_preferences_returns() {
        return new external_single_structure(
            array (
                'status' => new external_value(PARAM_NOTAGS, 'Status of response')
            )
        );
    }

}
