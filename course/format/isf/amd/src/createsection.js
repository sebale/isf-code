// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is an empty module, that is required before all other modules.
 * Because every module is returned from a request for any other module, this
 * forces the loading of all modules with a single request.
 *
 * This function also sets up the listeners for ajax requests so we can tell
 * if any requests are still in progress.
 *
 * @package    format_isf
 * @module     format_isf/createsection
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define(['jquery', 'core/ajax', 'core/log', 'core/str', 'core/config', 'core/modal_factory', 'core/modal_events', 'core/templates', 'theme_primaryisf/classyeditor'], function($, ajax, log, str, mdlcfg, ModalFactory, ModalEvents, Templates, editor) {

    editor.init($);

    var SectionModal = {
        init: function(courseid, formid) {

            var createsection_trigger = $('.create-sections');
            str.get_strings([
                { key: 'createsection', component: 'format_isf' },
            ]).then(function(add) {
                var form = $('#'+formid).html();
                $('#'+formid).remove();
                ModalFactory.create({
                    type: ModalFactory.types.SAVE_CANCEL,
                    title: add,
                    body: form,
                  }, createsection_trigger)
                  .done(function(modal) {

                    var form = modal.getBody().find('#create_section_form');
                    var savebtn = modal.getFooter().find('[data-action="save"]');
                    var name = modal.getBody().find('.section-name');
                    var summary = modal.getBody().find('#summary');
                    $(summary).Editor();

                    $(savebtn).prop('disabled', true);
                    $(name).change(function(e){
                        if ($(name).val() != '') {
                            $(savebtn).prop('disabled', false);
                        } else {
                            $(savebtn).prop('disabled', true);
                        }
                    });

                    modal.getRoot().on(ModalEvents.save, function(e) {
                        if ($(name).val() != ''){
                            var text = $(summary).Editor("getText");
                            $(summary).val(text);

                            $.ajax({
                                url: $(form).attr('action'),
                                type: "POST",
                                data: $(form).serialize(),
                            }).done(function( data ) {
                                location.reload();
                            });
                        }
                    });


                  });
            });

        }
    };

    /**
    * @alias module:format_isf/course
    */
    return SectionModal;

});
