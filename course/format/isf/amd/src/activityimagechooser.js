// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is an empty module, that is required before all other modules.
 * Because every module is returned from a request for any other module, this
 * forces the loading of all modules with a single request.
 *
 * This function also sets up the listeners for ajax requests so we can tell
 * if any requests are still in progress.
 *
 * @package    format_isf
 * @module     format_isf/course
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define(['jquery', 'theme_primaryisf/bxslider'], function($, Bxslider) {

    Bxslider.init($, document, window, undefined);

    window.chooseActivityImage = function(fileid) {
        $(".course-images-box").removeClass('checked');
        $(".course-images-box.cfile_"+fileid).addClass('checked');
        $('input[name="image"]').prop('checked', false);
        $("#id_image_"+fileid).prop('checked', true);
    }

    var Chooser = {
        init: function() {
            $('.activity-images-container ul').bxSlider({
              hideControlOnEnd: true,
              slideWidth: 130,
              slideMargin: 10,
              responsive: true,
              pager: false,
              infiniteLoop: true,
            });
        }
    };

    /**
    * @alias module:format_isf/course
    */
    return Chooser;

});
