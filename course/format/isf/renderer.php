<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderer for outputting the topics course format.
 *
 * @package format_isf
 * @copyright 2012 Dan Poltawski
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @since Moodle 2.3
 */


defined('MOODLE_INTERNAL') || die();
require_once($CFG->dirroot.'/course/format/renderer.php');

class format_isf_course_content_header implements renderable {}
class format_isf_course_content_footer implements renderable {}

/**
 * Basic renderer for topics format.
 *
 * @copyright 2012 Dan Poltawski
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class format_isf_renderer extends format_section_renderer_base {

    public $_user_preferences = array();

    /**
     * Constructor method, calls the parent constructor
     *
     * @param moodle_page $page
     * @param string $target one of rendering target constants
     */
    public function __construct(moodle_page $page, $target) {
        parent::__construct($page, $target);

        $this->get_user_preferences();

        // Since format_isf_renderer::section_edit_controls() only displays the 'Set current section' control when editing mode is on
        // we need to be sure that the link 'Turn editing mode on' is available for a user who does not have any other managing capability.
        $page->set_other_editing_capability('moodle/course:setcurrentsection');
    }

    public function get_user_preferences() {
        $course = $this->page->course;

        $preferences_list = format_isf_get_preferences_list($course);

        foreach ($preferences_list as $name=>$default) {
            $pref_name = str_replace('_'.$course->id, '', $name);
            $this->_user_preferences[$pref_name] = get_user_preferences($name, $default);

            if ($pref_name == 'toggled_sections'){
                if ($this->_user_preferences[$pref_name] != '0') {
                    $sections = explode(',', $this->_user_preferences[$pref_name]);
                    $this->_user_preferences[$pref_name] = $sections;
                }
            } elseif ($pref_name == 'toggledall_sections'){
                $this->_user_preferences[$pref_name] = get_user_preferences($name, $default);
            } elseif ($pref_name == 'activetab'){
                $tab     = optional_param('tab', '', PARAM_RAW);
                $isgradebook = (stristr($this->page->url, '/grade/'));
                if (!empty($tab)) {
                    set_user_preference('activetab_'.$course->id, $tab);
                    $this->_user_preferences[$pref_name] = $tab;
                } elseif ($isgradebook) {
                    $this->_user_preferences[$pref_name] = 'gradebook';
                } else {
                    $this->_user_preferences[$pref_name] = get_user_preferences($name, $default);
                }
            } elseif ($pref_name == 'fav_modules'){
                if ($this->_user_preferences[$pref_name] != '0') {
                    $modules = explode(',', $this->_user_preferences[$pref_name]);
                    $this->_user_preferences[$pref_name] = $modules;
                }
            }
        }
    }

    /**
     * Generate the starting container html for a list of sections
     * @return string HTML to output.
     */
    protected function start_section_list() {
        return html_writer::start_tag('ul', array('class' => 'topics'));
    }

    /**
     * Generate the closing container html for a list of sections
     * @return string HTML to output.
     */
    protected function end_section_list() {
        return html_writer::end_tag('ul');
    }

    /**
     * Generate the title for this section page
     * @return string the page title
     */
    protected function page_title() {
        return get_string('topicoutline');
    }

    /**
     * Generate the section title, wraps it in a link to the section page if page is to be displayed on a separate page
     *
     * @param stdClass $section The course_section entry from DB
     * @param stdClass $course The course entry from DB
     * @return string HTML to output.
     */
    public function section_title($section, $course) {
        return $this->render(course_get_format($course)->inplace_editable_render_section_name($section));
    }

    /**
     * Generate the section title to be displayed on the section page, without a link
     *
     * @param stdClass $section The course_section entry from DB
     * @param stdClass $course The course entry from DB
     * @return string HTML to output.
     */
    public function section_title_without_link($section, $course) {
        return $this->render(course_get_format($course)->inplace_editable_render_section_name($section, false));
    }

    /**
     * Generate the edit control items of a section
     *
     * @param stdClass $course The course entry from DB
     * @param stdClass $section The course_section entry from DB
     * @param bool $onsectionpage true if being printed on a section page
     * @return array of edit control items
     */
    protected function section_edit_control_items($course, $section, $onsectionpage = false)
    {
        global $PAGE;

        if (!$PAGE->user_is_editing()) {
            return array();
        }

        $coursecontext = context_course::instance($course->id);

        if ($onsectionpage) {
            $url = course_get_url($course, $section->section);
        } else {
            $url = course_get_url($course);
        }
        $url->param('sesskey', sesskey());

        $isstealth = $section->section > $course->numsections;
        $controls = array();

        if ($section->section) {
            if (\local_isfdup\hooks::can_duplicate($section->course)) {
                $controls['tsduplicateto'] = \local_isfdup\hooks::section_menu_link($section);
            }
        }

        if (!$isstealth && $section->section && has_capability('moodle/course:setcurrentsection', $coursecontext)) {
            if ($course->marker == $section->section) {  // Show the "light globe" on/off.
                $url->param('marker', 0);
                $markedthistopic = get_string('markedthistopic');
                $highlightoff = get_string('highlightoff');
                $controls['highlight'] = array('url' => $url, "icon" => 'i/marked',
                    'name' => $highlightoff,
                    'pixattr' => array('class' => '', 'alt' => $markedthistopic),
                    'attr' => array('class' => 'editing_highlight', 'title' => $markedthistopic));
            } else {
                $url->param('marker', $section->section);
                $markthistopic = get_string('markthistopic');
                $highlight = get_string('highlight');
                $controls['highlight'] = array('url' => $url, "icon" => 'i/marker',
                    'name' => $highlight,
                    'pixattr' => array('class' => '', 'alt' => $markthistopic),
                    'attr' => array('class' => 'editing_highlight', 'title' => $markthistopic));
            }
        }

        $parentcontrols = parent::section_edit_control_items($course, $section, $onsectionpage);

        // If the edit key exists, we are going to insert our controls after it.
        if (array_key_exists("edit", $parentcontrols)) {
            $merged = array();
            // We can't use splice because we are using associative arrays.
            // Step through the array and merge the arrays.
            foreach ($parentcontrols as $key => $action) {
                $merged[$key] = $action;
                if ($key == "edit") {
                    // If we have come to the edit key, merge these controls here.
                    $merged = array_merge($merged, $controls);
                }
            }

            return $merged;
        } else {
            return array_merge($controls, $parentcontrols);
        }
    }

    private function a($title, $href, $class = 'nav-link', $attr = array()) {

        $params = array(
            'class' => $class,
            'role' => 'tab'
        ) + $attr;

        if ($attr['data-type'] == 'tab') {
            $params['data-toggle'] = 'tab';
        }

        return html_writer::link($href, $title, $params);
    }

    private function get_course_tabs($course, $displaysection, $display = 'all') {
        global $CFG;

        $context = context_course::instance($course->id);
        $canedit = (has_capability('moodle/course:update', $context));
        $isstudent = (!$canedit);
        $isparent = (has_capability('local/manager:is_parent', $context) and !is_siteadmin());
        $isgradebook = (stristr($this->page->url, '/grade/'));

        $course_content = ''; $course_summary = '';
        if ($display != 'tabs'){
            if (!empty($displaysection)) {
                $course_content = $this->print_single_section_page($course, null, null, null, null, $displaysection);
            } else {
                $course_content = $this->print_multiple_section_page($course, null, null, null, null);
            }

            $course_summary = $this->get_course_formatted_summary($course, array('overflowdiv' => true, 'noclean' => false, 'para' => false));
        }

        $tabs = array();

        $tabs[] = array(
            'title' => get_string('course', 'format_isf'),
            'href' => ($isgradebook) ? $CFG->wwwroot.'/course/view.php?id='.$course->id.'&tab=course' : 'course',
            'link' => $CFG->wwwroot.'/course/view.php?id='.$course->id.'&tab=course',
            'type' => ($isgradebook) ? 'link' : 'tab',
            'active' => ($this->_user_preferences['activetab'] == 'course'),
            'classes' => 'course-content'.
                        (($this->_user_preferences['toggledall_sections'] == '1') ? ' toggled' : '') .
                        (($course->coursetype) ? ' primary-course-view' : ' secondary-course-view') . ' ' .
                        ($this->_user_preferences['course_view']) .
                        (((int)$course->columns > 2) ? ' columns-'.$course->columns : ' columns-2'),
            'content' => $course_content
        );

        if ($course->tabagenda == 1 or ($course->tabagenda == 2 and !$isstudent) or ($course->tabagenda == 3 and !$isparent)){
            $tabs[] = array(
                'title' => get_string('agenda', 'format_isf'),
                'href' => ($isgradebook) ? $CFG->wwwroot.'/course/view.php?id='.$course->id.'&tab=agenda' : 'agenda',
                'link' => $CFG->wwwroot.'/course/view.php?id='.$course->id.'&tab=agenda',
                'type' => ($isgradebook) ? 'link' : 'tab',
                'active' => ($this->_user_preferences['activetab'] == 'agenda'),
                'classes' => 'course-agenda',
                'content' => ''
            );
        }

        if ($course->tabinfo == 1 or ($course->tabinfo == 2 and !$isstudent) or ($course->tabinfo == 3 and !$isparent)){
            $tabs[] = array(
                'title' => get_string('courseinfo', 'format_isf'),
                'href' => ($isgradebook) ? $CFG->wwwroot.'/course/view.php?id='.$course->id.'&tab=courseinfo' : 'courseinfo',
                'link' => $CFG->wwwroot.'/course/view.php?id='.$course->id.'&tab=courseinfo',
                'type' => ($isgradebook) ? 'link' : 'tab',
                'active' => ($this->_user_preferences['activetab'] == 'courseinfo'),
                'classes' => 'course-info',
                'content' => $course_summary
            );
        }

        if ($course->tabgrades == 1 or ($course->tabgrades == 2 and !$isstudent) or ($course->tabgrades == 3 and !$isparent)) {
            $tabs[] = array(
                'title' => get_string('gradessummary', 'format_isf'),
                'href' => ($isgradebook) ? $CFG->wwwroot.'/course/view.php?id='.$course->id.'&tab=gradessummary' : 'gradessummary',
                'link' => $CFG->wwwroot.'/course/view.php?id='.$course->id.'&tab=gradessummary',
                'type' => ($isgradebook) ? 'link' : 'tab',
                'active' => ($this->_user_preferences['activetab'] == 'gradessummary'),
                'classes' => 'course-gradessummary',
                'content' => ''
            );
        }


        if ($course->tabgradebook == 1 or ($course->tabgradebook == 2 and !$isstudent) or ($course->tabgradebook == 3 and !$isparent)){
            $tabs[] = array(
                'title' => get_string('gradebook', 'format_isf'),
                'href' => $CFG->wwwroot.'/grade/report/grader/index.php?id='.$course->id,
                'link' => $CFG->wwwroot.'/grade/report/grader/index.php?id='.$course->id,
                'type' => 'link',
                'active' => ($this->_user_preferences['activetab'] == 'gradebook'),
                'classes' => 'course-gradebook',
                'content' => ''
            );
        }

        if ($course->tabra == 1 or ($course->tabra == 2 and !$isstudent) or ($course->tabra == 3 and !$isparent)){
            $tabs[] = array(
                'title' => get_string('recentactivity', 'format_isf'),
                'href' => ($isgradebook) ? $CFG->wwwroot.'/course/view.php?id='.$course->id.'&tab=recentactivity' : 'recentactivity',
                'link' => $CFG->wwwroot.'/course/view.php?id='.$course->id.'&tab=recentactivity',
                'type' => ($isgradebook) ? 'link' : 'tab',
                'active' => ($this->_user_preferences['activetab'] == 'recentactivity'),
                'classes' => 'course-recentactivity',
                'content' => ''
            );
        }

        return $tabs;
    }

    function print_course_header($course) {
        global $DB, $CFG;

        $html = '';
        $context = context_course::instance($course->id);
        $image_url = "$CFG->wwwroot/course/format/isf/pix/tbg.png";

        $fs = get_file_storage();
        $imgs = $fs->get_area_files($context->id, 'format_isf', 'wallimage', $course->id);
        foreach ($imgs as $file) {
            $filename = $file->get_filename();
            $filetype = $file->get_mimetype();
            if ($filename == '.' or !$filetype) continue;
            $url = moodle_url::make_pluginfile_url($context->id, 'format_isf', 'wallimage', $course->id, '/', $filename);
            $image_url = $url->out();
        }


        $link = html_writer::tag('a',
            html_writer::tag('i', '', array('class' => 'fa fa-eye-slash')) . ' '. get_string('hideimage', 'format_isf'), array(
                'href' => '#',
                'id' => 'close-img',
                'class' => 'close-course-img',
            )
        );

        $img = html_writer::tag('div',
            html_writer::img($image_url, '') . $link,
            array('class' => 'image'.((!$this->_user_preferences['show_wallimage']) ? ' hidden' : ''))
        );

        $link = html_writer::tag('a',
            html_writer::tag('i', '', array('class' => 'fa fa-eye')) . ' ' . get_string('showimage', 'format_isf'), array(
                'href' => '#',
                'id' => 'show-img',
                'class' => 'show-course-img'.(($this->_user_preferences['show_wallimage']) ? ' hidden' : ''),
            ));

        $html = html_writer::tag('div', html_writer::tag('div',
            html_writer::tag('div', $img . $link, array('class' => 'col-lg-12')),
            array('class' => 'clearfix')),
            array('class' => 'course-img'));

        $coursename = course_get_format($course)->get_course_name();
        $html .= html_writer::tag('h2', $coursename, array('class'=>'course-title'));

        return $html;
    }

    function print_course_tabs($course, $displaysection, $display = 'all') {

        $course_tabs = $this->get_course_tabs($course, $displaysection, $display);
        $html = '';

        if ($display != 'content'){
            $li = '';
            foreach($course_tabs as $tab) {
                $li .= html_writer::tag('li',
                    $this->a(
                        $tab['title'],
                        (($tab['type'] == 'tab') ? '#' : '')."{$tab['href']}",
                        'nav-link tab-' . ($tab['active'] ? ' active' : ''),
                        array('data-type'=>$tab['type'], 'data-link'=>$tab['link'])
                    ),
                    array('class' => 'nav-item')
                );
            }

            $html .= html_writer::tag('ul', $li, array(
                'class' => 'nav nav nav-tabs',
                'role' => 'tablist'
            ));
        }

        if ($display != 'tabs'){
            $nav_items = '';
            foreach($course_tabs as $tab) {
                $nav_items .= html_writer::tag('div', $tab['content'], array(
                    'class' => 'tab-pane ' . $tab['classes'] . ($tab['active'] ? ' active' : ''),
                    'id' => $tab['href']
                ));
            }

            $html .= html_writer::start_tag('div', array('class' => "tab-content"));
            $html .= $nav_items;
            $html .= html_writer::end_tag('div');
        }
        return $html;
    }

    /**
     * Output the html for a single section page .
     *
     * @param stdClass $course The course entry from DB
     * @param array $sections (argument not used)
     * @param array $mods (argument not used)
     * @param array $modnames (argument not used)
     * @param array $modnamesused (argument not used)
     * @param int $displaysection The section number in the course which is being displayed
     */
    public function print_single_section_page($course, $sections, $mods, $modnames, $modnamesused, $displaysection) {
        global $PAGE;

        $output = '';

        $modinfo = get_fast_modinfo($course);
        $course = course_get_format($course)->get_course();

        // Can we view the section in question?
        if (!($sectioninfo = $modinfo->get_section_info($displaysection))) {
            // This section doesn't exist
            print_error('unknowncoursesection', 'error', null, $course->fullname);
            return;
        }

        if (!$sectioninfo->uservisible) {
            if (!$course->hiddensections) {
                $output .= $this->start_section_list();
                $output .= $this->section_hidden($displaysection, $course->id);
                $output .= $this->end_section_list();
            }
            // Can't view this section.
            return $output;
        }

        // course filter
        $output .= $this->course_filter($course, $modinfo);

        // The requested section page.
        $thissection = $modinfo->get_section_info($displaysection);

        // Now the list of sections..
        $output .= $this->start_section_list();

        // Show completion help icon.
        $completioninfo = new completion_info($course);
        $output .= $completioninfo->display_help_icon();

        $output .= $this->section_header($thissection, $course, false, $displaysection);

        $output .= $this->courserenderer->course_section_cm_list($course, $thissection, $displaysection);
        $output .= $this->courserenderer->course_section_add_cm_control($course, $displaysection, $displaysection);
        $output .= $this->section_footer();
        $output .= $this->end_section_list();

        return $output;
    }

    /**
     * Output the html for a multiple section page
     *
     * @param stdClass $course The course entry from DB
     * @param array $sections (argument not used)
     * @param array $mods (argument not used)
     * @param array $modnames (argument not used)
     * @param array $modnamesused (argument not used)
     */
    public function print_multiple_section_page($course, $sections, $mods, $modnames, $modnamesused) {
        global $PAGE;

        $output = '';

        $modinfo = get_fast_modinfo($course);
        $course = course_get_format($course)->get_course();

        $context = context_course::instance($course->id);
        // Title with completion help icon.
        $completioninfo = new completion_info($course);
        $output .= $completioninfo->display_help_icon();
        $output .= $this->output->heading($this->page_title(), 2, 'accesshide');

        // course filter
        $output .= $this->course_filter($course, $modinfo);

        // Copy activity clipboard..
        $output .= $this->course_activity_clipboard($course, 0);

        // Now the list of sections..
        $output .= $this->start_section_list();

        foreach ($modinfo->get_section_info_all() as $section => $thissection) {
            if ($section == 0) {
                // 0-section is displayed a little different then the others
                if ($thissection->summary or !empty($modinfo->sections[0]) or $PAGE->user_is_editing()) {
                    $output .= $this->section_header($thissection, $course, false, 0);
                    $output .= $this->courserenderer->course_section_cm_list($course, $thissection, 0);
                    $output .= $this->courserenderer->course_section_add_cm_control($course, 0, 0);
                    $output .= $this->section_footer();
                }
                continue;
            }
            if ($section > $course->numsections) {
                // activities inside this section are 'orphaned', this section will be printed as 'stealth' below
                continue;
            }
            // Show the section if the user is permitted to access it, OR if it's not available
            // but there is some available info text which explains the reason & should display.
            $showsection = $thissection->uservisible ||
                    ($thissection->visible && !$thissection->available &&
                    !empty($thissection->availableinfo));
            if (!$showsection) {
                // If the hiddensections option is set to 'show hidden sections in collapsed
                // form', then display the hidden section message - UNLESS the section is
                // hidden by the availability system, which is set to hide the reason.
                if (!$course->hiddensections && $thissection->available) {
                    $output .= $this->section_hidden($section, $course->id);
                }

                continue;
            }

            if (!$PAGE->user_is_editing() && $course->coursedisplay == COURSE_DISPLAY_MULTIPAGE) {
                // Display section summary only.
                $output .= $this->section_summary($thissection, $course, null);
            } else {
                $output .= $this->section_header($thissection, $course, false, 0);
                if ($thissection->uservisible) {
                    $output .= $this->courserenderer->course_section_cm_list($course, $thissection, 0);
                    $output .= $this->courserenderer->course_section_add_cm_control($course, $section, 0);
                }
                $output .= $this->section_footer();
            }
        }

        if ($PAGE->user_is_editing() and has_capability('moodle/course:update', $context)) {
            // Print stealth sections if present.
            foreach ($modinfo->get_section_info_all() as $section => $thissection) {
                if ($section <= $course->numsections or empty($modinfo->sections[$section])) {
                    // this is not stealth section or it is empty
                    continue;
                }
                $output .= $this->stealth_section_header($section);
                $output .= $this->courserenderer->course_section_cm_list($course, $thissection, 0);
                $output .= $this->stealth_section_footer();
            }

            $output .= $this->end_section_list();

            $output .= html_writer::start_tag('div', array('id' => 'changenumsections', 'class' => 'mdl-right'));

            // Increase number of sections.
            $straddsection = get_string('increasesections', 'moodle');
            $icon = $this->output->pix_icon('t/switch_plus', $straddsection);
            $output .= html_writer::link('javascript:void(0)', $icon.get_accesshide($straddsection), array('class' => 'create-sections'));

            $output .= html_writer::end_tag('div');
        } else {
            $output .= $this->end_section_list();
        }

        return $output;
    }

    public function course_filter($course, $modinfo) {
        global $PAGE, $USER, $DB;
        $o = '';

        $o .= html_writer::start_div('activity-filter clearfix');

        $o .= html_writer::start_div('div', array('class'=>'section-filter-labels'));

        // favourite filter
        $o .= html_writer::start_tag('label');
            $o .= html_writer::empty_tag('input', array('type'=>'checkbox', 'id'=>'filterstarfav'));
            $o .= get_string('favorite', 'format_isf');
        $o .= html_writer::end_tag('label');

        // activities filter
        /*$o .= html_writer::start_tag('label');
            $o .= html_writer::empty_tag('input', array('type'=>'checkbox', 'id'=>'filteract'));
            $o .= get_string('activities', 'format_isf');
        $o .= html_writer::end_tag('label');*/

        // activities filter
        /*$o .= html_writer::start_tag('label');
            $o .= html_writer::empty_tag('input', array('type'=>'checkbox', 'id'=>'filterres'));
            $o .= get_string('resources', 'format_isf');
        $o .= html_writer::end_tag('label');*/

        $o .= html_writer::end_div();

        // filter dropdown

        $o .= html_writer::start_div('div', array('class'=>'section-filter-wrapper'));

        $modnames = get_module_types_names();
        $modules = get_module_metadata($course, $modnames, null);
        $o .= html_writer::select(array('-1'=>get_string('viewall', 'format_isf')) + $modnames, 'mod_filter', '', array(), array('id'=>'mod_filter', 'class'=>'rtype'));

        // search input
        $o .= html_writer::start_div('div', array('class'=>'filter-search form-inline'));
            $o .= html_writer::empty_tag('input', array('type'=>'text', 'class'=>'form-control', 'id'=>'filtersearch', 'placeholder'=>get_string('searchlearningmodules', 'format_isf')));
            $o .= html_writer::tag('i', '', array('class'=>'fa fa-search'));
        $o .= html_writer::end_div('div');

        // toggle all sections
        $o .= html_writer::tag('i', '', array('class'=>'sections-toggler fa fa-angle-down', 'title'=>get_string('toggleallsection', 'format_isf')));

        // toggle course view
        if (isset($course->coursetype) and $course->coursetype){
            $o .= html_writer::start_tag('span', array('class'=>'view-toggler'));
                $o .= html_writer::tag('i', '', array('class'=>'fa fa-th-large'.(($this->_user_preferences['course_view'] == 'grid-view') ? ' hidden' : ''), 'title'=>get_string('gridview', 'format_isf')));
                $o .= html_writer::tag('i', '', array('class'=>'fa fa-th-list'.(($this->_user_preferences['course_view'] == 'list-view') ? ' hidden' : ''), 'title'=>get_string('listview', 'format_isf')));
            $o .= html_writer::end_tag('span');
        }

        // toggle course view
        if ($PAGE->user_is_editing()){
            $o .= html_writer::start_tag('span', array('class'=>'create-sections'));
                $o .= html_writer::tag('i', '', array('class'=>'fa fa-plus-circle', 'title'=>get_string('createsection', 'format_isf')));
            $o .= html_writer::end_tag('span');
            $PAGE->requires->js_call_amd('format_isf/createsection', 'init', array('courseid'=>$course->id, 'formid'=>'create_section_formbox'));
            $o .= $this->createSectionForm($course);
        }

        $o .= html_writer::end_div();

        $o .= html_writer::end_div();

        return $o;
    }

    protected function render_format_isf_course_content_header(format_isf_course_content_header $a) {
        global $PAGE, $USER, $DB, $CFG;

        if (!$this->page->url->compare(new moodle_url('/course/view.php'), URL_MATCH_BASE) and !stristr($PAGE->url, '/grade/')){
            return '';
        }

        $o = '';
        $section     = optional_param('section', 0, PARAM_INT);

        $course = $this->page->course;
        $course = course_get_format($course)->get_course();

        // quick modules form
        require_once($CFG->dirroot . '/local/quickadd/renderer.php');
        $renderer = $PAGE->get_renderer('local_quickadd');
        $o .= $renderer->modalForm($course);

        $o .= $this->print_course_header($course);
        $o .= $this->print_course_tabs($course, $section, 'tabs');

        return $o;
    }

    protected function render_format_isf_course_content_footer(format_isf_course_content_footer $a) {
        global $PAGE, $USER, $DB;

        $o = '';

        return $o;
    }

    /**
     * Generate the display of the header part of a section before
     * course modules are included
     *
     * @param stdClass $section The course_section entry from DB
     * @param stdClass $course The course entry from DB
     * @param bool $onsectionpage true if being printed on a single-section page
     * @param int $sectionreturn The section to return to after an action
     * @return string HTML to output.
     */
    protected function section_header($section, $course, $onsectionpage, $sectionreturn=null) {
        global $PAGE;

        $o = '';
        $currenttext = '';
        $sectionstyle = '';

        if ($section->section != 0) {
            // Only in the non-general sections.
            if (!$section->visible) {
                $sectionstyle = ' hidden';
            } else if (course_get_format($course)->is_section_current($section)) {
                $sectionstyle = ' current';
            }
        }

        if (is_array($this->_user_preferences['toggled_sections']) and in_array($section->id, $this->_user_preferences['toggled_sections'])){
            $sectionstyle .= ' toggled';
        }

        $o.= html_writer::start_tag('li', array('id' => 'section-'.$section->section,
            'class' => 'section main clearfix'.$sectionstyle, 'role'=>'region',
            'aria-label'=> get_section_name($course, $section),
            'data-section-id' => $section->id));

        if ($PAGE->user_is_editing() and \local_isfdup\hooks::can_duplicate($section->course)) {
            $o.= \local_isfdup\hooks::section_modals($section);
        }

        // Create a span that contains the section title to be used to create the keyboard section move menu.
        $o .= html_writer::tag('span', get_section_name($course, $section), array('class' => 'hidden sectionname'));

        $leftcontent = $this->section_left_content($section, $course, $onsectionpage);
        $o.= html_writer::tag('div', $leftcontent, array('class' => 'left side'));

        $rightcontent = $this->section_right_content($section, $course, $onsectionpage);
        $o.= html_writer::tag('div', $rightcontent, array('class' => 'right side'));
        $o.= html_writer::start_tag('div', array('class' => 'content'));

        // When not on a section page, we display the section titles except the general section if null
        $hasnamenotsecpg = (!$onsectionpage && ($section->section != 0 || !is_null($section->name)));

        // When on a section page, we only display the general section title, if title is not the default one
        $hasnamesecpg = ($onsectionpage && ($section->section == 0 && !is_null($section->name)));

        $classes = ' accesshide';
        if ($hasnamenotsecpg || $hasnamesecpg) {
            $classes = '';
        }
        $sectionname = html_writer::tag('span', $this->section_title($section, $course));
        $sectionname .= html_writer::tag('i', '', array('class'=>'section-toggler fa fa-angle-down', 'title'=>get_string('togglesection', 'format_isf')));
        $o.= $this->output->heading($sectionname, 3, 'sectionname' . $classes);

        $o.= html_writer::start_tag('div', array('class' => 'summary'));
        $o.= $this->format_summary_text($section);
        $o.= html_writer::end_tag('div');

        $context = context_course::instance($course->id);
        $o .= $this->section_availability_message($section,
                has_capability('moodle/course:viewhiddensections', $context));

        return $o;
    }

    public function get_course_formatted_summary($course, $options = array()) {
        global $CFG;

        require_once($CFG->libdir. '/filelib.php');
        if (!$course->summary) {
            return '';
        }
        $options = (array)$options;
        $context = context_course::instance($course->id);

        $summary = file_rewrite_pluginfile_urls($course->summary, 'pluginfile.php', $context->id, 'course', 'summary', null);
        $summary = format_text($summary, $course->summaryformat, $options, $course->id);

        return $summary;
    }

    public function createSectionForm($course) {
        global $CFG;

        $output = '';

        $sections = array();
        $modinfo = get_fast_modinfo($course);
        foreach ($modinfo->get_section_info_all() as $section => $thissection) {
            if ($thissection->section == 0) continue;
            if ($thissection->section > $course->numsections) continue;
            $sections[] = array('sid'=>$thissection->section, 'sname' => (($thissection->name) ? $thissection->name : get_string('sectionname', 'format_isf').' '.$thissection->section));
        }
        $context = array(
            'formaction' => new moodle_url('/course/format/isf/ajax.php', array('action'=>'create_section')),
            'sections' => $sections,
            'courseid' => $course->id
        );
        if (count($sections)) {
            $context['issections'] = true;
        }
        return $this->render_from_template('format_isf/createsection_form', $context);
    }

}
