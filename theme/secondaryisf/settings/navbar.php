<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version details
 *
 * @package    theme_secondaryisf
 * @copyright 2015 Jeremy Hopkins (Coventry University)
 * @copyright 2015 Fernando Acedo (3-bits.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

// Variable setting
$choices1to12 = array();
for ($i = 0; $i < 13; $i++) {
    $choices1to12[$i] = $i;
}

// Settings for tools menus.

$temp = new admin_settingpage('theme_secondaryisf_header_navbar_menu', get_string('navbarmenusettings', 'theme_secondaryisf'));

$temp->add(new admin_setting_heading('theme_secondaryisf_toolsmenu', get_string('toolsmenu', 'theme_secondaryisf'),
    format_text(get_string('toolsmenudesc', 'theme_secondaryisf'), FORMAT_MARKDOWN)));

$temp->add(new admin_setting_heading('theme_secondaryisf_toolsmenu', get_string('toolsmenuheading', 'theme_secondaryisf'),
format_text(get_string('toolsmenuheadingdesc', 'theme_secondaryisf'), FORMAT_MARKDOWN)));

$name = 'theme_secondaryisf/disablecustommenu';
$title = get_string('disablecustommenu', 'theme_secondaryisf');
$description = get_string('disablecustommenudesc', 'theme_secondaryisf');
$setting = new admin_setting_configcheckbox($name, $title, $description, false, true, false);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

$name = 'theme_secondaryisf/enabletoolsmenus';
$title = get_string('enabletoolsmenus', 'theme_secondaryisf');
$description = get_string('enabletoolsmenusdesc', 'theme_secondaryisf');
$default = true;
$setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Number of tools menus.
$name = 'theme_secondaryisf/toolsmenuscount';
$title = get_string('toolsmenuscount', 'theme_secondaryisf');
$description = get_string('toolsmenuscountdesc', 'theme_secondaryisf');
$default = THEME_secondaryisf_DEFAULT_TOOLSMENUSCOUNT;
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices1to12);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// If we don't have a menuscount yet, default to the preset.
$toolsmenuscount = get_config('theme_secondaryisf', 'toolsmenuscount');
if (!$toolsmenuscount) {
    $toolsmenuscount = THEME_secondaryisf_DEFAULT_TOOLSMENUSCOUNT;
}

for ($toolsmenusindex = 1; $toolsmenusindex <= $toolsmenuscount; $toolsmenusindex ++) {
    $temp->add(new admin_setting_heading('theme_secondaryisf_menus' . $toolsmenusindex,
    get_string('toolsmenuheading', 'theme_secondaryisf') . $toolsmenusindex,
    format_text(get_string('toolsmenudesc', 'theme_secondaryisf'), FORMAT_MARKDOWN)));

    $name = 'theme_secondaryisf/toolsmenu' . $toolsmenusindex . 'title';
    $title = get_string('toolsmenutitle', 'theme_secondaryisf') . ' ' . $toolsmenusindex;
    $description = get_string('toolsmenutitledesc', 'theme_secondaryisf');
    $default = get_string('toolsmenutitledefault', 'theme_secondaryisf');
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_RAW);
    $temp->add($setting);

    $name = 'theme_secondaryisf/toolsmenu' . $toolsmenusindex . 'chinesetitle';
    $title = get_string('toolsmenuchinesetitle', 'theme_secondaryisf') . ' ' . $toolsmenusindex;
    $description = get_string('toolschinesemenutitledesc', 'theme_secondaryisf');
    $default = get_string('toolsmenuchinesetitledefault', 'theme_secondaryisf');
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_RAW);
    $temp->add($setting);

    $name = 'theme_secondaryisf/toolsmenu' . $toolsmenusindex;
    $title = get_string('toolsmenu', 'theme_secondaryisf') . ' ' . $toolsmenusindex;
    $description = get_string('toolsmenudesc', 'theme_secondaryisf');
    $setting = new admin_setting_configtextarea($name, $title, $description, '', PARAM_RAW, '50', '10');
    $temp->add($setting);

    $name = 'theme_secondaryisf/toolsmenu' . $toolsmenusindex . 'chinesemenu';
    $title = get_string('toolschinesemenu', 'theme_secondaryisf') . ' ' . $toolsmenusindex;
    $description = get_string('toolschinesemenudesc', 'theme_secondaryisf');
    $setting = new admin_setting_configtextarea($name, $title, $description, '', PARAM_RAW, '50', '10');
    $temp->add($setting);

    $name = 'theme_secondaryisf/toolsmenu' . $toolsmenusindex . 'field';
    $title = get_string('toolsmenufield', 'theme_secondaryisf');
    $description = get_string('toolsmenufielddesc', 'theme_secondaryisf');
    $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_RAW);
    $temp->add($setting);
}
