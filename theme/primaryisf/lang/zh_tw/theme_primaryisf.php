<?php

$string['advancedsettings'] = '進階設定';
$string['brandcolor'] = '名牌顏色';
$string['brandcolor_desc'] = '強調色';
$string['choosereadme'] = '弘立書院小學部是一個先進而個人化的主題，這個主題不但可以被直接使用，亦可以在使用Bootstrap 4創建新主題時作為一個父主題而使用。';
$string['currentinparentheses'] = '(當前)';
$string['configtitle'] = '弘立書院小學部';
$string['generalsettings'] = '一般設定';
$string['pluginname'] = '弘立書院小學部';
$string['presetfiles'] = '附加主題預設文件';
$string['presetfiles_desc'] = '';
$string['preset'] = '預設主題';
$string['preset_desc'] = '請選擇一個預設以廣泛地改變主題的外觀。';
$string['rawscss'] = 'Raw SCSS';
$string['rawscss_desc'] = '請用這個範圍來提供一個會放入在樣式表（style sheet）尾部的SCSS或CSS代碼。';
$string['rawscsspre'] = 'Raw initial SCSS';
$string['rawscsspre_desc'] = '你可以在這個範圍內提供初始化的SCSS代碼，它將會被最先放入。你會在大部分時間使用這個設定來界定變數。';
$string['region-side-pre'] = '右';
$string['dashboard'] = '儀表板';
$string['calendar'] = '月曆';
$string['settings'] = '設定';
$string['courses'] = '課程';
$string['manage'] = '管理';
$string['users'] = '用戶';
$string['categories'] = '分類';
$string['courses'] = '課程';
$string['manageappearance'] = '外觀';
$string['coursesandcurriculum'] = '課程';
$string['sitesettings'] = '網站設定';
$string['appearance'] = '外觀';
$string['features'] = '特徵';
$string['languages'] = '語言';
$string['add'] = '增加';
$string['sidebarsettings'] = '側邊列設定';
$string['quickmodules'] = '快捷模組（Quick Modules）';

// Settings

$string['navbarmenusettings'] = '弘立書院小學部目錄';
$string['newstickerenabled'] = '啟用字幕跑馬燈';
$string['newstickerenableddesc'] = '在你主頁的頂部啟用一個字幕跑馬燈';

// ISF Menus
$string['enabletoolsmenus'] = '啟用弘立書院小學部目錄';
$string['enabletoolsmenusdesc'] = '如果你不想顯示目錄，你可以任它保持在關閉的狀態。';

$string['disablecustommenu'] = '停用Moodle個人化目錄';
$string['disablecustommenudesc'] = '停用在導航欄上的Moodle個人化目錄（仍然會顯示其它你已安裝的主題）';

$string['toolsmenuheading'] = '弘立書院目錄（在主導航）';
$string['toolsmenuheadingdesc'] = '你可以在弘立書院小學部目錄中配置一些連結（在主導航欄上）。請注意
那裏同時有中文版面及英文版面。
這與Moodle個人化目錄的格式很相似，但這個允許你新增fa頭像到目錄中：
<pre>
"&lt;span class=&quot;fa fa-youtube-play&quot;&gt;&lt;/span&gt; YouTube|http://www.youtube.com"" target=""_blank|YouTube"
"&lt;span class=&quot;fa-area-chart&quot;&gt;&lt;/span&gt; Tableau Public|http://www.tableau-public.com"" target=""_blank|Tableau Public"
"&lt;span class=&quot;fa-wordpress&quot;&gt;&lt;/span&gt; WordPress|http://www.wordpress.com"" target=""_blank|WordPress"
</pre><br />';

$string['toolsmenuscount'] = '弘立書院小學部目錄的數量';
$string['toolsmenuscountdesc'] = '設定你想要加入主導航欄的弘立書院小學部目錄數量';

$string['toolsmenuheading'] = '弘立書院小學部目錄 ';
$string['toolsmenu'] = '英文下拉目錄';
$string['toolsmenudesc'] = '將一個英文下拉目錄增加至主導航欄';
$string['toolschinesemenu'] = '中文下拉目錄';
$string['toolschinesemenudesc'] = '將一個中文下拉目錄增加至主導航欄';

$string['toolsmenutitle'] = '英文標題';
$string['toolsmenuchinesetitle'] = '中文標題';
$string['toolsmenutitledefault'] = '標題';
$string['toolsmenuchinesetitledefault'] = '標題';
$string['toolsmenutitledesc'] = '在主導航欄上新增一個你想要顯示的小學部目錄英文標題';
$string['toolschinesemenutitledesc'] = '在主導航欄上新增一個你想要顯示的小學部目錄中文標題';

$string['toolsmenulabel'] = '弘立學校小學部目錄';

$string['toolsmenufield'] = '用戶類型（可選的）';
$string['toolsmenufielddesc'] = '例如：老師、學生、家長、辦公室、管理員（只需要列表中的一個項目，沒有空格，全部要小寫）';
$string['togglefilemanager'] = '切換文件管理器';
$string['activitypicker'] = '活動選擇器';
$string['activityimages'] = '活動圖片';
$string['coursenavigation'] = '課程導航';
$string['add'] = '新增';
$string['addquickbeforemodule'] = '新增之前的快捷模組（Quick Module）';
$string['addquickmodule'] = '新增新的快捷模組（Quick Module）';
$string['addbeforemodule'] = '新增之前的模組（Module）';
$string['savechanges_next_user'] = '儲存更改及下一個用戶';


// Slider

$string['frontpageslidersettings'] = '弘立書院中學部圓盤傳送帶';

$string['slideshowsettings'] = '幻燈片';
$string['slideshowsettingsheading'] = '在標題頁自訂圓盤傳送帶，請確保你用的圖片大小是1500px * 300px';
$string['slideshowdesc'] = '上傳圖片（1500px * 300px），並為標題頁的圓盤傳送帶增加連結和描述';

$string['sliderimage'] = '幻燈片圖片';
$string['sliderimagedesc'] = '為你的幻燈片新增一張圖片。';

$string['slidercaption'] = '幻燈片字幕';
$string['slidercaptiondesc'] = '為你的幻燈片新增字幕，請看以下的例子';

$string['sliderurl'] = '幻燈片URL地址';
$string['sliderurldesc'] = '新增一個能連去你的幻燈片URL地址';

$string['sliderenabled'] = '啟用幻燈片';
$string['sliderenableddesc'] = '啟用主頁置頂的幻燈片';

$string['slidercount'] = '幻燈片區塊數量';
$string['slidercountdesc'] = '選擇你想要增加的幻燈片區塊，然後 <strong>按儲存</strong> 來加載輸入範圍';

$string['slidercohorts'] = '群組';
$string['slidercohortsdesc'] = '向已選群組顯示幻燈片';

$string['slideroption2snippet'] = '<p>幻燈片字幕的HTML樣本：</p>
<pre>
&#x3C;div class=&#x22;span6 col-sm-6&#x22;&#x3E;
&#x3C;h3&#x3E;Hand-crafted&#x3C;/h3&#x3E; &#x3C;h4&#x3E;pixels and code for the Moodle community&#x3C;/h4&#x3E;
&#x3C;a href=&#x22;#&#x22; class=&#x22;submit&#x22;&#x3E;Please favorite our theme!&#x3C;/a&#x3E;
</pre>';

// Cache definitions.
$string['cachedef_userdata'] = '一個用作儲存用戶特殊數據的會話緩存';


$string['showall'] = '顯示所有選項';
$string['showvasic'] = '顯示基本選項';
$string['showbasic'] = '顯示基本選項';
$string['addlearningitem'] = '新增一個學習項目';
$string['mastercourses'] = '主要課程';
$string['yes_delete_from_moodle'] = '是的，只從Moodle刪除';
$string['yes_delete_from_moodle_and_ps'] = '是的，從Moodle及Powerschool刪除';
$string['region-side-course'] = '課程導航';
$string['addablock'] = '新增一個區塊';
$string['next'] = '下一個';
$string['previous'] = '上一個';
$string['prev'] = '上一個';
$string['preview'] = '檢閱';
