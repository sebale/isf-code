<?php
// Moodle
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// ISF Primary is built as a theme for Moodle.

/**
 * ISF Primary
 *
 *
 * @package    	theme_primaryisf
 * @copyright  	2017 ISF
 * @license    	http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @created by	ISF
 */

defined('MOODLE_INTERNAL') || die();

// 
// $capabilities = array(
//     'theme/primaryisf:is_guest' => array(
//         'captype' => 'read',
//         'contextlevel' => CONTEXT_SYSTEM,
//         'archetypes' => array(
//             'guest' => CAP_ALLOW
//         )
//     ),
//     'theme/primaryisf:is_user' => array(
//         'captype' => 'read',
//         'contextlevel' => CONTEXT_SYSTEM,
//         'archetypes' => array(
//             'user' => CAP_ALLOW
//         )
//     ),
//     'theme/primaryisf:is_student' => array(
//         'captype' => 'read',
//         'contextlevel' => CONTEXT_SYSTEM,
//         'archetypes' => array(
//             'student' => CAP_ALLOW
//         )
//     ),
//     'theme/primaryisf:is_parent' => array(
//         'captype' => 'read',
//         'contextlevel' => CONTEXT_SYSTEM,
//         'archetypes' => array(
//             'parent' => CAP_ALLOW
//         )
//     ),
//     'theme/primaryisf:is_teacher' => array(
//         'captype' => 'read',
//         'contextlevel' => CONTEXT_SYSTEM,
//         'archetypes' => array(
//             'teacher' => CAP_ALLOW
//         )
//     ),
//     'theme/primaryisf:is_manager' => array(
//         'captype' => 'read',
//         'contextlevel' => CONTEXT_SYSTEM,
//         'archetypes' => array(
//             'manager' => CAP_ALLOW
//         )
//     ),
//     'theme/primaryisf:is_coursecreator' => array(
//         'captype' => 'read',
//         'contextlevel' => CONTEXT_SYSTEM,
//         'archetypes' => array(
//             'coursecreator' => CAP_ALLOW
//         )
//     ),
//     'theme/primaryisf:is_admin' => array(
//         'captype' => 'write',
//         'contextlevel' => CONTEXT_SYSTEM,
//         'archetypes' => array(
//             'admin' => CAP_ALLOW
//         )
//     ),
//     'theme/primaryisf:is_superadmin' => array(
//         'captype' => 'write',
//         'contextlevel' => CONTEXT_SYSTEM,
//         'archetypes' => array(
//             'superadmin' => CAP_ALLOW
//         )
//     )
// );
