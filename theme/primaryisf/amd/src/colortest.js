// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is an empty module, that is required before all other modules.
 * Because every module is returned from a request for any other module, this
 * forces the loading of all modules with a single request.
 *
 * This function also sets up the listeners for ajax requests so we can tell
 * if any requests are still in progress.
 *
 * @package    theme_primaryisf
 * @module     theme_primaryisf/colortest
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define(['jquery', 'theme_primaryisf/colorpicker', 'theme_primaryisf/select2', 'theme_primaryisf/moment', 'theme_primaryisf/fullcalendar', 'theme_primaryisf/classyeditor', 'theme_primaryisf/dtpicker', 'theme_primaryisf/circliful', 'theme_primaryisf/rating'],
	function($, Colorpicker, select2, moment, calendar, editor, datePicker, circliful, rating) {

    m = moment.init();
    Colorpicker.init($);
    select2.init($);
  	calendar.init($, m);
  	editor.init($);
    datePicker.init($, m);
    circliful.init($);
    rating.init($);

    return {
        init: function () {
        	$('.card-block').append('<input type="color" id="testInput"/>');

            $('#testInput').ColorPicker({
							color: '#0000ff',
							onShow: function (colpkr) {
								$(colpkr).fadeIn(500);
								return false;
							},
							onHide: function (colpkr) {
								$(colpkr).fadeOut(500);
								return false;
							},
							onChange: function (hsb, hex, rgb) {
								$('#testInput div').css('backgroundColor', '#' + hex);
							}
						});

						$('.card-block').append('<select class="js-states-test form-control">\
						  <optgroup label="Alaskan/Hawaiian Time Zone">\
						    <option value="AK">Alaska</option>\
						    <option value="HI">Hawaii</option>\
						  </optgroup>\
						  <optgroup label="Pacific Time Zone">\
						    <option value="CA">California</option>\
						    <option value="NV">Nevada</option>\
						    <option value="OR">Oregon</option>\
						    <option value="WA">Washington</option>\
						  </optgroup>\
						</select>');

						$('.js-states-test').select2();

						$('.card-block').append("<div id='calendar'></div>");

						$('#calendar').fullCalendar({
				    	    // put your options and callbacks here
					    });

						// editor
						$('.card-block').append('<textarea class="classy-editor"></textarea>');

						$(".classy-editor").Editor();


						$('.card-block').append('<div class="container">\
					    <div class="row">\
					        <div class="col-sm-6">\
					            <input type="text" class="form-control" id="datetimepicker4" />\
					        </div>\
					    </div>\
						</div>');

						$('#datetimepicker4').datetimepicker();

						$('.card-block').append('<div class="row">\
						    <div class="col-lg-2">\
						        <div id="test-circle"></div>\
						    </div>\
						</div>');

						$("#test-circle").circliful({
			                animationStep: 5,
			                foregroundBorderWidth: 5,
			                backgroundBorderWidth: 15,
			                percent: 75
			            });

			            $('.card-block').append('<div class="star-rating course-star-rating" id="course_star_rating">\
			            	<input type="radio" name="rate" class="rating" value="1" checked="checked" />\
			            	<input type="radio" name="rate" class="rating" value="2" />\
			            	<input type="radio" name="rate" class="rating" value="3" />\
			            	<input type="radio" name="rate" class="rating" value="4" />\
			            	<input type="radio" name="rate" class="rating" value="5" />\
			            </div>');

						$("#course_star_rating").rating(function(vote, event){
						    $.ajax({
						        url: "http://tenant1.talentquest.sebale.net/course/format/talentquest/ajax.php?action=set-rating&id=10",
						        type: "GET",
						        data: {rate: vote}
						    });
						});
      	}
  	};

});
