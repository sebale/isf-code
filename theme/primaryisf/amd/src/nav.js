// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Contain the logic for a main nav and sidebar top-level actions
 *
 * @package    theme_primaryisf
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
define(['jquery', 'core/log'],
     function($, Log) {

    var SELECTORS = {
        TOGGLE_REGION: '#nav-drawer .sidebar',
        TOGGLE_ITEM: '#nav-drawer .sidebar-block',
        TOGGLE_ACTION: '#nav-drawer .flat-nav a[data-key]',
        BODY: 'body'
    };

    /**
     * Constructor for the Nav.
     *
     * @param {object} root The root jQuery element for the modal
     */
    var Nav = function() {

        if (!$(SELECTORS.TOGGLE_REGION).length) {
            Log.debug('Page is missing a navigation region');
        }
        if (!$(SELECTORS.TOGGLE_ACTION).length) {
            Log.debug('Page is missing a navigation toggle link');
        }
        
        this.registerEventListeners();
        
        var small = $(document).width() < 768;
        if (small) {
            this.hideSidebar();
        }
    };

    /**
     * hide sidebar with all elements
     *
     * @method hideSidebar
     */
    Nav.prototype.hideSidebar = function() {
      $(SELECTORS.BODY).removeClass('nav-with-panels');
      $(SELECTORS.TOGGLE_ACTION).removeClass('active');
      $(SELECTORS.TOGGLE_REGION + ', ' + SELECTORS.TOGGLE_ITEM).css({'display': 'none', 'opacity': 0});
    }

    /**
     * show navigation sidebar
     *
     * @method showSidebar
     * @param {string} box The name of sidebar box which will be displayed
     */
    Nav.prototype.showSidebar = function(box) {
      this.hideSidebar();
      $(SELECTORS.BODY).addClass('nav-with-panels');
      $(SELECTORS.TOGGLE_REGION + ', ' + SELECTORS.TOGGLE_ITEM + '[role="block-' + box + '"]').css({'display': 'block', 'opacity': 1});
    }


    /**
     * Set up all of the event handling for the navigation.
     *
     * @method registerEventListeners
     */
    Nav.prototype.registerEventListeners = function() {
      $(SELECTORS.TOGGLE_ACTION).click(function(event){
        var elem = $(event.target).is('i') ? $(event.target).parent() : $(event.target);
        var key = elem.attr('href');
        
        if (elem.attr('href')[0] === '#') {
          
          // if current opened - close panel
          if(elem.hasClass('active')) {
            this.hideSidebar();
            return false;
          }
          this.showSidebar(elem.attr('href').slice(1));
          elem.addClass('active');
        } else {
          this.hideSidebar();
        }
      }.bind(this));

      /* hide panel when click outsite */
      $(SELECTORS.BODY).click(function(event){
        var elem = $(event.target);
        if( elem.is('button[data-action="toggle-drawer"]') || elem.closest('button[data-action="toggle-drawer"]').length ) {
          this.hideSidebar();
        }
      }.bind(this));
    };

    return {
        'init': function() {
            return new Nav();
        }
    };
});
