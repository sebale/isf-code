// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is an empty module, that is required before all other modules.
 * Because every module is returned from a request for any other module, this
 * forces the loading of all modules with a single request.
 *
 * This function also sets up the listeners for ajax requests so we can tell
 * if any requests are still in progress.
 *
 * @package    theme_primaryisf
 * @module     theme_primaryisf/colorinit
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define(['jquery', 'theme_primaryisf/colorpicker'], function($, Colorpicker) {

    Colorpicker.init($);

    return {
        init: function () {

            if ($('input.ColorPicker').length){
                $('input.ColorPicker').each(function(e){
                    var color = $(this).val();
                    if (typeof $(this).attr('placeholder') !== typeof undefined && $(this).attr('placeholder') !== false && !color.length){
                        var color = $(this).attr('placeholder');
                    }
                    var obj = $(this);
                    $(this).ColorPicker({
                        onChange: function (hsb, hex, rgb) {
                            obj.val(hex);
                        },
                        onBeforeShow: function () {
                            if (color.length){
                                obj.ColorPickerSetColor(color);
                            }
                        },
                        onShow: function (colpkr) {
                            $(colpkr).fadeIn(500);
                            return false;
                        },
                        onHide: function (colpkr) {
                            $(colpkr).fadeOut(500);
                            return false;
                        },
                    });
                });
            }

            if ($('.ColorPicker input.form-control').length){
                $('.ColorPicker input.form-control').each(function(e){
                    var color = $(this).val();
                    if (typeof $(this).attr('placeholder') !== typeof undefined && $(this).attr('placeholder') !== false && !color.length){
                        var color = $(this).attr('placeholder');
                    }
                    var obj = $(this);
                    $(this).ColorPicker({
                        onChange: function (hsb, hex, rgb) {
                            obj.val(hex);
                        },
                        onBeforeShow: function () {
                            if (color.length){
                                obj.ColorPickerSetColor(color);
                            }
                        },
                        onShow: function (colpkr) {
                            $(colpkr).fadeIn(500);
                            return false;
                        },
                        onHide: function (colpkr) {
                            $(colpkr).fadeOut(500);
                            return false;
                        },
                    });
                });
            }
        }
    };

});
