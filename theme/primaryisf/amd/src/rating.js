// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is an empty module, that is required before all other modules.
 * Because every module is returned from a request for any other module, this
 * forces the loading of all modules with a single request.
 *
 * This function also sets up the listeners for ajax requests so we can tell
 * if any requests are still in progress.
 *
 * @package    theme_primaryisf
 * @module     theme_primaryisf/colorpicker
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define([], function() {

    return {
        /**
         *
         * Color picker
         * Author: Stefan Petre www.eyecon.ro
         * 
         * Dual licensed under the MIT and GPL licenses
         * 
         */
        init: function (a) {
    a.fn.rating = function(b) {
        b = b || function() {};
        this.each(function(d, c) {
            a(c).data("rating", {
                callback: b
            }).bind("init.rating", a.fn.rating.init).bind("set.rating", a.fn.rating.set).bind("hover.rating", a.fn.rating.hover).trigger("init.rating")
        })
    };
    a.extend(a.fn.rating, {
        init: function(h) {
            var d = a(this),
                g = "",
                j = null,
                f = d.children(),
                c = 0,
                b = f.length;
            for (; c < b; c++) {
                g = g + '<a class="star" title="' + a(f[c]).val() + '" />';
                if (a(f[c]).is(":checked")) {
                    j = a(f[c]).val()
                }
            }
            f.hide();
            d.append('<div class="stars">' + g + "</div>").trigger("set.rating", j);
            a("a", d).bind("click", a.fn.rating.click);
            d.trigger("hover.rating")
        },
        set: function(f, g) {
            var c = a(this),
                d = a("a", c),
                b = undefined;
            if (g) {
                d.removeClass("fullStar");
                b = d.filter(function(e) {
                    if (a(this).attr("title") == g) {
                        return a(this)
                    } else {
                        return false
                    }
                });
                b.addClass("fullStar").prevAll().addClass("fullStar")
            }
            return
        },
        hover: function(d) {
            var c = a(this),
                b = a("a", c);
            b.bind("mouseenter", function(f) {
                a(this).addClass("tmp_fs").prevAll().addClass("tmp_fs");
                a(this).nextAll().addClass("tmp_es")
            });
            b.bind("mouseleave", function(f) {
                a(this).removeClass("tmp_fs").prevAll().removeClass("tmp_fs");
                a(this).nextAll().removeClass("tmp_es")
            })
        },
        click: function(g) {
            g.preventDefault();
            var f = a(g.target),
                c = f.parent().parent(),
                b = c.children("input"),
                d = f.attr("title");
                if(!(parseInt(d) > 0)){
                    d = d.split(' ')[d.split(' ').length - 2]
                }
            matchInput = b.filter(function(e) {
                if (a(this).val() == d) {
                    return true
                } else {
                    return false
                }
            });
            matchInput.attr("checked", true);
            c.trigger("set.rating", matchInput.val()).data("rating").callback(d, g)
        }
    })
}}});