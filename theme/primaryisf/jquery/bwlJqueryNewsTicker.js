 /*****************************************************
  *@Description: BWL Sticky Animated News Ticker jQuery Plugin
  *@Version: 1.0.0
  *@Created By: MD MAHBUB ALAM KHAN
  *@Portfolio: http://www.bluewindlab.net
  *@Created At: 31-08-2015
  *@Last Edited: 31-08-2015
  *****************************************************/

if ( typeof Object.create !== 'function' ) {
    
    Object.create = function ( obj ) {
        
        function F() {};
        F.prototype = obj;
        return new F();
    }
    
}


(function( $, window, document, undefind ){

    var BWLNewsTicker = {
        
        init: function( options, elem ) {
            
           var self = this;
                 self.elem = elem;
                 self.$elem = $( elem ).find('.bwl_news_lists');
            
            /*------------------------------ DEFAULT OPTIONS ---------------------------------*/
              
            this.options = $.extend ( {}, $.fn.bwlJqueryNewsTicker.config, options); // Override old sutff    
            
            /*------------------------------  Wrap Ticker With Custom Class ---------------------------------*/
            
            if( self.options.hide_ticker == true ) {
                $( elem ).remove();
            }
            $( elem ).addClass('bnt_dn');
            /*------------------------------  THEME Class ---------------------------------*/
            
            // Initialize.
            var theme = "",
                 custom_ticker_wrapper_class = "",
                 custom_ticker_title_class = "",
                 custom_ticker_header_toggle_wrapper_class = "",
                 custom_ticker_footer_toggle_wrapper_class = "";
            
            
            if( self.options.theme == "red") {
                
                theme = "red_";
                
            } else if( self.options.theme == "green") {
                
                theme = "green_";
                
            } else if( self.options.theme == "blue") {
                
                theme = "blue_";
                
            } else if( self.options.theme == "orange") {
                
                theme = "orange_";
                
            } else if( self.options.theme == "pink") {
                
                theme = "pink_";
                
            } else if( self.options.theme == "purple") {
                
                theme = "purple_";
                
            } else if( self.options.theme == "olive") {
                
                theme = "olive_";
                
            } else if( self.options.theme == "yellow") {
                
                theme = "yellow_";
                
            } else if( self.options.theme == "gray") {
                
                theme = "gray_";
                
            } else if( self.options.theme == "chocolate") {
                
                theme = "chocolate_";
                
            } else {
                
            }
            
            // Generate Theme Class.
            
            if ( theme !="" ) {
                
                custom_ticker_wrapper_class += theme+"bwl_"+this.options.position+"_ticker_wrapper ";
                custom_ticker_title_class += " "+theme+"ticker_title";
                custom_ticker_header_toggle_wrapper_class += theme+"ticker_header_toggle_wrapper ";
                custom_ticker_footer_toggle_wrapper_class += theme+"ticker_footer_toggle_wrapper ";
                
            }
            
            
            /*------------------------------  Wrap Ticker With Custom Class ---------------------------------*/
            
            $( elem ).attr("class", custom_ticker_wrapper_class +"bwl_"+this.options.position+"_ticker_wrapper"); // Add Class With Parent Div.
            self.$elem.wrap('<div class="bwl_'+this.options.position+'_ticker_content"></div>'); // Add Custom Class With Ticker Wrapper.
            
            /*------------------------------  Ticker Title Settings ---------------------------------*/
            
            if( this.options.title_show == true ) {
                
                self.$elem.before('<span class="ticker_title'+custom_ticker_title_class+'">'+this.options.title+'</span>');
                
            }
            
            /*------------------------------ FEED ---------------------------------*/
            
            if( this.options.feed == true ) {
//                console.log("Feed");
                self._bwl_get_feed_data(  $( elem ) );
            }
            
            
             /*------------------------------  Ticker Button Icons Settings ---------------------------------*/
             
            this.options.bwl_up_icon = '<i class="fa '+self.options.bwl_up_icon+'"></i>';
            this.options.bwl_down_icon = '<i class="fa '+self.options.bwl_down_icon+'"></i>';
            this.options.bwl_prev_icon = '<i class="fa '+self.options.bwl_prev_icon+'"></i>';
            this.options.bwl_next_icon = '<i class="fa '+self.options.bwl_next_icon+'"></i>';
            
            /*------------------------------  Ticker Button Settings---------------------------------*/
            
            if( this.options.btn_show == true ) {
                
                self.$elem.after('<div class="ticker_btn_wrapper">'+
                                        '<span class="bwl_prev_btn">'+this.options.bwl_prev_icon+'</span>'+
                                        '<span class="bwl_next_btn">'+this.options.bwl_next_icon+'</span>'+
                                       '</div>');

                if( this.options.position == "header" ) {
                  
                    $( elem ).after('<div class="'+custom_ticker_header_toggle_wrapper_class+'ticker_header_toggle_wrapper">'+
                                            '<span class="bwl_header_toggle_btn" data-display-status="0">'+this.options.bwl_up_icon+'</span>'+
                                        '</div>');
                    
                } else if( this.options.position == "footer" ) {
                                    
                    $( elem ).after('<div class="'+custom_ticker_footer_toggle_wrapper_class+'ticker_footer_toggle_wrapper">'+
                                            '<span class="bwl_footer_toggle_btn" data-display-status="0">'+this.options.bwl_down_icon+'</span>'+
                                         '</div>');
                }
                
            }
            
            /*------------------------------ Title Arrow (Triange/Circle) ---------------------------------*/
            if ( self.options.title_arrow_show == true ) {
           
                $( elem ).find('.ticker_title').append($('<span/>', {
                    'class':'bnt-'+self.options.title_arrow+'-right'
                }));
                
                
                $( elem ).find('span.bnt-'+self.options.title_arrow+'-right').css({
                    'top' : (parseInt(self.options.height, 10) - parseInt($('.bnt-'+self.options.title_arrow+'-right').outerHeight(),10)) /2 + 'px'
                });
                
            }
           
            /*------------------------------ Custom Theme Settings ---------------------------------*/
            
            self._bwl_custom_theme_settings( $( elem ) );
            
            /*------------------------------ Border Settings ---------------------------------*/
            
            self._bwl_ticker_border_settings( $( elem ) );
            
            /*------------------------------  Process Animation Class ---------------------------------*/
            
            var animation_class = self._get_animation_class();
            
            this.options.OutAnimation = animation_class[0];
            this.options.inAnimation = animation_class[1];
            
            /*------------------------------ SET TIMMER ---------------------------------*/
            self._bwl_set_timmer();
            
            /*------------------------------ BIND ALL CLICK EVENTS  ---------------------------------*/
            
            this._bindEvent();
            
//            return false;
            this._init_bwl_ticker( $( elem ) );
            
            /*------------------------------ Ticker Options ---------------------------------*/    
            
            if( this.options.position == "header" ) {
          
                $( 'span.bwl_header_toggle_btn'). on('click', function(){
//                    console.log("Toggle Header");
                    self._bwl_toggle_ticker_bar(  $( elem ), $(this), self.options.position, $(this).data('display-status') );
                });
            
            }
            
            if( this.options.position == "footer" ) {
                
                $( 'span.bwl_footer_toggle_btn').on('click' ,function(){
//                    console.log("Toggle Footer");
                    self._bwl_toggle_ticker_bar(  $( elem ), $(this), self.options.position, $(this).data('display-status') );
                });
                
             }
            
            
            $( elem ).on("click", 'span.bwl_next_btn' ,function(){
//                console.log("Next");
                self._bwl_display_next_item();
            });
           
           $( elem ).on("click", 'span.bwl_prev_btn' ,function(){
//               console.log("Previous");
                self._bwl_display_prev_item();
            });
            
            //RTL.
            
            self._bwl_ticker_rtl_settings( $( elem ) );
             
        },
                
        _bindEvent: function() {
    
            var self = this;
            
            self.$elem.find( 'li' ).first().addClass( self.options.inAnimation );
            
            self.$elem.find('li a').each(function() {

                        $(this).hover(function() {
//                            console.log("Mouse Hover");
                            clearTimeout(self.bnt_timmer);
                        }, function() {
//                            console.log("Mouse Out");
                            self._bwl_set_timmer();
                        })

                });

        },
        
        _init_bwl_ticker: function( $elem ) {
           
            var self = this,
                  bwl_ticker_wrapper = $elem,
                  bwl_header_toggle_btn = $elem.next('.bwl_display_btn'),
                  bwl_news_ticker_container_height = bwl_ticker_wrapper.outerHeight();
            
            if ($('.bwl_header_toggle_btn').length && self.options.position=="header") {
                
                var ticker_header_toggle_wrapper = $('.ticker_header_toggle_wrapper'),
                     bwl_header_toggle_btn = $('.bwl_header_toggle_btn');
                
                var $bnt_header_elements = $([]).add($elem).add(ticker_header_toggle_wrapper);
                     $bnt_header_elements.addClass('bnt_dn');
                     
//                     console.log("Header Ticker Cookie Status: " + $.cookie('bwl_header_ticker_status'));
                
                if ( typeof( self._bwl_read_cookie('bwl_header_ticker_status') ) != 'undefined' && self._bwl_read_cookie('bwl_header_ticker_status') == 0) {
//                    console.log("Hide The Header Ticker");
                    // Hide The Header Ticker
                    //clearTimeout(self.bnt_timmer); // reset timmer
                    
                    ticker_header_toggle_wrapper.css({
                        'right':'21px',
                        'top' : '0px'
                    }); 
                    
                    ticker_header_toggle_wrapper.slideDown('slow').find('span').html(self.options.bwl_down_icon).data('display-status', 0);


                } else {
                     // Show Header Ticker.
//                     console.log("Show The Header Ticker");
                     $elem.slideDown('slow', function(){
                         ticker_header_toggle_wrapper.slideDown('slow').find('span').html(self.options.bwl_up_icon).data('display-status', 1);
                     });
                     
                     ticker_header_toggle_wrapper.css({
                        'right':'21px',
                        'top' : bwl_news_ticker_container_height+'px'
                    });  
                    
                }
                
                // RTL Support
                
                if( self.options.rtl == true ) {
                    
                    ticker_header_toggle_wrapper.css({
                        'left':'21px'
                    });
                    
                } else {

                //LTL

//                ticker_header_toggle_wrapper.css({
//                        'right':'21px'
//                    });  

                }
                
                
            } else if ($('.bwl_footer_toggle_btn').length && self.options.position=="footer") {
                
                var ticker_footer_toggle_wrapper = $('.ticker_footer_toggle_wrapper'),
                     bwl_footer_toggle_btn = $('.bwl_footer_toggle_btn');
                
                var $bnt_footer_elements = $([]).add($elem).add(ticker_footer_toggle_wrapper);
                     $bnt_footer_elements.addClass('bnt_dn');
                
                if ( typeof( self._bwl_read_cookie('bwl_footer_ticker_status') ) != 'undefined' && self._bwl_read_cookie('bwl_footer_ticker_status') == 0) {
//                    console.log("Hide The Footer Ticker");
                    // Hide The Header Ticker
                    //clearTimeout(self.bnt_timmer); // reset timmer
                    ticker_footer_toggle_wrapper.css({
                        'bottom' : '0px'
                    });
                    
                    ticker_footer_toggle_wrapper.slideDown('slow').find('span').html(self.options.bwl_up_icon).data('display-status', 0);

                } else {
                    
                     // Show Footer Ticker.
//                     console.log("Show The Footer Ticker");
                     $elem.slideDown('slow', function(){
                         ticker_footer_toggle_wrapper.slideDown('slow').find('span').html(self.options.bwl_down_icon).data('display-status', 1);
                     });
                     
                     ticker_footer_toggle_wrapper.css({
                        'right':'21px',
                        'bottom' : bwl_news_ticker_container_height+'px'
                    });  
                     
                    
                }
                
                // RTL Support
                
                if( self.options.rtl == true ) {
                    
                    ticker_footer_toggle_wrapper.css({
                        'left':'21px'
                    });
                    
                } else {

                //LTL

//                    ticker_footer_toggle_wrapper.css({
//                        'right':'21px',
//                        'bottom' : bwl_news_ticker_container_height+'px'
//                    });  

                }
                
                
            } else {
                
                // Will do code here later.
            }
            
            return false;  
                
            
        },
        
        _bwl_custom_theme_settings: function( $elem ) {
            
            var self = this;
//            $elem.find('.ticker_title').append('<div class="bnt-triangle-right"></div>');
//            console.log(self.options.height);
            
            if( self.options.height != 48 ) {
            
                var $items = $([]).add($elem).add($elem.find('.ticker_title')).add($elem.find('.bwl_news_lists li'));
            
                $items.css({
                    'height': self.options.height+'px',
                    'line-height': self.options.height+'px'
                });
                
                var $ticker_btn_height = $elem.find(".bwl_prev_btn").outerHeight();
//                console.log(" "+$ticker_btn_height);
//                console.log(" "+parseInt(self.options.height) - parseInt($ticker_btn_height));
                //Ticker Content Background
                $elem.find(".ticker_btn_wrapper").css({

                    'top': (parseInt(self.options.height,10) - parseInt($ticker_btn_height,10)) /2 + 'px'

                });
                
            }
            
            if( self.options.theme == "custom") {
                 
                // Title
                $elem.find('.ticker_title').css({
                    'background-color': self.options.title_bg,
                    'color': self.options.title_color
                });
                
                //Next & Previous Button.
                $elem.find('.bwl_prev_btn, .bwl_next_btn').css({
                    'border-color': self.options.title_bg,
                    'background-color': self.options.title_bg,
                    'color': self.options.title_color
                });
                
                // Toggle Button
                $(".bwl_"+self.options.position+"_toggle_btn").css({
                    'background-color': self.options.title_bg,
                    'color': self.options.title_color
                });
                
                //Ticker Content Background
                $(".bwl_"+self.options.position+"_ticker_wrapper").css({
                    'background-color': self.options.ticker_content_bg
                });
                
            }
            
        },
        
        _bwl_ticker_border_settings: function(  $elem ) {
            
            var self = this;
            
            if ( self.options.border_status == false ) {
//                console.log("Remove Border");
                $elem.css({
                    'border-width': '0px'
                });
                
            }
            
            if ( self.options.custom_border == true ) {
               
//             console.log("Custom Border");
                
                if( this.options.position == "header" ) {
                     
                     $elem.css({
                        'border-bottom': self.options.border_size+'px ' + self.options.border_style +' '+self.options.border_color
                    });
                    
                } else if( this.options.position == "footer" ) {
                    
                    $elem.css({
                        'border-top': self.options.border_size+'px ' + self.options.border_style +' '+self.options.border_color
                    });
                    
                } else {
                    
                    $elem.css({
                        'border': self.options.border_size+'px ' + self.options.border_style +' '+self.options.border_color
                    });
//                    
//                    if( self.option.rtl == false ) {
//                        $elem.css({
//                            'border-left': '0px solid #FFFFFF'
//                        });
//                    }
                    
                    
                }
                
            }
            
        },
        
        _bwl_toggle_ticker_bar: function( $elem, $toggle_btn, position, ticker_display_status ){
            var self = this;
//           console.log(position);
//           console.log(ticker_display_status);
           
           var direction_icon;

                if ( ticker_display_status == 1 ) {

                    $toggle_btn.data('display-status', 0).hide();

                    $([]).add($toggle_btn).add($elem).slideUp(function() {
                        
                        $('.ticker_'+position+'_toggle_wrapper').removeClass('bnt_dn').slideDown("slow", function(){
                            
                             if( position == 'header' ) {
                                
                                $(this).css({
                                    'top': '0px'
                                });
                                
                                direction_icon = self.options.bwl_down_icon;
                                
                            } else if( position == 'footer' ) {
                                
                                $(this).css({
                                    'bottom': '0px'
                                });
                                
                                direction_icon = self.options.bwl_up_icon;
                            }
                            
                            $toggle_btn.html( direction_icon ).slideDown(700);

                        });
                        
                    });

                    self._bwl_create_cookie('bwl_'+position+'_ticker_status', 0 , 30);

                    
 
                } else {
                    
                    $toggle_btn.data('display-status', 1).hide(1);
                    
                    $([]).add($toggle_btn).add($elem).slideDown(function() {
                        
                        $('.ticker_'+position+'_toggle_wrapper').removeClass('bnt_dn').slideDown(700, function(){
                           
                            if( position == 'header' ) {
                                
                                $(this).css({
                                    'top':  $elem.outerHeight()
                                });
                                
                                direction_icon = self.options.bwl_up_icon;
                                
                            } else if( position == 'footer' ) {
                                
                                $(this).css({
                                    'bottom':  $elem.outerHeight()
                                });
                                
                                direction_icon = self.options.bwl_down_icon;
                                
                            }
                            
                            $toggle_btn.html( direction_icon ).slideDown(1000);

                        });
                        
                    });
                    
//                    $.removeCookie('bwl_'+position+'_ticker_status', {path: '/'}); // => true
                    self._bwl_create_cookie('bwl_'+position+'_ticker_status');
                }
            
        },
        
        _bwl_get_feed_data: function( $elem ) {
           
            var self = this;
            
//            console.log(self.options.position);
            
            var bwl_parent_ticker_content = $elem.find('div.bwl_'+self.options.position+'_ticker_content');
            
             $.when( self._ajax_parse_feeddata( self.options.feed_limit, self.options.feed_url) ).done(function( data ){
                 
                 var feed_string = '';
                 
//                 console.log(data.responseData.feed.entries);
//                 console.log(data.responseData.feed.entries.length);
//                 console.log(typeof( data.responseData.feed.entries ));


                try {
                    
                    $.each(data.responseData.feed.entries,function(e,item){
                        
                        var target_string = "";
                        var feed_link = item.link;
                        if( self.options.feed_link_new_window == true ) {
                          target_string += ' target="_blank"';  
                        }
                        
                        if ( self.options.envato_user !="" ) {
                      
                            feed_link +='?ref='+self.options.envato_user;
                            
                        }
                        
                        feed_string+='<li><div class="itemTitle"><a href="'+ feed_link +'" '+target_string+'>'+item.title+"</a></div>";

                    });
                 
                    bwl_parent_ticker_content.find('ul.bwl_news_lists').html("").html(feed_string);
                    
                    self._bindEvent();
                    
                } catch (exception) {
                    
                    feed_string += '<div class="bnt_nothing_found">Nothing Found!</div>';
                    bwl_parent_ticker_content.html("").html(feed_string);
                    
                }


            });
            
        },
        
        _ajax_parse_feeddata: function(feed_limit, feed_url) {
            
            return $.ajax({
                                url:"https://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num="+feed_limit+"&output=json&q="+encodeURIComponent(feed_url)+"&hl=en&callback=?",
                                data:{},
                                dataType: 'JSON'
                            });
            
        }, 
        
        _bwl_display_next_item: function(){
           
            var self = this,
                 parent_height = self.$elem.height();
                 clearTimeout(self.bnt_timmer); // reset timmer
                 
            self.$elem.find('li').first().addClass(self.options.OutAnimation).delay(300).queue(function() {

                $(this).removeAttr('class').appendTo(self.$elem).dequeue();
                self.$elem.find('li').first().addClass(self.options.inAnimation).attr("height", parent_height + "px");
                self._bwl_set_timmer(); // enable timmer.

            })
            
        },
        
         _bwl_display_prev_item: function(){
           
            var self = this,
                 parent_height = self.$elem.height();
                 clearTimeout(self.bnt_timmer); // reset timmer
            self.$elem.find('li').last().addClass(self.options.OutAnimation).delay(300).queue(function() {

                $(this).removeAttr('class').prependTo(self.$elem).dequeue();
                self.$elem.find('li').removeAttr('class');
                self.$elem.find('li').first().addClass(self.options.inAnimation).attr("height", parent_height + "px");
                self._bwl_set_timmer(); // enable timmer.
            })
            
        },
        
        _bwl_set_timmer: function(){
           
            var self = this;
            
            self.bnt_timmer = setTimeout(function () {

                                                    self._bwl_show_ticker_next_item();

                                            }, self.options.interval );
            
        },
            
        _get_animation_class : function() {
          
          var self = this,
                inAnimation = "animated ",
                outAnimation = "animated ";
       
        if (self.options.animation == "flash" ) {
              
            outAnimation += 'fadeOut';
            inAnimation += 'flash';                
            
        } else if (self.options.animation == "shake" ) {
              
               outAnimation += 'fadeOut';
               inAnimation += 'shake';
              
          } else if (self.options.animation == "tada" ) {
              
               outAnimation += 'tada';
               inAnimation += 'tada';
              
          } else if (self.options.animation == "swing" ) {
              
               outAnimation += 'swing';
               inAnimation += 'swing';
              
          } else if (self.options.animation == "wobble" ) {
              
               outAnimation += 'wobble';
               inAnimation += 'wobble';
              
          } else if (self.options.animation == "pulse" ) {
              
               outAnimation += 'pulse';
               inAnimation += 'pulse';
              
          } else if (self.options.animation == "flipx" ) {
              
               outAnimation += 'flipOutX';
               inAnimation += 'flipInX';
              
          } else if (self.options.animation == "faderight" ) {
              
               outAnimation += 'fadeOutRight';
               inAnimation += 'fadeInLeft';
              
          } else if (self.options.animation == "fadeleft" ) {
              
               outAnimation += 'fadeOutLeft';
               inAnimation += 'fadeInRight';
              
          } else if (self.options.animation == "slide" ) {
              
               outAnimation += 'slideOutLeft';
               inAnimation += 'slideInRight';
              
          } else if (self.options.animation == "slideup" ) {
              
               outAnimation += 'slideOutUp';
               inAnimation += 'slideInDown';
              
          } else if (self.options.animation == "bounce" ) {
              
               outAnimation += 'bounceOu';
               inAnimation += 'bounceIn';
              
          } else if (self.options.animation == "lightspeed" ) {
              
               outAnimation += 'lightSpeedOut';
               inAnimation += 'lightSpeedIn';
              
          } else if (self.options.animation == "roll" ) {
              
               outAnimation += 'rollOut';
               inAnimation += 'rollIn';
              
          } else if (self.options.animation == "rotate" ) {
              
               outAnimation += 'rotateOut';
               inAnimation += 'rotateIn';
              
          } else {
              
               outAnimation += 'fadeOut';
               inAnimation += 'fadeIn';
              
          }

          return [outAnimation, inAnimation];
        
    },     
            
        _bwl_show_ticker_next_item: function() {

            var self = this,
                 parent_height =  self.$elem.height();
                
                self.$elem.find( 'li' ).first().addClass( self.options.OutAnimation ).delay( 1000 ).queue(function() {

                    $(this).removeAttr( 'class' ).appendTo( self.$elem ).dequeue();
                    self.$elem.find( 'li' ).first().addClass(  self.options.inAnimation ).attr("height", parent_height+"px");

                })

               self._bwl_set_timmer();

        } ,
        
        _bwl_ticker_rtl_settings: function( $elem ) {
            
             var self = this;
             
             if( self.options.rtl == true ) {
                    
                    $elem.find('.ticker_title').css({
                        'float':'right',
                        'margin-right' : '0px',
                        'margin-left' : '5px'
                    });
                    
                    $elem.find('ul.bwl_news_lists li').css({
                        'text-align':'right',
                        'float' : 'right'
                    });
                    
                    $elem.find('div.ticker_btn_wrapper').css({
                        'left':'5px'
                    });
                    
                    if( self.options.position == 'inline' ) {
                    
                        if( self.options.custom_border == false ) {
                            self.options.border_size = 1;
                        }
                    
                        $elem.css({
                            'border-left': self.options.border_size+'px ' + self.options.border_style +' '+self.options.border_color
                        })
                    
                    }
                    
            } else {
                
                //LTL

                $elem.find('div.ticker_btn_wrapper').css({
                        'right':'5px'
                    }); 
                   
                   // Handle The Border Section When Ticker Title is Hidden
                   
                   if( self.options.position == 'inline' && this.options.title_show == false) {
                    
                        if( self.options.custom_border == false ) {
                            self.options.border_size = 1;
                        }
                    
                        $elem.css({
                            'border-left': self.options.border_size+'px ' + self.options.border_style +' '+self.options.border_color
                        })
                    
                    } else {
                   
                        $elem.css({
                            'border-left': '0px ' + self.options.border_style +' '+self.options.border_color
                        })    
                
                   }
                   
            }
            
        },
    
        _bwl_create_cookie: function(name,value,days) {

            /*------------------------------ Create Cookie  ---------------------------------*/

            if (days) {
            var date = new Date();
            date.setTime(date.getTime()+(days*24*60*60*1000));
            var expires = "; expires="+date.toGMTString();
            }
            else var expires = "";
            document.cookie = name+"="+value+expires+"; path=/";
            
        },

        _bwl_read_cookie: function (name) {

            /*------------------------------ Read Cookie  ---------------------------------*/

            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for(var i=0;i < ca.length;i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1,c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
            }
            return null;

        },

        _bwl_erase_cookie: function (name) {

            /*------------------------------ Erase Cookie  ---------------------------------*/

            var self = this;

            self._bwl_create_cookie(name,"",-1);

        }
        
        
    };
    
    // Initialization Of Plugin

    $.fn.bwlJqueryNewsTicker = function( options ) {
        
        return this.each(function(){
           
            var news_ticker=  Object.create( BWLNewsTicker );
                  news_ticker.init( options, this );
            
        });
        
    };
    
    // Default Options Setion.
    
    $.fn.bwlJqueryNewsTicker.config = {
            position: 'header', // 3 Positions. header,footer,inline 
            hide_ticker: false, // Set value as True, if you want to hide ticker.
            interval: 7000, // For each slide rotation.
            animation: 'fade', // Available animations. rotate/roll/lightspeed/bounce/slideup/slide/fadeleft/faderight/flipx/wobble/shake/pluse/flash/swing/tada
            height: '48', // Add any numeric value to set ticker custom height.
            title: 'Breaking News', // Set Custom Ticker Title.
            title_show: true, // Set value as False, if you hde the title box.
            title_arrow_show: true,  // Set value as False, if you want to hide the arrow before title.
            title_arrow: 'triangle', // triangle/circle
            title_bg: '#2c2c2c', // Set Ticker Title Background color.
            title_color: '#FFFFFF', // Set Ticker Title Text Color.
            ticker_content_bg: '#FFFFFF', // Set Ticker Content Background Color
            ticker_content_color: '#2c2c2c', // Set Ticker Content Text Color
            border_status: true, // Set value as false if you want to hide border.
            custom_border: false, // Set value as true if you want to create you're own ticker border.'
            border_size: 3, // Set border Width.
            border_color: '#2c2c2c', // Set border color.
            border_style: 'solid', //dotted, solid, dashed
            btn_show: true, // Set value as false, if you want to hide Next/Prev Button.
            bwl_up_icon: 'fa-arrow-up', // fa-arrow-up/fa-angle-double-up/fa-angle-up/fa-chevron-up/fa-caret-up/fa-chevron-circle-up
            bwl_down_icon: 'fa-arrow-down',  // fa-arrow-down/fa-angle-double-down/fa-angle-down/fa-chevron-down/fa-caret-down/fa-chevron-circle-down
            bwl_prev_icon: 'fa-arrow-left', // fa-arrow-left/fa-angle-double-left/fa-angle-left/fa-chevron-left/fa-caret-left/fa-chevron-circle-left
            bwl_next_icon: 'fa-arrow-right', // fa-arrow-right/fa-angle-double-right/fa-angle-right/fa-chevron-right/fa-caret-right/fa-chevron-circle-right
            rtl: false, // Set value as True if you want to enable Right To Left Text Mode.
            feed: false, // Set value as True if you want to parse RSS Feed
            feed_url: 'http://codecanyon.net/feeds/new-codecanyon-items.atom', // Put A Valid RSS/Atom Feed link.
            feed_location: 'header', // header/footer/inline
            feed_limit: 5, // set any numeric value.
            feed_link_new_window: true, // Set true if you want to open feed link in new window.
            envato_user: '', // Set you're envato user name for Referral link. Example: xenioushk'
            theme: 'black' // black(default)/red/green/blue/orange/pink/purple/olive/yellow/gray/chocolate
            
    };
    
})( jQuery, window, document);